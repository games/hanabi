# Hanabi - Conventions et techniques (v4.23.7)

Bienvenue ! Que vous débutiez ou non avec Hanabi, si vous aimez apprendre de nouvelles choses, vous êtes au bon endroit.

(Pour les plus pressé·e·s d’entre vous, ça commence [ici](#débutant-niveau-1), et pour les habitué·e·s, la 
[table des matières](#table-des-matières) est à la fin).

## Introduction

<a href="#introduction"></a>

Si vous arrivez tout juste, je vous invite à lire ce document dans l’ordre, au fur et mesure que vous expérimentez, car les notions sont
introduites de manière incrémentale : les bases sont au début, et plus les techniques sont sophistiquées plus elles reposent sur celles
présentées auparavant.

Tout ce qu’il vous faut, c’est de la logique, pratiquer, et une équipe d’au moins 3 personnes (même si les grands principes 
restent valables à 2 aussi).

### Sommaire

Voici rapidement les thèmes principaux abordés dans chaque partie :

- Débutant
  - [niveau 1](#débutant--niveau-1) : Protéger les cartes
  - [niveau 2](#débutant--niveau-2) : Faire poser les cartes
  - [niveau 3](#débutant--niveau-3) : La position finesse
- Avancé
  - [niveau 1](#avancé--niveau-1) : La finesse
  - [niveau 2](#avancé--niveau-2) : La finesse renversée et la jouabilité implicite
  - [niveau 3](#avancé--niveau-3) : La mono-finesse
- Expérimenté
  - [niveau 1](#expérimenté--niveau-1) : Le bluff
  - [niveau 2](#expérimenté--niveau-2) : La finesse ordurière et son bluff
  - [niveau 3](#expérimenté--niveau-3) : Les bluffs avancés
- [Expert](#expert) : Le jeu en couches

Pour plus de détails, consultez la [table des matières](#table-des-matières) à la fin.

Chaque thème est abordé sous forme de **convention**, d’**astuce** ou de **technique**. Un même thème peut comporter plusieurs 
aspects (ex : 1**a**, 1**b**…), dont certains ne sont introduits que plus tard, pour un niveau de jeu plus avancé.

Dans les titres est parfois indiqué le nom anglais, tel qu’il est plus largement utilisé dans la communauté internationale et notamment sur la
plate-forme en ligne [Board Game Arena (BGA)](https://boardgamearena.com/). Ces termes anglais sont récapitulés à la fin du document
([§ traduction](#traduction)).

### Vocabulaire

Les termes les plus couramment utilisés sont les suivants :

- **Défausser une carte** : La mettre « à la poubelle », dans la défausse. Elle ne peut plus être utilisée durant la partie, mais on peut toujours
consulter les cartes défaussées.
- **Marquer une carte** : Utiliser un jeton indice pour montrer une ou plusieurs cartes dans la main de quelqu’un d’autre
(soit d’une même *valeur*, soit d’une même *couleur*). La ou les cartes en question sont désormais marquées.
- **Poser une carte** : La placer dans la zone de feux d’artifice, dans une nouvelle pile ou sur la carte de valeur précédente.
- **Causer une bombe** : Tenter de poser une carte qui n’est pas ***jouable*** actuellement. À la 3ᵉ bombe, l’équipe 
perd immédiatement la partie. (Selon la version du jeu, les bombes sont matérialisées via des jetons rouges ou des tuiles représentant
un « paysage ».)

D’autres termes de vocabulaire sont introduits tout au long du document, et mis en valeur dans le texte (italique/gras). 
Référez-vous au [lexique](#lexique) à la fin pour en consulter la liste complète.


### Noms des personnes de l’équipe

Dans les exemples, il peut y avoir jusqu’à 5 personnes autour de la table : **A**lice, **B**ob, **C**arole, **D**avid et
**E**ve (notez leurs **initiales**). À chaque fois, on considère que *vous* êtes à la place d’**Alice** et que *les autres* jouent
après vous dans l’ordre **alphabétique** :

![Les 5 personnes jouent dans l’ordre alphabétique](images/tour-de-table.jpg)

Pour simplifier la représentation des situations complexes, les mains sont représentées en colonne et non en cercle, comme on peut le voir 
dans l’exemple ci-dessous.

### Légende des illustrations

Ce document est illustré par de nombreuses images ([générées via cet outil](https://hanabi.devnotebook.fr)). Voici comment lire celle-ci :

[![Illustration type](images/legende-illustration.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C!(B1)%2CXX%2C(X)1%2CXX%2C(Y)1%2C%3C-B%7CXX%2CRX%2CXX%2CR(4)%2CR5&highestCardsPlayed=G1%2CRX%2CBX%2CWX%2CYX&discardedCards=G4%2CB2&playersName=&nbRemainingCardsInDeck=22)

- En haut à gauche apparaissent la pioche et les cartes posées. Ici, seul le `1 vert` a été posé et il reste 22 cartes dans la pioche.
- En dessous, sont listées les cartes défaussées : un `2 bleu` et un `4 vert`.
- Les membres de l’équipe apparaissent en colonne, à droite, dans l’ordre du tour (**A**lice, puis **B**ob, puis **C**arole). 
La flèche sous leur nom rappelle le sens d’arrivée des cartes : la dernière carte piochée est placée du côté gauche de la main.
- Les cartes sont en **gris** quand la couleur est sans importance. Les informations en haut des cartes sont connues de toute l’équipe, 
alors que celles dans leur partie basse sont inconnues de leur propriétaire.
- Bob a reçu un indice `bleu` sur la carte qu’il vient de piocher, mais ignore qu’il s’agit d’un `1`.
- Bob a deux autres cartes marquées `1`, mais ignore que l’une d’elles est `jaune`.
- Carole a trois cartes marquées `rouge`. Elle sait que celle de droite est un `5`, mais pas que celle à côté est un `4`.

Selon ce qu’on veut illustrer, la partie gauche avec la taille de la pioche, les cartes posées et défaussées peut ne pas apparaître, 
et le nombre de mains peut varier.


## 📌 Conventions,💡 Astuces et 🔧 Techniques

<a href="#conventions-astuces-et-techniques"></a>

### Débutant – niveau 1

<a href="#débutant-niveau-1"></a>

Applicable dès la première partie.

Rien de complexe ici, juste de quoi limiter le chaos lors de la première expérience de jeu.

#### 📌 Convention 1 – Cartes marquées

<a href="#convention-1"></a>

Ne défaussez pas vos cartes marquées, sauf si vous savez qu’elles sont *inutiles* (ex : une carte marquée `1` alors que tous les `1` ont déjà 
été posés).

Dans Hanabi, vous avez peu, voire pas d’informations concernant votre main. Il serait donc dommage de défausser les seules cartes 
pour lesquelles vous en avez.

#### 💡 Astuce 0 – Mémoire

<a href="#astuce-0"></a>

Pour vous rappeler des indices qu’on vous donne, vous pouvez placer vos cartes d’une certaine manière dans votre main. Les remonter,
les baisser, les mettre de travers… Il est toutefois déconseillé de les déplacer les unes par rapport aux autres, afin de conserver l’ordre
dans lequel elles sont arrivées dans votre main.

Si vous préférez jouer avec **plus de raisonnement** et (presque) **pas de mémoire**, vous pouvez matérialiser tous les
marquages de carte. Par exemple, vous pouvez indiquer la *valeur* d’une carte avec un dé et sa *couleur* avec un bâtonnet/jeton de
couleur correspondante, en venant le placer devant la carte.

Ce procédé est utilisé dans toutes les versions en ligne du jeu, où l’on n’a pas de cartes physiques entre les mains. C’est très pratique
pour progresser dans le jeu, car cela évite les erreurs de mémoire et permet de se focaliser sur les points importants.

**Note** : Pour ne pas trop dénaturer le jeu, je conseille de ne matérialiser que les informations « publiques ». Tout ce que vous êtes 
capable de déduire en regardant les mains de vos partenaires doit rester secret, car eux/elles, ne le voient pas.   
Si vraiment vous voulez vous affranchir de votre mémoire, faites comme au Cluedo, prenez des notes sur papier pendant la partie et 
cachez-les aux autres.

#### 📌 Convention 2 – *Côté pioche*

<a href="#convention-2"></a>

Placez les nouvelles cartes que vous piochez toujours du même côté de votre main. Indiquez à vos partenaires dès le début de quel 
côté il s’agit. On peut l’appeler votre « *côté pioche* ».

En général, toute l’équipe se met d’accord, par exemple « tout le monde place ses nouvelles cartes du côté droit de sa main ». 
(Attention, quand vous êtes en face de vos partenaires, votre droite et la leur sont inversées.)

**Rappel** : dans les illustrations de ce document, le *côté pioche* est le côté **gauche** de chaque main.

#### 📌 Convention 3 – Position défausse (en anglais : *chop*)

<a href="#convention-3"></a>

En cas d’hésitation, lorsque vous souhaitez défausser une carte parmi plusieurs, choisissez la carte la plus **ancienne** (ou pour les cartes du début, celle à l’opposé 
du *côté pioche*). On peut dire que cette carte est en « ***position défausse*** ».

Ex :

- J’ai deux cartes marquées `5` et un autre marquée `rouge` et je souhaite défausser une carte.

[![Le 4 bleu est en position défausse](images/position-defausse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CRX%2CX5%2C(B4)%2CX5)

- Si toutes les cartes `rouges` ont déjà été posées, je défausse ma carte `rouge`. Sinon, je défausse ma plus ancienne non marquée. Je ne
la connais pas, mais il s’avère ici que c’est un `4 bleu`.

Cette convention très simple permet à tout le monde, de savoir qu’elle est la prochaine carte que chaque personne est susceptible de
défausser, et laisse le temps de marquer les nouvelles, si besoin, avant qu’elles ne soient défaussées.

#### 💡 Astuce 1a – Carte ***en danger***

<a href="#astuce-1a"></a>

Protégez les cartes « *uniques* » (celles en un seul exemplaire), en les marquant.

Ex : la personne après moi a un `5 jaune` non marqué en ***position défausse***. Je le marque d’un indice `5`.

[![Je marque la carte en danger](images/carte-en-danger.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2C!(Y5)%2C%3C-5&playersName=Bob)

Les `5` sont les seules cartes *uniques* au début de la partie. Les autres le deviennent quand tous leurs autres exemplaires sont
défaussés. Pensez bien à consulter régulièrement la **défausse** pour être au fait des nouvelles cartes *uniques*.  
Si, par exemple, les deux exemplaires du `2 rouge` sont dans la défausse, on ne pourra plus poser les `3`, `4` et `5 rouge`. La série rouge
ne pourra plus dépasser le `1` !

(Pour rappel, dans chaque couleur, il y a : trois cartes `1`, une carte `5` et deux cartes `2`, `3` et `4`.)

En général, il n’est pas *urgent* de protéger une carte *unique*. Cela le devient quand elle arrive en ***position défausse*** 
dans la main de la personne juste après vous et qu’elle risque de défausser. Cette carte est alors « ***en danger*** ».

#### Conclusion

C’est tout ! Vous savez repérer les cartes *uniques* et les empêcher d’être défaussées.  
Pour cela, vous savez les marquer au bon moment : quand elles sont « ***en danger*** », qu’elles arrivent en ***position défausse***.

La protection est indispensable, mais il faut aussi faire poser des cartes. Rendez-vous au chapitre suivant.


### Débutant – niveau 2

<a href="#débutant-niveau-2"></a>

Applicable après quelques tours de jeu, même lors d’une première partie.

On est toujours dans les mécaniques de base, mais les conventions sont légèrement plus arbitraires.

#### 📌 Convention 4 – Indices utiles

<a href="#convention-4"></a>

Dans la mesure du possible – *c’est-à-dire très souvent* – ne donnez un indice **que** pour **faire poser** des cartes, ou pour
**protéger** une carte ***en danger***.

Dans le premier cas, on parle d’indice « *offensif* », dans le second d’indice « *défensif* ».

Ex : au premier tour, ne marquez pas un `5` qui se trouve *côté pioche* ou même au milieu de la main d’une personne. Attendez qu’il 
soit ***en danger*** et qui sait, peut-être que d’ici là, d’autres `5` auront été piochés et que vous pourrez protéger tout le lot de `5` 
d’un seul indice.

Cette convention assez intuitive permet donc d’économiser énormément d’indices. Si on vous marque une carte de votre main, il
s’agit soit d’un indice *défensif*, soit d’un indice *offensif*.  
La carte marquée pouvait-elle être ***en danger*** ? Non, puisqu’elle était au milieu de main. Dans ce cas, pas besoin d’en connaître 
la *couleur* **et** la *valeur*, c’est sûrement pour la faire poser.

#### 📌 Convention 5a – indices *couleur* vs *valeur*

<a href="#convention-5a"></a>

N’utilisez un indice *couleur* **que** de manière ***offensive***, pour faire poser des cartes. 

Ex : je marque `rouge` le `3 rouge` ***jouable*** de la personne après moi.

[![Je donne un indice couleur](images/indice-couleur-direct.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2C!(R3)%2CXX%2CXX%2C%3C-R&highestCardsPlayed=WX%2CBX%2CR2%2CYX%2CGX&playersName=Bob)

À l’inverse, utilisez un indice *valeur* (= chiffre) pour protéger une carte ***en danger***.

Ex : je marque `3` le `3 bleu` *unique* de la personne après moi.

[![Je donne un indice valeur](images/indice-valeur-direct.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2C!(B3)%2C%3C-3&highestCardsPlayed=R1%2CY1%2CG2%2CBX%2CW1&discardedCards=B3&playersName=Bob)

Les cartes *uniques* étant d’abord des `5`, il est plus compréhensible de les marquer `5` pour les protéger. Par extension, on utilise 
des indices *valeur* pour protéger les cartes ***en danger***, et des indices *couleur* pour marquer les cartes ***jouables***.

Cette convention vous permet de marquer une carte en ***position défausse*** tantôt pour la faire poser (via un indice *couleur*), 
tantôt pour la protéger (via un indice *valeur*) et ce, de manière compréhensible pour votre partenaire.

#### 📌 Convention 6a – Marquage collatéral (*valeur*)

<a href="#convention-6a"></a>

S’il marque plusieurs cartes d’un coup, dont une ***en danger***, un indice *valeur* est considéré comme « *défensif* ». 
C’était peut-être le seul moyen de protéger la carte ! 

Ni la carte protégée, ni les autres cartes marquées par cet indice, ne doivent donc être considérées comme ***jouables*** 
sans information complémentaire.

Ex : Si un `3 vert` a été défaussé, et l’autre exemplaire est ***en danger*** chez Bob (c’est-à-dire en ***position défausse***), je le 
marque avec un indice *valeur*. Cela marque également deux autres `3`.

[![Je protège un 3 en en marquant deux autres supplémentaires](images/marquage-collateral-defausse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2C!(B3)%2CXX%2C!(R3)%2C!(G3)%2C%3C-3&highestCardsPlayed=Y2%2CGX%2CBX%2CWX%2CRX&discardedCards=G3&playersName=Bob)

Bob se dit que peut-être l’un d’eux est le `3 jaune` qui est ***jouable***, mais dans le doute considère l’indice comme *défensif*.

Pour aller plus loin, s’il a effectivement un `3 jaune`, j’aurais sans doute pu le marquer d’un indice *offensif* `jaune`. 
Seul cet indice `3` en revanche, permettait de protéger le `3 vert`. C’est cette interprétation qu’on doit donc privilégier.

#### 💡 Astuce 2 – Ne pas avoir peur de défausser

<a href="#astuce-2"></a>

Si vous n’avez aucun indice à donner pour faire poser ou protéger une carte, n’ayez pas peur de défausser.

S’il n’y a rien à marquer, c’est que les cartes dans les mains de vos partenaires ne sont pas *utiles* pour l’instant. 
Si vous n’avez rien à poser non plus, il est souvent préférable de défausser, pour en piocher d’autres plus intéressantes.

N’oubliez pas que chaque défausse vous rend 1 jeton indice, c’est très précieux.

Faites confiance à votre équipe, elle sait protéger les cartes *uniques*. Vous pouvez donc défausser sans risque majeur. Si toutefois 
vous défaussez une carte *unique* alors que votre défausse était prévisible, c’est vos partenaires qui sont responsables.

#### 📌 Convention 0 – Nommage des cartes

<a href="#convention-0"></a>

Pour simplifier le nommage des cartes dans une explication écrite, on utilise l’initiale de la couleur de la carte, avec son numéro. 
Ex : `1B` pour le 1 **b**leu, `3V` pour le 3 **v**ert.

Sur [Board Game Arena (BGA)](https://boardgamearena.com/), où le jeu rassemble des joueurs et joueuses de tous pays, 
ce sont les initiales des couleurs **anglaises** qui sont utilisées, et le numéro est placé derrière la lettre. Ex : `G2` pour le 2 vert, ou 
`W5` pour le 5 blanc.  
Cela présente le grand avantage, même entre francophones, de ne pas avoir la même initiale `B` pour le bleu et le blanc. 

Dans la suite de ce document, c’est cette notation « anglaise » qui est utilisée dans les exemples.  
Pour rappel : `G` pour vert, `B` pour bleu, `R` pour rouge, `W` pour blanc, `Y` pour jaune (et avec les extensions, `M` pour multicolore et `Bk`
pour noir).

#### Conclusion

C’était rapide ! Vous savez désormais donner et interpréter des indices *offensifs* (*couleur*) ou *défensifs* (*valeur*) et vous n’avez 
pas peur de défausser quand vous n’avez rien d’autre à faire.

À ce stade, il est facile de faire poser une carte en donnant un indice qui ne marque qu’elle, mais dans la vraie vie, un indice en marque
souvent plusieurs, pas toutes ***jouables***.

Rendez-vous au chapitre suivant.

### Débutant – niveau 3

<a href="#débutant-niveau-3"></a>

Applicable après quelques parties ou dès que les mécaniques de base du jeu sont assimilées.

La notion de ***position finesse*** va améliorer considérablement vos indices *offensifs*.

#### 📌 Convention 7 – Position finesse (en anglais : *finesse position*)

<a href="#convention-7"></a>

En cas d’hésitation pour poser une carte parmi plusieurs, posez la plus **récente** (même si elle n’est pas marquée !). 
On dit que cette carte est en « ***position finesse*** ».

Une ***position finesse*** dépend de ce que vous voulez poser (ce n’est pas forcément la dernière carte piochée).

Ex :

- Si vous avez une seule carte marquée `2` au milieu de votre main et que vous voulez poser un `2`, c’est celle-là.
- Si l’on vous marque deux cartes `jaunes` et que vous voulez en poser une, choisissez la plus récente.
- Si l’on vous marque trois cartes `3` et que vous voulez en poser une, choisissez le `3` le plus récent.
- Si l’on vous marque trois cartes `3`, alors que celui du milieu était déjà marqué `bleu` et que vous voulez poser un `B3`, 
choisissez évidemment celui-là.
- Si vous n’avez pas de carte marquée `rouge` et que vous voulez en poser une, choisissez la plus récente (parmi celles pouvant être rouges).
- Si vous n’avez aucune carte marquée `5`, mais une carte marquée `jaune` pouvant être un `5` et que vous voulez poser un `5` 
(ex : au dernier tour), choisissez cette carte.
- Si vous n’avez aucune carte de marquée, mais que vous voulez en poser une (ex : au dernier tour), choisissez la plus récente.

Cette convention se combine avec la notion d’indices *utiles*. Si votre partenaire marque deux cartes d’un indice *couleur*, 
c’est nécessairement un indice *offensif* pour faire poser des cartes. (Si aucune de celles marquées n’était ***jouable***, l’indice 
n’aurait pas d’intérêt à ce moment-là du jeu.)  
Vous lui faites confiance et sachant désormais laquelle de ces cartes est en ***position finesse***, vous pouvez la poser.

**Note :** le mot « finesse » est le mot anglais pour la technique de **la passe** (ou de la « l’impasse ») dans les jeux de plis 
(principalement le bridge, la belote, etc.). Il n’est pas très cohérent avec Hanabi, mais est tellement utilisé par la communauté, 
y compris francophone qu’il n’est pas traduit dans ce document.

#### 📌 Convention 6b – Marquage collatéral (*couleur*)

<a href="#convention-6b"></a>

Si un indice *offensif* marque plusieurs cartes en même temps, **seule** celle en ***position finesse*** est ***jouable***. 

Les suivantes ne le sont pas a priori. On a probablement choisi de les marquer avec, parce qu’elles seront bientôt ***jouables*** 
ou ***en danger***. Attendez d’en savoir plus.

Ex : un cas typique de début de partie est de marquer un `2` ***jouable*** et un `5` avec un indice *couleur*. 
Le `2` étant plus récent, il sera posé, et le `5` est désormais protégé puisqu’il est marqué.

[![Le 5 jaune est marqué en bonus](images/marquage-collateral-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2C!(Y2)%2CXX%2C!(Y5)%2CXX%2C%3C-Y&highestCardsPlayed=Y1&playersName=Bob)

**Note** : Il existe d’autres conventions qui stipulent justement l’inverse (que toute les cartes marquées doivent être considérées comme
***jouables***), ce qui change en
profondeur la façon dont on donne les indices. Elles sont clairement **incompatibles avec ce document**.

#### 📌 Convention 8a – Prévisibilité

<a href="#convention-8a"></a>

Si on vous donne un indice *offensif*, on s’attend à ce que vous posiez, ou éventuellement donniez un indice, mais **pas** à ce que vous défaussiez.

D’ailleurs, pourquoi retarder la pose de votre carte en défaussant ?

Comme toutes conventions, celle-ci n’a pas pour but d’être respectée à la lettre, mais elle ajoute de la **prévisibilité**. 
Il est plus facile de savoir quoi faire à votre tour si vous pouvez prévoir ce que fera la personne après vous.

#### 📌 Convention 5b – indices *valeur* *offensifs*

<a href="#convention-5b"></a>

Les indices *valeur* ne sont pas restreints à la *défense*. Vous pouvez les utiliser de manière *offensive* pour faire poser des cartes. 

Le cas du `1` est le plus évident : si quelqu’un en a plusieurs en main au début de la partie, vous les marquerez tous d’un coup, 
d’un indice *valeur*, plutôt que de donner autant d’indices *couleur*.

De manière plus générale, si aucune carte marquée par l’indice ne semble ***en danger***, ce n’est **pas** un indice *défensif*. 
Il est *offensif* et la carte est donc ***jouable***.

Ex :

- Le `B2` a été posé, mais pas le `B3`. Il n’y a pas de carte `3` dans la défausse.
- Je marque `3` la carte en ***position défausse*** de Bob.
- Il comprend que c’est un indice *offensif* et pose cette carte.

[![Le 3 est jouable](images/indice-valeur-offensif.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2C(BX)%2CXX%2CXX%2C!(B3)%2C%3C-3&highestCardsPlayed=R1%2CY1%2CG2%2CB2%2CW1&discardedCards=R4)

Pourquoi ne pas avoir donné un indice *couleur* puisqu’on veut un indice *offensif* ? Parce que Bob a une autre carte `bleue` plus
récente. Si je donnais un indice *couleur*, c’est l’autre carte `bleue` qui se retrouvait en  ***position finesse***, et Bob aurait essayé de
poser la mauvaise.

**Remarque** : On préfère souvent l’indice *couleur* à l’indice *valeur*, car il donne une information plus précise. Si mon indice
*offensif* est  `R` et que la seule carte `rouge` posée est le `R1`, la personne qui le reçoit sait qu’elle a le `R2`.  
Si mon indice est `2`, elle sait que sa carte  est ***jouable***, mais pas forcément de quel `2` il s’agit. Cela peut la gêner si elle veut faire poser
un `2` à la personne après elle. Elle hésitera au cas où ça créerait un doublon.

#### 💡 Astuce 3 – Déduction et défausse

<a href="#astuce-3"></a>

Consultez régulièrement la défausse, vous en déduirez d’importantes informations.

Ex :  Je reçois un indice *défensif* sur un `4` alors que le seul `4` dans la défausse est `bleu`. J’en déduis que ma carte est un `B4`.

#### 💡 Astuce 4a – Évitez les doublons

<a href="#astuce-4a"></a>

Si possible, ne marquez pas une carte dont un autre exemplaire est déjà marqué dans la main de quelqu’un d’autre, ou bien deux cartes
identiques dans la main d’une même personne.

Dans ce second cas, essayez d’attendre qu’elle défausse la plus ancienne des deux.

Ex :

- Bob a deux `G2`, dont un en ***position défausse***.

[![Je ne marque pas le doublon](images/doublon.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2C(G2)%2CXX%2CXX%2C(G2)&highestCardsPlayed=G1%2CBX%2CRX%2CWX%2CYX)

- Ici, je ne donne pas l’indice `vert` (ni `2`) à Bob. J’attends qu’il défausse.

#### 💡 Astuce 4b – Évitez les cartes *inutiles*

<a href="#astuce-4b"></a>

De la même manière, essayez de ne pas marquer les cartes *inutiles* quand vous donnez des indices.

Ex : 

- Le `R2` a été posé et Bob a un `R3` ***jouable*** puis un `R1` en ***position finesse***.
- Je lui donne l’indice `3` plutôt que `rouge`, pour ne marquer que le `R3` (et pas le `R1` désormais *inutile*).

[![Je ne marque pas le 1](images/carte-inutile.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C!(R3)%2C(R1)%2CXX%2CXX%2CXX%2C%3C-3&highestCardsPlayed=R2%2CY1%2CG2%2CB2%2CW12)

Sans ça, une carte *inutile* peut rester très longtemps à polluer une main, car son/sa propriétaire n’osera pas la défausser. Vous devrez
alors lui donner un indice supplémentaire, juste pour l’en informer.

#### 💡 Astuce 1b – Plusieurs cartes ***en danger***

<a href="#astuce-1b"></a>

Si la personne après vous ne peut pas protéger toute seule les cartes ***en danger*** de la personne après elle, qui risque 
de défausser, aidez-la en protégeant la première ***en danger***.

Ex :

 - Un `R4` est dans la défausse. Les deux plus anciennes cartes de Carole sont respectivement le `R4` et le `Y5` : elle a deux cartes
 ***en danger***.
 - Je lui donne l’indice `4` pour protéger son `R4`.
 - Bob après moi, donne l’indice `5`, marquant le `Y5`.

[![Deux indices sont nécessaires](images/double-protection.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2C%3F(X5)%2C!(R4)%2C%3C-4%2C5&highestCardsPlayed=R1%2CY1%2CG2%2CB2%2CW1&discardedCards=R4)

Les deux cartes sont maintenant marquées. Elles ne risquent plus la défausse et ne sont plus ***en danger***.  
Si j’avais laissé Bob s’en charger seul, il aurait peut-être protégé le `4`, mais alors le `5` serait devenu ***en danger***.

Il arrive même parfois que trois personnes doivent donner des indices défensifs à une quatrième, qui n’a que des cartes *uniques* en main !

#### Conclusion

Vous savez donner ou recevoir un indice *offensif* marquant plusieurs cartes : celle en ***position finesse*** est ***jouable***, 
et les autres sont des cartes *utiles*, voire *uniques*. Vous savez donner et discerner des indices *valeur*, qu’ils soient *offensifs* 
ou *défensifs*. 

Bravo, vous connaissez les bases ! Que diriez-vous de faire poser plusieurs cartes qui se suivent avec un seul indice ? 
Rendez-vous au niveau **avancé**.


### Avancé – niveau 1

<a href="#avancé-niveau-1"></a>

Applicable dès que la notion de ***position finesse*** est acquise.

C’est à partir d’ici que le jeu offre son réel potentiel. Si vous et votre équipe vous faites confiance, un peu de « magie » pourra opérer !  
À ce niveau, vous pouvez sans problème utiliser le mode de jeu **6 couleurs** et/ou l’extension **Flamboyants**.

#### 📌 Convention 9a – *Finesse* (en anglais : *finesse* ou *prompt*)

<a href="#convention-9a"></a>

Si l’on marque une carte ni ***jouable***, ni ***en danger***, c’est que la ou les cartes « *manquantes* » pour qu’elle devienne ***jouable*** 
sont **actuellement** en ***position finesse*** dans les mains des personnes précédentes. Celles-ci doivent donc *répondre* à l’indice
en posant les cartes *manquantes* dans l’ordre.

Avec un seul indice, on fait ainsi poser 2 cartes ou plus !

Cette convention est appelée « *finesse* ». On parle ainsi de « créer une *finesse* », de « *répondre* à une *finesse* » et bien sûr, 
de la « ***position finesse*** » que vous connaissez déjà.

##### Finesse avec marquage

Ex : 

- Le `Y1` a été posé, et le `Y2` est marqué `2` dans la main de Bob (sans qu’il n’en connaisse la couleur).
- Je donne l’indice `jaune` à Carole en marquant son `Y3`, ce qui est clairement un indice *offensif*.

[![Le 2 jaune manquant est marqué](images/finesse-avec-marquage.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2C(Y)2%2CXX%2CXX%7CXX%2C!(Y3)%2CXX%2CXX%2C%3C-Y&highestCardsPlayed=Y1)

- Bob voit que s’il ne fait rien, Carole va poser son `Y3` alors qu’il n’est pas ***jouable***. Il comprend qu’il a la 
carte *manquante*, et que la ***position finesse*** pour le `Y2` est sa carte marquée `2`. Il la pose.
- Carole sait qu’elle a reçu un indice *offensif*, et comprend que si Bob a pu poser son `2` sans en connaître la couleur, c’est que sa carte marquée par l’indice n’est pas un `Y2` mais la suivante, le `Y3`. Elle peut donc la poser.

##### Finesse sans marquage

L’exemple précédent était assez intuitif pour Bob, puisque son `Y2` était partiellement connu. Mais rappelez-vous, une carte peut être 
en ***position finesse*** même sans être marquée.

Ex :

Cette fois, le `Y2` n’est pas marqué du tout.

[![Le 2 jaune manquant n’est pas marqué](images/finesse-sans-marquage.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(Y2)%2CXX%2CXX%2CXX%7CXX%2C!(Y3)%2CXX%2CXX%2C%3C-Y&highestCardsPlayed=Y1)

Bob a le même raisonnement que dans l’exemple précédent : s’il ne fait rien, Carole causera une bombe en essayant de poser son `Y3` 
non ***jouable***. Il comprend qu’il a la carte manquante  et que sa ***position finesse*** est la carte la plus récente de sa main. 
Il pose son `Y2`.

Attention, pour ces deux situations, si Bob ne *répond* pas, il doit avoir une bonne raison, compréhensible par Carole. Sinon, elle risque 
de poser sa carte trop tôt, et causer une bombe.  
Ces « bonnes raisons » sont en général des *indices urgents* à donner (voir plus loin dans le document).

Attention également, si Bob choisit de poser une autre carte d’abord, il ne devra pas oublier que la carte permettant de *répondre* 
à la *finesse* aura peut-être **bougé** dans sa main !

##### Indice *valeur*

Vous pouvez créer une *finesse* avec un indice *valeur*, tant qu’il s’agit d’un indice *offensif*.

Ex :

Cette fois, je donne l’indice `3` à Carole.

[![Mon indice est un indice valeur](images/finesse-indice-valeur.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(Y2)%2CXX%2CXX%2CXX%7CXX%2C!(Y3)%2CXX%2CXX%2C%3C-3&highestCardsPlayed=Y1&discardedCards=&playersName=&nbRemainingCardsInDeck=)

Bob a un raisonnement similaire. Si je donne un indice *offensif*, c’est pour faire poser des cartes. Il devine qu’il a le `Y2` manquant en main
et le pose.

##### Finesse distante

En général, c’est la personne juste avant la carte manquante qui donne l’indice, mais ce n’est pas obligatoire et cela fonctionne aussi 
« à distance ».

Ex :

Même situation, mais c’est Carole et David qui ont les cartes `jaunes` et toujours moi qui donne l’indice.

[![Carole a la carte manquante](images/finesse-distante.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7C(Y2)%2CXX%2CXX%2CXX%7CXX%2C!(Y3)%2CXX%2CXX%2C<-Y&highestCardsPlayed=Y1)

- Bob voit que le `Y3` n’est pas ***jouable***, mais que justement, Carole a la carte manquante en ***position finesse*** dans sa main.
Il fait quelque chose d’indépendant à mon indice, et ne marque **pas** le `Y2` de Carole. (Pas besoin, il est déjà connu de manière 
implicite grâce à mon indice.)
- Carole sait qu’elle a la carte manquante puisque Bob n’a pas *répondu* à l’indice, et que sans elle, David risque de causer une bombe.
Elle la pose.
- David peut poser son `Y3`.

#### 📌 Convention 9b – *Longue finesse* (en anglais : *long finesse*)

<a href="#convention-9b"></a>

On parle de *« longue » finesse* quand elle fait poser plus de deux cartes. Un exemple de situation parfaite :

- Le `R1` a été posé. Bob a le `R2` en **position finesse**, Carole le `R3`, David le `R4` et Eve le `R5`, 
toutes en **position finesse**.
- Je marque `rouge` le `5` d’Eve.

[![4 cartes pour un seul indice](images/longue-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(R2)%2CXX%2CXX%2CXX%7C(R3)%2CXX%2CXX%2CXX%7C(R4)%2CXX%2CXX%2CXX%7C!(R5)%2CXX%2CXX%2CXX%2C%3C-R&highestCardsPlayed=R1)

- Bob voit qu’après lui se suivent les `3`, `4` et `5` de la couleur de l’indice et qu’il manque le `2`. Il pose son `2`.
- Carole voit un `2` non marqué, posé à partir de l’indice sur le `5`, et les `4` et le `5` derrière elle. Elle pose son `3`.
- Même raisonnement pour David, qui pose son `4`.
- Au tour d’Eve, la carte que je lui ai marquée est ***jouable***. Elle la pose et la *finesse* est *résolue*. Un seul indice 
pour quatre cartes posées !

#### 📌 Convention 10a – Extension Avalanche de couleurs (mode 6 couleurs)

<a href="#convention-10a"></a>

Cette extension introduit une 6ᵉ série de cartes (les cartes **multicolores**), et trois nouveaux modes de jeu.

Comme il y a une série supplémentaire, on peut atteindre **30 points** !  
L’initiale `M` est utilisée pour désigner ces nouvelles cartes : `M1`, `M2`…` M5`.

Dans le mode le plus simple, la série de 10 cartes multicolores a une répartition identique aux autres (trois `1`, un unique `5`…).

Un nouveau type d’indice *couleur* `multicolore` est introduit.  
Exactement comme pour les cartes `bleues`, `rouges`… donner un indice de ce type marquera toutes, et uniquement les cartes `multicolores` 
de la main de votre partenaire.

Ce mode n’augmente pas vraiment la complexité. Il est plus difficile d’obtenir un score parfait, mais aussi plus facile d’atteindre 25 points,
 25/30 étant beaucoup plus accessible que 25/25.


#### 🔧 Technique 1 – Indice de *correction* et *extraction*

<a href="#technique-1"></a>

Vous avez parfois besoin de plusieurs indices successifs pour faire poser une carte, parce que les cartes ne sont pas dans 
le bon ordre dans la main de leur propriétaire. (Ex : quelqu’un a deux cartes vertes, mais celle qui est ***jouable*** n’est 
pas celle en ***position finesse***.) 
Dans ce cas, vous pouvez vous y mettre à deux, et donner chacun·e un indice pour la marquer.

Ex : 

- Tous les `1` ont été posés, sauf le `G1`. Carole en a justement un, mais dans sa main ses cartes en ***position finesse***, sont dans cet
ordre : `Y1`, `G2`, puis `G1`.
- Je donne l’indice `vert` pour lui marquer deux cartes.

[![Deux indices pour cibler une carte](images/indice-de-correction.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7C(Y1)%2C!(G2)%2C!(G1)%2CXX%2C%3C-G&highestCardsPlayed=B2%2CGX%2CR3%2CW2%2CY1)

- Bob comprend que s’il ne fait rien, Carole va essayer de poser son `2` et causer une bombe. Il *corrige* en donnant un indice 
`1` ou `2`.
- Carole vient de recevoir deux indices *offensifs* sur les mêmes cartes. Elle comprend qu’au moins une des deux est ***jouable*** et 
choisit la bonne. Si l’indice de *correction* était `2`, elle peut alors poser le `1` puis le `2` !

Attention, assurez-vous qu’il reste au moins un jeton indice à votre partenaire pour la *correction* !

**Note** : On appelle « *extraction* » ce genre d’enchaînement d’indices nécessaires pour faire poser une carte.

#### 📌 Convention 11a – Priorité entre *finesse* et correction

<a href="#convention-11a"></a>

Normalement, un indice « nécessitant *correction* » doit être distinguable d’une *finesse*, afin que la personne qui vous suit sache 
si elle doit poser une carte *manquante* ou *corriger*.

Dans l’exemple précédent, le premier indice `vert` marque le `2` et le `1`. Si Bob devait *répondre* à une finesse en posant un `G1`,
on aurait désormais un `G1` inutile marqué dans la main de Carole. Bob considère donc qu’il n’y a pas de *finesse* et qu’il doit corriger.

Si le premier indice était `2`, par contre, Bob devrait *répondre* à la *finesse*, car rien n’indique que le `G1` de Carole n’est nécessaire. 
Bob peut tout à fait avoir, lui aussi, un `G1` en ***position finesse*** et on aura alors deux cartes posées pour un seul indice :

[![L’indice 2 est une finesse](images/priorite-finesse-vs-correction.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(G1)%2CXX%2CXX%2CXX%7C(Y1)%2C!(G2)%2C(G1)%2CXX%2C%3C-2&highestCardsPlayed=B2%2CGX%2CR3%2CW2%2CY1)

#### 📌 Convention 12a – *Indice urgent*

<a href="#convention-12a"></a>

Un indice est *urgent* s’il ne peut pas attendre.

Ex :

- Marquer une carte ***en danger*** est *urgent*, si on l’empêche ainsi d’être défaussée.
- Donner un indice de *correction* à la personne après soi est *urgent*, pour lui éviter de causer une bombe.
- Créer une finesse est *urgent*, si l’une des cartes *manquantes* risque de ne plus être en ***position finesse*** si l’on attend.  
Ex : Bob sait qu’il peut poser son `B1`, mais vient de piocher le `R2`, alors que Carole a le `R3`. 
Si je ne marque pas le `3` tout de suite, Bob posera son `1`, décalant ainsi le `R2` dans sa main et je ne pourrai plus créer la *finesse*.

[![L’indice doit être donné pendant que le R2 est en position finesse](images/finesse-urgente.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(R2)%2CB(1)%2CXX%2CXX%7CXX%2C!(R3)%2CXX%2CXX%2C%3C-R&highestCardsPlayed=BX%2CR1)

Attention, même dans ces trois situations, l’indice n’est pas forcément *urgent*, si je suis sûr·e que la personne après moi va donner un indice (typiquement *défensif*). Les cartes de sa main ne bougeront donc pas et je pourrai donner l’indice « urgent » au tour suivant.

#### 💡 Astuce 5a – Ne spoliez pas

<a href="#astuce-5a"></a>

Évitez de donner un indice que la personne après vous peut donner elle-même, alors qu’elle n’a rien à poser.

Qui sait, peut-être qu’elle peut donner un meilleur indice et créer une *finesse* grâce à une carte de votre main, alors que vous n’auriez 
fait poser qu’une seule carte !

Donner un indice à la place de votre voisin ou voisine est considéré comme un « vol d’indice ». Cela peut être légitime, mais est
généralement malvenu.

#### 💡 Astuce 5b – Donnez de quoi s’occuper

<a href="#astuce-5b"></a>

À l’inverse, même si vous avez quelque chose à poser, mais que la personne qui vous suit risque de défausser, il est souvent intéressant 
de lui donner un indice *offensif*. Cela évitera qu’elle ne défausse tout de suite une carte *utile*, par exemple. Vous poserez la vôtre 
au prochain tour.

C’est d’autant plus important si votre indice permet de faire poser plusieurs cartes à une ou plusieurs personnes (via une *finesse*).

Attention, cette décision est toutefois à pondérer selon le nombre de jetons indice restants et si la carte que vous avez à poser est
attendue par d’autres pour compléter une *finesse*. De plus, il est parfois préférable de laisser quelqu’un défausser, notamment quand
beaucoup de ses cartes en ***position défausse*** sont *inutiles*.

#### 💡 Astuce 6a – Extension Flamboyants (défausse)

<a href="#astuce-6a"></a>

Cette extension introduit des récompenses aléatoires lorsque l’on pose la dernière carte d’une couleur, à la place de la récupération
d’un jeton indice habituelle. Parmi elles s’en trouvent deux permettant de récupérer des cartes depuis la défausse. 

Cela veut dire que même si les deux `G3` sont dans la défausse, vous aurez peut-être l’opportunité d’en récupérer un. 
Les cartes `G4` et `G5` ne sont donc pas encore *inutiles* !

#### 💡 Astuce 6b – Extension Flamboyants (pose depuis la défausse)

<a href="#astuce-6b"></a>

L’une des récompenses proposées par cette extension permet de récupérer une carte de la défausse, et de la poser **immédiatement**. 
C’est très puissant !

Pour ne pas gâcher cette récompense, il est souvent préférable de ne pas terminer une série tant qu’il n’y a pas au moins une carte 
***jouable*** dans la défausse, et d’attendre pour poser son `5`.

Attention, cette récompense n’est disponible qu’une fois au maximum et peut ne jamais tomber ! Si la pioche est presque vide, et que la fin 
de partie approche, il peut être préférable de ne pas trop attendre non plus.

#### 💡 Astuce 6c – Extension Flamboyants (pas de jeton)

<a href="#astuce-6c"></a>

Parmi les récompenses aléatoires proposées par cette extension, il y en a peu qui permettent de récupérer des jetons indice 
(comme la récompense habituelle dans une partie sans l’extension). 

Si vous pouvez poser votre `5`, mais qu’il n’y a plus de jeton indice disponible, demandez-vous s’il n’est pas préférable de défausser 
à la place. Cela assure un jeton à la personne après vous, au cas où elle aurait un indice *urgent* à donner.

#### Conclusion

Vous savez créer et *répondre* à une *(longue) finesse*. Vous avez une bonne idée de l’utilité d’un indice à un moment précis : 
est-il *urgent*, concerne-t-il, peut-il être donné par la personne après vous… ?

Les variations de la *finesse* ne s’arrêtent pas là. Par exemple, il serait pratique de pouvoir créer une *finesse* lorsque les cartes ne 
sont pas réparties dans le bon ordre entre vos partenaires. 

Rendez-vous au chapitre suivant.


### Avancé – niveau 2

<a href="#avancé-niveau-2"></a>

Applicable dès que l’équipe est à l’aise avec les *finesses*.

À ce niveau, vous pouvez utiliser le mode **6 couleurs difficile**, mais cela augmentera considérablement la difficulté !

#### 🔧 Technique 2a – *Finesse renversée* et *longue renversée* (en anglais : *(long) reverse finesse*)

<a href="#technique-2a"></a>

Alors que la *finesse* consiste à marquer une carte non ***jouable*** chez quelqu’un, pour que la personne **précédente**  *réponde*, 
la *renversée* (ou *finesse renversée*) consiste à faire *répondre* la personne **suivante**.

Ex : 

- Le `Y1` a été posé, et le `Y2` est marqué `2` dans la main de Carole (sans qu’elle n’en connaisse la couleur).
- Je donne l’indice `jaune` à Bob en marquant son `Y3`.

[![La carte manquante suit la carte marquée](images/renversee-avec-marquage.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C!(Y3)%2CXX%2CXX%2CXX%2C%3C-Y%7CXX%2C(Y)2%2CXX%2CXX&highestCardsPlayed=Y1)

- Bob voit la main de Carole et comprend que je n’ai pas de raison de marquer un second `Y2`. C’est donc le `3` qu’il a en main. 
Il ne pose pas sa carte et ne marque pas non plus le `Y2` de Carole. (Il patiente en faisant autre chose de son tour.)
- Carole voit que j’ai marqué une carte non ***jouable*** d’un indice *offensif*, et que Bob ne l’a pas posée. Elle devine qu’elle possède le
`Y2` *manquant* et le pose.
- Bob a vu Carole poser son `Y2` sans plus d’information. Il a ainsi confirmation qu’il possède bien la carte suivante, et peut poser son `Y3`.

Cela fonctionne même si le `Y2` de Carole n’est pas marqué du tout, mais en ***position finesse***. Bob le voit et suspecte 
alors la *renversée*. Il attend pour poser sa carte `jaune`.

[![La carte manquante n’est pas marquée](images/renversee-sans-marquage.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C!(Y3)%2CXX%2CXX%2CXX%2C%3C-Y%7C(Y2)%2CXX%2CXX%2CXX&highestCardsPlayed=Y1)

Carole voit qu’il manque le `Y2` pour rendre la carte marquée ***jouable*** et pose donc sa carte en ***position finesse*** : sa plus récente.

Comme pour les *finesses*, les *renversées* fonctionnent « à distance ». Je peux faire poser une carte par David, en donnant un indice à
Carole, même s’il y a Bob après moi, ou en donnant l’indice à Bob avec Carole entre lui et David.

De plus, il est possible de « mixer » une *(longue) renversée* et une *(longue) finesse*, dès lors que tout le monde comprend qu’il a les cartes *manquantes* pour que celle marquée devienne ***jouable***. Dans ce cas, peu importe que des personnes **précèdent** 
ou **succèdent** à celle ayant reçu l’indice, ou que celle qui donne l’indice ait elle-même une des cartes *manquantes*.

Ex : 

- Mes partenaires ont chacun·e une carte `jaune` en ***position finesse*** : Bob le `Y4`, Carole le `Y2` ***jouable*** et David le `Y5`. Quant à
moi, mon `Y3` est connu.
- Je marque le `Y5` de David d’un indice `jaune`.

[![Les cartes ne sont pas dans le bon ordre, mais forment une finesse](images/longue-finesse-avec-renversee.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CY3%7C(Y4)%2CXX%2CXX%2CXX%7C(Y2)%2CXX%2CXX%2CXX%7C!(Y5)%2CXX%2CXX%2CXX%2C%3C-Y&highestCardsPlayed=Y1)

Le principe est toujours le même : toutes les cartes *manquantes* doivent être posées jusqu’à rendre celle marquée ***jouable***.

**Petit mémo facile** : « Si je ne vois pas la carte manquante, alors c’est moi qui l’ai, en ***position finesse***. »

#### 📌 Convention 10b – Extension Avalanche de couleurs (mode 6 couleurs difficile)

<a href="#convention-10b"></a>

Ce mode fonctionne de la même manière que le premier, à un gros détail près qui change tout : **chaque carte multicolore est _unique_** !

Cela rajoute beaucoup de difficulté, car le nombre de cartes *uniques* est presque doublé dès le début de la partie.

Une convention particulière peut être utilisée dans ce mode : on peut *défendre* une carte multicolore **en danger**, avec un 
indice *couleur*.  Cela offre une option supplémentaire à l’indice *valeur*, qui oblige parfois à polluer une main avec des cartes *inutiles*.

Mettez-vous d’accord avec vos partenaires dès le début de la partie si un indice *couleur* sur une carte `multicolore` 
en ***position défausse*** doit être considéré comme *défensif* ou *offensif*.

#### 💡 Astuce 7 – Question de point de vue

<a href="#astuce-7"></a>

Pour savoir quoi faire à votre tour, posez-vous la question de ce que va faire la personne suivante, sachant qu’elle ne voit pas ses cartes, 
et qu’elle devra se mettre à la place de la personne après elle pour se décider.

Gardez à l’esprit le point de vue de chaque personne, en particulier si vous envisagez une *longue finesse/renversée* un peu complexe.

Grâce à cela, vous parviendrez mieux à déterminer les priorités, si un indice est *urgent*, si vous devez poser telle carte avant telle autre…

#### 💡 Astuce 8 – Priorité entre indices *offensif* et *défensif*

<a href="#astuce-8"></a>

Si quelqu’un a une carte ***jouable*** et une autre ***en danger***, privilégiez souvent l’indice *offensif*. Avec un peu de chance, 
la carte ***en danger*** sera ***jouable*** via *finesse* d’ici le tour suivant.

Attention, c’est parfois une mauvaise idée, quand il ne reste que très peu de jetons indice et que beaucoup de cartes vont 
bientôt être ***en danger*** !

#### 🔧 Technique 3 – Marquage double

<a href="#technique-3"></a>

Vous pouvez **marquer une carte déjà marquée** pour changer la façon dont son ou sa propriétaire doit la considérer.

**1<sup>er</sup> cas offensif**.  
Le cas évident consiste à modifier le marquage des cartes. Ex : quelqu’un avait un `G3` marqué `3` en main, qui vient de devenir 
***jouable***. Je lui donne l’indice `vert`.  
Il n’aurait pas posé son `3`, mais le marquage *couleur* vient modifier ses perspectives : il comprend qu’il a un `G3` ***jouable***.

**2<sup>d</sup> cas offensif**.  
Même situation, mais je donne l’indice `3` plutôt que `vert`, cette fois.  
La personne n’aurait pas posé son `3`, mais le second marquage vient modifier ses perspectives :  elle comprend qu’elle a un `3`
***jouable***.

Vous pouvez donc marquer une carte déjà marquée, avec le même indice. C’est parfois pratique pour éviter le marquage collatéral de 
cartes *inutiles*. C’est un indice *offensif*, pour faire poser la carte.

Ex : 

- Le `G2` a déjà été posé. Bob a un `G3` ***jouable***, marqué `3` au milieu de sa main (dont il ne connaît pas la couleur), mais il a aussi 
un `G2` et un `G1` non marqués. (Ces deux dernières cartes étant évidemment *inutiles*).
- Je marque `3` à nouveau sur son `G3` (plutôt que `vert`).
- Il comprend que son `3` est ***jouable***.

[![Je marque 3 une carte déjà marquée 3](images/marquage-double.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2C!(G3)%2C(G1)%2C(G2)%2C%3C-3&highestCardsPlayed=G2)

Cela fonctionne avec les indices *couleur* et *valeur*. On peut ainsi marquer `jaune` une carte déjà `jaune`, ou `4` une carte déjà `4`.

**1<sup>er</sup> cas défensif**.  
On m’avait indiqué trois `1` dans ma main, et après en avoir posé deux, je m’apprête à poser le dernier. On me marque alors `jaune` sur 
ce `1` alors que le  `Y1` a déjà été posé. Cela modifie mes perspectives : ce `1` n’est pas ***jouable***, mais *inutile*.

**2<sup>d</sup> cas défensif**.  
Même principe, mais cette fois, on me re-marque `1`. Cela modifie mes perspectives : ce `1` n’est **pas** ***jouable***, mais *inutile*.

**Attention**, pour les 2<sup>d</sup> cas (offensif et défensif), il faut bien que l’indice n’ajoute pas de nouveau marquage. Sinon, il s’agit 
simplement d’un indice offensif sur la nouvelle carte marquée, en ***position finesse***.

#### 📌 Convention 12b – *Défense* non *urgente*

<a href="#convention-12b"></a>

Vous n’êtes pas obligé de marquer la carte ***en danger*** de la personne après vous, si vous êtes sûr·e qu’elle ne va pas défausser.
L’indice n’est alors pas *urgent*.

Il y a deux raisons principales de penser ça : 
- Évidemment, lorsque cette personne sait qu’une de ses cartes est ***jouable***.
- Si elle a, elle aussi, un indice *urgent* à donner à la personne après elle.

Attention toutefois, quand le stock de jetons est très bas. Votre voisin ou voisine pourrait choisir de défausser quand même, pour récupérer 
un jeton.

Attention également, quand on joue à **3**, il arrive régulièrement qu’au même moment, tout le monde ait un indice *défensif urgent* 
à donner. Si personne ne se décide et se dit que ça peut attendre, une carte *unique* risque d’y passer…

#### 📌 Convention 13a – *Jouabilité implicite* (en anglais : Good touch convention)

<a href="#convention-13a"></a>

Si une carte est marquée dans votre main (même depuis longtemps), vous pouvez considérer qu’elle est *utile*. Dans ce cas, elle 
va devenir ***jouable*** implicitement.

Cette convention permet d’économiser beaucoup de jetons indices au cours d’une partie, mais nécessite que l’équipe marque un minimum
de cartes *inutiles* collatérales.

Ex :

- Au début de la partie, on vous marque trois `1` dans votre main. Considérez que si on les a tous marqués, c’est qu’ils sont ***jouables*** 
(sauf si bien sûr tous les `1` ont déjà été posés).
- Si en fin de partie, le `G4` a été posé et que vous avez une carte marquée `vert` en main, considérez que c’est le `G5`.
- Si vous avez une carte marquée `2` en main et que tous les `1` ont été posés, considérez-la comme ***jouable***.

Attention, poser une telle carte présente quand même un risque, alors préférez-lui une autre carte « explicitement ***jouable*** ».

De plus, laissez l’opportunité à vos partenaires d’indiquer que la carte en question n’est justement **pas** ***jouable***, au besoin.

#### 📌 Convention 13b – Explicitation

<a href="#convention-13b"></a>

Comme les cartes marquées deviennent implicitement ***jouables***, si elles ne le sont pas (et donc *inutiles*), vous devez l’indiquer à 
leur propriétaire.

Cela a deux intérêts : faire défausser une carte *inutile* qui pollue la main de quelqu’un, et éviter une future bombe de sa part.

Ex :

- Au début de la partie, Bob a le `G1` et le `B1`, et Carole le `Y1`, le `R1` et le `B1`. On leur a marqué chacun·e leurs `1`.
- Je donne l’indice `bleu` à Carole, pour marquer sa carte qui fait doublon.

[![Je marque la carte inutile](images/explicitation.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2C(G)1%2CXX%2C(B)1%2CXX%7CXX%2C(Y)1%2CXX%2C(R)1%2C!(B)1%2C%3C-B&playersName=Bob%2CCarole)

- Elle pourra ainsi s’en défausser et laisser Bob poser le `B1` à sa place.

#### Conclusion

Vous savez créer et *répondre* à une *(longue) finesse renversée*. Vous savez comment tirer parti de la *jouabilité implicite* des cartes.

Il vous reste juste à savoir créer une *finesse* quand plusieurs cartes se trouvent chez une même personne. Rendez-vous au chapitre suivant.


### Avancé – niveau 3

<a href="#avancé-niveau-3"></a>

Ce niveau arrive naturellement à force de jouer des *finesses* et *renversées*.

L’équipe est désormais efficace avec les indices. Elle peut utiliser l’extension **Poudre noire**, qui ajoute difficulté et complexité.

#### 🔧 Technique 4 – Mono-finesse (en anglais : *self-finesse*)

<a href="#technique-4"></a>

Quand on marque une carte pour une *finesse*, **plusieurs** cartes *manquantes* peuvent être dans la main d’une même personne.

Exemples : 

- Aucun `2` n’est ***jouable***, Bob a les `B1` et `B2`, dans l’ordre, en ***position finesse*** et personne d’autre n’a de `1` en 
***position finesse***. Je marque `2` le `B2`. Bob sait qu’il s’agit d’un indice *offensif* et regarde la main de ses partenaires, mais ne voit aucun `1` en ***position finesse***. Il en déduit qu’il a lui-même le `1` et le `2` et les pose dans l’ordre.

[![Marquer le 2 fait poser le 1](images/mono-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(B1)%2C!(B2)%2CXX%2CXX%2CXX%2C%3C-2&highestCardsPlayed=BX)

Notez que si le `2` et le `1` sont inversés dans la main, cela fonctionne également.

- Le `G1` a été posé. Aucun `G2` et `G3` ne sont visibles pour Bob. Il a dans sa main trois cartes marquées `vertes`, mais il n’en 
connaît pas les valeurs (qui sont dans l’ordre `G2`, `G3` et `G4`). Je marque la plus ancienne avec `4`. Il comprend que les trois 
cartes se suivent et les pose une à une.
- Même situation, mais les cartes sont dans cet ordre : `G2`, `G4` et `G3`. Je donne l’indice `4`. Bob comprend qu’il a le `2` et le `3` également, 
et dans quel ordre les poser.

[![Marquer 4 fait poser 3 cartes](images/mono-finesse-longue.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CG(2)%2C!G(4)%2CG(3)%2C%3C-4&highestCardsPlayed=G1)

- Le `R2` a été posé et aucun `4` n’est ***jouable***. Bob a une carte marquée `rouge` en main et vient de piocher le `4` correspondant. 
Je le marque d’un indice `4`. Comme personne d’autre n’a de `3` en ***position finesse***, il comprend que c’est lui qui a la carte manquante.
Sa carte `rouge` étant en ***position finesse*** pour un `3`, il la pose, suivi du `4` au tour suivant.

[![Marquer le 4 fait poser le 3](images/mono-finesse-en-desordre.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C!(R4)%2CXX%2CXX%2CR(3)%2CXX%2C%3C-4&highestCardsPlayed=R2)


Cela fonctionne **uniquement** :

- Si la carte marquée n’est ni ***jouable***, ni ***en danger***, du point de vue de la personne qui reçoit l’indice.
- Si personne d’autre n’a de carte *manquante* candidate pour rendre la carte marquée ***jouable***.

Ce type de *finesse* est bien sûr combinable avec les *longues finesses* et *longues renversées*. 

Exemple complexe :

- Je marque `5` la carte déjà marquée `rouge` de Bob, alors qu’aucune carte `rouge` n’a été posée, et que mon `R3` est connu.

[![On a à la fois finesse, renversée et mono-finesse](images/mono-finesse-complexe.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CR3%2CXX%7C(R4)%2CXX%2CXX%2CXX%2C!R(5)%2C%3C-5%7C(R1)%2C(R2)%2CXX%2CXX%2CXX&highestCardsPlayed=RX)

- Bob sait qu’il s’agit d’un indice *offensif* et cherche les cartes *manquantes* pour rendre son `5` ***jouable***. Il voit les `R1` et 
`R2` dans le bon ordre en ***position finesse*** chez Carole. Avec mon `R3`, il manque encore le `R4`. Il comprend qu’il l’a en 
***position finesse*** et patiente en défaussant une carte.
- Carole sait que l’indice `5` était *offensif*, mais ne voit pas les `R1` et `R2` manquants. Elle en déduit qu’ils sont en ***position finesse***
dans sa main et pose le `R1`.
- Aux tours suivants, l’équipe pose les autres cartes `rouges` dans l’ordre jusqu’au `5`, y compris Bob qui s’est rappelé la 
nouvelle position de son `R4` suite à sa défausse.

**Même mémo** : « Si je ne vois pas les cartes manquantes, alors c’est moi qui les ai, en ***position finesse***. ».

#### 📌 Convention 14a –  Priorité entre mono-finesse et indice *offensif* *direct*

<a href="#convention-14a"></a>

Une *mono-finesse* **doit** être différenciable d’un indice *offensif* *direct*.

Ex :

- Le `G2` est ***jouable*** et Bob a les `B1` et `B2` non marqués, en ***position finesse***.
- Je marque `2` son `B2`.

[![L’indice 2 semble direct](images/mono-finesse-invalide.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(B1)%2C!(B2)%2CXX%2CXX%2CXX%2C%3C-2&highestCardsPlayed=G1)

- Bob voit que le `G2` est ***jouable***, mais cherche un `1` en ***position finesse*** chez ses partenaires, au cas où il y aurait
une renversée. Il n’en voit pas, considère donc que son  `2` est le `G2` et cause une bombe en essayant de le poser.

Par convention, on interdit donc toute *finesse* ayant une partie mono, si la carte marquée peut paraître ***jouable*** et qu’aucune autre carte
n’a encore été posée en *réponse* à l’indice.
Sans ça, chaque indice pourrait être une *mono-finesse* et on ne saurait jamais si l’on doit poser la carte marquée ou une autre d’abord.

#### 📌 Convention 15a – Extension Poudre Noire (`Bk5`) (en anglais : *Black powder extension*)

<a href="#convention-15a"></a>

Si vous jouez avec l’extension **Poudre noire**, vous ne pouvez pas marquer les cartes `noires` via des indices *couleur* ,
et vous devez les poser dans l’ordre inverse : du `5` au `1`.

Un `5` est donc ***jouable*** dès le début de la partie (le noir). Si l’on marque un `5` qui n’est **pas** ***en danger***, c’est par conséquent 
le `Bk5` (ou éventuellement une *finesse*).

L’indice doit alors être considéré comme *offensif* et non *défensif*.

[![L’indice est offensif](images/cinq-noir.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2C!(Bk5)%2CXX%2C%3C-5)

#### 📌 Convention 15b – Extension Poudre Noire (`Bk1` non visible)

<a href="#convention-15b"></a>

Si vous jouez avec cette extension, le `1 noir` est *unique*. Il peut donc être ***en danger*** dès le début de la partie. 

Quelqu’un à qui l’on marque des cartes `1` dont une en ***position défausse*** ne doit pas poser cette dernière. Il peut poser les 
autres (s’il reste des `1` à poser), mais doit attendre, soit un indice *offensif* concernant son dernier `1`, soit de voir le `Bk1` dans 
une autre main.

[![Un Bk1 en position défausse est marqué avec d’autres 1](images/un-noir.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2C!(G1)%2C!(R1)%2CXX%2C!(Bk1)%2C%3C-1)

#### 📌 Convention 15c – Extension Poudre Noire (`Bk1` visible)

<a href="#convention-15c"></a>

Quelqu’un à qui l’on marque des cartes `1` dont une en ***position défausse*** **doit** commencer par cette dernière s’il voit le `Bk1` 
dans une autre main (après avoir *répondu* aux finesses éventuellement).  
Cela permet à la personne qui l’a en main de le savoir et de se mettre en garde pour la suite.

**Astuce** : si deux personnes ont une carte `1` en ***position défausse*** et que la première défausse plutôt que de poser son `1` 
(de peur qu’il ne soit `noir`), la seconde personne peut considérer que le sien n’est **pas** `noir` et le poser.

#### 📌 Convention 11b – *Extraction* avec Poudre Noire

<a href="#convention-11b"></a>

Pour faire poser une carte `noire` quand une autre de même valeur est plus récente, une correction est nécessaire.
On marque alors l’autre carte via un indice `couleur`. Cela ne doit pas être considéré comme une finesse, mais comme une *extraction*.

Ex :

- Le `Y1` a été posé. Le `Bk3` est ***jouable*** et se trouve au milieu de la main de Carole, qui a aussi un `Y3` en ***position finesse***.
- Je les marque tous les deux `3`.

[![Extraire un Bk3](images/correction-avec-poudre-noire.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2CXX%7C!(Y3)%2CXX%2C!(Bk3)%2CXX%2CXX%2C%3C-3&highestCardsPlayed=Bk4%2CY1)

- Bob comprend que pour faire poser le `Bk3` deux indices sont nécessaires. Il ajoute un indice `jaune` en *correction*.
- Carole vient de recevoir deux indices non *défensifs*. Elle ne voit pas de `Y2` et en déduit donc que l’un de ses `3` est ***jouable*** 
immédiatement. Elle pose son `Bk3`. 

Bob considère qu’il n’y a pas de *finesse* ici, car je n’ai pas donné l’indice `jaune` qui aurait été le plus évident pour ça. Il me fait 
confiance et suppose que je n’avais pas d’autre choix pour aider à faire poser le `Bk3`.

Une autre ambiguïté peut se poser pour Carole, si l’indice de correction marque une nouvelle carte en plus. 
Cela nécessite une convention : le premier indice est prioritaire. Le second le **corrige** pour l’aider à faire poser l’une des cartes marquées.

Ex :

- Même situation que pour l’exemple précédent, mais cette fois l’indice `jaune` marque la seconde carte de la main de Carole.

[![Une carte collatérale est marquée](images/marquage-collateral-avec-poudre-noire.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2CXX%7C!%3F(Y3)%2C%3F(YX)%2C!(Bk3)%2CXX%2CXX%2C%3C-3%2CY&highestCardsPlayed=Bk4%2CY1)

- Carole sait qu’elle a reçu un indice `3` puis un indice `jaune` dans cet ordre. Elle en déduit que le second indice **corrige** le premier. 
C’est donc un 3 qu’elle doit poser : son `Bk3`.

Si Carole avait en main le `Y3` puis le `Y2`, j’aurais dû indiquer `jaune` à la place. L’indice de correction `3` aurait indiqué que deux cartes `jaunes` étaient ***jouables***.

#### 💡 Astuce 9 – Une finesse pour deux

<a href="#astuce-9"></a>

Si plusieurs cartes identiques sont en ***position finesse*** chez plusieurs partenaires, évitez de donner un indice pouvant faire croire 
à une *finesse*. Laissez-les se donner l’indice mutuellement.

Exemple 1 : 

- Bob et Carole ont la même carte ***jouable*** en ***position finesse*** : le `G2`.

[![On dirait une finesse](images/doublon-sans-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(G2)%2CXX%2CXX%2CXX%2CXX%7C(G2)%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=G1)

Option 1 :

- Je marque `G` la carte de Bob. Il voit le `G2` de Carole et considère qu’il s’agit d’une *renversée* et qu’il a le `G3`. Il ne pose pas sa 
carte, attendant d’abord celle de Carole.
- Carole ne pose pas son `G2` puisque c’est celui de Bob qui est marqué.
- Au tour suivant, Bob pose finalement sa carte. Puisqu’il n’y a pas eu de *réponse*, c’est qu’il n’y avait pas de *finesse* et que c’était un indice
*direct*. Mais, on a perdu un tour dans l’affaire.

Option 2 :

- Je marque `G` la carte de Carole.
- Bob voit un indice *direct* pour Carole et défausse.
- Carole pose son `G2`.

L’option 2 semble plus intéressante, et voyons cet autre exemple, lorsque l’indice est bien une *finesse* :

- Même situation, mais cette fois David a le `G3` en plus.

[![On ignore qui a la carte manquante](images/doublon-avec-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(G2)%2CXX%2CXX%2CXX%7C(G2)%2CXX%2CXX%2CXX%7C!(G3)%2CXX%2CXX%2CXX%2C<-G&highestCardsPlayed=G1)

- Je marque le `G3` de David.
- Bob voit le `G2` manquant chez Carole, et considère donc que l’indice n’est pas pour lui. Il défausse.
- Carole pose son `G2`.

Donner l’indice de l’option 1 du premier exemple est clairement une mauvaise idée.  
L’option 2 et l’exemple 2 sont très similaires, et dans ces deux situations, Carole devrait faire confiance à Bob et poser sa carte. S’il 
n’a pas donné de *réponse*, c’est qu’il n’y avait pas de *finesse* pour lui.

Cela paraît logique, mais dans le feu de l’action, on a vite fait de douter de nos partenaires, et si l’équipe fait régulièrement des erreurs, 
c’est compréhensible. Souvent, Carole attendra un tour de plus « au cas où », afin de laisser une autre chance à Bob de jouer. Bref,
pas mieux que l’option 1, finalement.

Il reste l’option 3 : **laisser Bob donner l’indice lui-même**. Comme il ne voit pas son propre `G2`, sa carte est retirée de l’équation et 
il n’y a plus d’ambiguïté du point de vue de Carole.

#### 📌 Convention 16 – Priorité entre les positions défausse et finesse

<a href="#convention-16"></a>

Il y a un cas particulier où la carte ***jouable*** n’est **pas** celle en ***position finesse***, mais celle en ***position défausse*** : 
quand on vous donne un indice *valeur* marquant plusieurs cartes, potentiellement toutes ***jouables***.

Ex : 

- Tous les `2` ont été posés et l’on marque deux cartes `3` dans ma main, dont une en ***position défausse***. Je pose d’abord cette dernière.
- On marque trois `1` dans ma main en début de partie, dont un en ***position défausse***. Je pose d’abord celui-ci.

Cette convention permet de limiter les cartes *inutiles* dans les mains de l’équipe. Si l’on voulait me faire poser celle en 
***position finesse***, il suffisait d’attendre que je défausse ma carte en ***position défausse*** avant de me donner l’indice. 
Ou, tout simplement de me donner un indice couleur pour ne marquer qu’elle.

[![Le W1 est posé en priorité](images/position-defausse-prioritaire.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C!(G1)%2CXX%2C!(R1)%2CXX%2C!(W1)%2C%3C-1&highestCardsPlayed=G1%2CB1%2CY1%2CR1%2CWX)

De plus, cela offre un moyen supplémentaire de faire poser une carte en ***position défausse***, quand un indice *offensif* *couleur* 
ne le permet pas. (Typiquement, prenez le même exemple, avec une autre carte `blanche` plus récente dans la main de Bob).

Attention, cela ne fonctionne que si **toutes** les cartes marquées sont potentiellement ***jouables***. Si ce n’est pas le cas, on est dans une configuration classique. 
De plus, **les indices *couleur* ne sont pas concernés par cette convention**.

Ex : Tous les `2` ont été posés, **sauf** le `R2`, et l’on marque deux cartes `3` dans ma main, dont une en ***position défausse***. 
Si au moins un `3` est *unique*, et peut être ***en danger*** dans ma main, alors je considère cet indice comme *défensif*,  et ne 
pose aucun de mes `3`. 
Sinon, c’est que l’indice est *offensif* et je pose mon `3` en ***position finesse*** comme d’habitude.

#### 📌 Convention 17 – Pas de réponse

<a href="#convention-17"></a>

Si vous recevez un indice ressemblant à une *finesse* ou à une *renversée*, mais que personne n’y *répond*, fiez-vous à cet adage : « pas de *réponse*, pas de *finesse* ». Votre carte marquée est donc ***jouable***.

Dans les faits, puisqu’une erreur d’inattention peut toujours arriver, vous pouvez temporiser un tour de plus (mais en évitant de défausser). 
Qui sait, peut-être que la personne se rendra compte de son oubli, ou que quelqu’un lui redonnera un indice pour l’aider.  
Si rien de plus ne se passe, faites confiance à votre équipe et posez votre carte.

Cette situation peut se produire à cause de la personne ayant donné l’indice.

Ex :

- Je marque `3` le `B3` ***jouable*** de Bob, alors qu’un `G2` ***jouable*** est présent en ***position finesse*** dans la main de David.

[![Mon indice ressemble à une renversée](images/pas-de-reponse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2C!(B3)%2CXX%2CXX%2C%3C-3%7CXX%2CXX%2CXX%2CXX%7C(G2)%2C(Y1)%2CXX%2CXX&highestCardsPlayed=B2%2CG1%2CYX)

- Bob suppose légitimement qu’il s’agit d’une renversée `G`, et défausse. Mon indice n’est donc pas très bon, puisqu’il aurait
eu le même effet si je l’avais donné un tour plus tard.
- Carole marque le `Y1` de David, qui le pose.
- À son tour, Bob fait confiance à David en considérant qu’il n’y a pas eu d’oubli et pose son `B3`.

Cette situation de flottement et d’ambiguïté pour Bob est due à mon indice. Si je lui avait donné un indice *couleur* ou attendu que le `G2` 
de David ne soit plus en ***position finesse***, cela aurait été plus clair.

#### 🔧 Technique 5 – Indice pour défausse

<a href="#technique-5"></a>

S’il n’y a rien à faire poser, vous pouvez marquer des cartes *inutiles*. 
Cela permet de protéger temporairement des cartes *utiles*, pas encore ***en danger***, mais en ***position défausse***.

Bien sûr, cela ne fonctionne que si la personne qui reçoit l’indice **est certaine que ces cartes sont inutiles** !

En général, ce genre d’indice est moins intéressant qu’un autre faisant poser des cartes. Mais, quand il marque beaucoup de cartes, 
il assure autant de défausses sans risque.

Ex :

- En milieu de partie, tous les `1` ont été posés, mais Bob en a trois en main en ***position finesse***.
- Je lui donne l’indice `1`, pour lui indiquer de les défausser en priorité, prolongeant d’autant la durée de vie de ses autres cartes.

Votre partenaire va également en déduire que ses cartes en ***position défausse*** sont *utiles*. À éviter, donc, si ce n’est pas le cas.

#### 🔧 Technique 6 – Indice *transparent* (en anglais : *blank/empty clue*)

<a href="#technique-6"></a>

Si vous êtes obligé·e de donner un indice (ex : s’il y a 8 jetons), mais qu’il n’y a rien, ni ***en danger***, ni à faire poser, donnez un indice 
qui ne marque **aucune** carte. Cela sera presque *transparent*, et évitera que quelqu’un ne l’interprète comme une *finesse*.

Ex : indiquez à quelqu’un qu’il n’a aucun `5`, ou aucune carte `verte`. 

Cela n’est pas inutile et peut permettre de faire deviner une carte marquée.

Ex : Quelqu’un a reçu un indice *défensif* sur un `4`, alors que seuls les `B4` et `R4` étaient *uniques*. 
Si je lui donne l’indice `rouge` et que cela ne marque aucune de ses cartes, il peut en déduire que son `4` est `bleu`.

#### 💡 Astuce 10 – Défausses consécutives  (en anglais : *double discard*)

<a href="#astuce-10"></a>

Si les deux personnes après vous ont les mêmes cartes en ***position défausse***, celles-ci sont presque ***en danger***, et surtout, 
leurs propriétaires ne le savent pas ! Si vos deux partenaires défaussent, tous les exemplaires de la carte sont perdus.

[![Les deux B3 sont en position défausse](images/defausses-consecutives.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2C(B3)%7CXX%2CXX%2CXX%2CXX%2C(B3)&highestCardsPlayed=G1%2CB1%2CY1%2CR1%2CWX)

Cette situation est plus ou moins complexe à gérer, et demande à être anticipée.

- La meilleure façon de l’éviter est d’avoir déjà marqué l’une des deux cartes en même temps qu’une carte ***jouable***.
- Sinon, avant le tour critique, vous pouvez « voler un indice » à la personne après vous, pour la forcer à défausser au lieu de donner l’indice
elle-même. Ainsi, les deux exemplaires de la carte à protéger ne tomberont pas en ***position défausse*** au même tour.
- Sinon, au tour critique, donnez un indice pour faire poser une carte à l’une des deux personnes. L’autre défaussera sa carte et 
vous pourrez protéger le dernier exemplaire au tour suivant.
- Si personne n’a de carte ***jouable***, la meilleure solution est souvent de ne rien faire. Il y a de bonnes chances pour que l’une des 
deux ait un indice à donner plutôt que de défausser, surtout si vous-même venez de défausser et de piocher une toute nouvelle carte.

Donner un indice *défensif* sur l’une de ces cartes est tentant, mais présente un danger important. Cela risque de ressembler 
à une *finesse* ou une *renversée*, dont la *réponse* causera une bombe. À vous de juger si cette bombe est préférable à la perte
potentielle de ces deux cartes.

**Note** : Il existe une autre possibilité, déconseillée à ce niveau de jeu et [abordée plus tard](#-technique-11-indice-bombeur). 
Vous pouvez donner un « faux indice *offensif* » et causer une bombe. Votre partenaire essaiera de poser sa carte marquée plutôt que de
défausser.

#### 🔧 Technique 2b – Auto-finesse

<a href="#technique-2b"></a>

Si vous devez poser une carte marquée et que la personne qui vous suit a la suivante en ***position défausse***, vous pouvez la marquer
d’un indice *couleur*, comme si vous vous créiez une *finesse*  pour votre propre carte.

Ex :

- Bob a marqué mon `R2` d’un indice `R`, pour créer une *finesse*. Carole y a répondu en posant son `R1`. Bob a le
`R3` en ***position défausse***.
- Si je pose mon `R2`, Bob risque de défausser son `R3`. À la place, je le marque d’un indice `R`.

[![Je marque le R3](images/auto-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2C!R(2)%2CXX%2CXX%2CXX%2C%3C-R%7CXX%2CXX%2CXX%2CXX%2C(R3)%2C%3C-R%7CXX%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=R1)

- Bob sait que je connais mon `R2`. Comme sa carte est en ***position défausse*** et mon indice est `rouge`, il se doute que sa carte
n’est pas encore ***jouable*** et il patiente en faisant autre chose de son tour.
- À mon tour, je pose mon `R2`, et Bob pose enfin son `R3`.

#### 📌 Convention 14b –  Mono-finesse et *jouabilité implicite*

<a href="#convention-14b"></a>

La *jouabilité implicite* se combine avec les *mono-finesses*. Dans ce cas, la *mono-finesse* est prioritaire sur l’indice *offensif* *direct*.

Ex :

- Les `G3` et `W4` ont été posés. Bob a une carte marquée `verte` depuis longtemps en main.
- Je marque un `5` au milieu de sa main.

[![La carte verte devient jouable avec l’indice 5](images/mono-finesse-jouabilite-implicite.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2C!(X5)%2CXX%2CG(4)%2CXX%2C%3C-5&highestCardsPlayed=B2%2CG3%2CR5%2CW4%2CY5)

- La carte que je viens de marquer `5` peut être le `W5` actuellement ***jouable***, ou le `G5` (s’il s’agit d’une *mono-finesse*). 
Bob pose d’abord sa carte marquée `verte`.
- Au prochain tour, il pose son `5`.

Avant l’indice, Bob ne pouvait pas se permettre de poser sa carte marquée `verte` car elle pouvait être soit un `4` soit un `5`. Une fois l’indice
reçu, peu importe la couleur du `5`, l’autre devient ***jouable***, et « au cas où » l’indice serait une *mono-finesse*, Bob ne doit **pas** poser
le `5` en premier mais d’abord sa carte `verte`.

#### 💡 Astuce 5c – Donner un indice, poser ou défausser ?

<a href="#astuce-5c"></a>

Vous devriez toujours préférer donner un indice *offensif* à la personne après vous, plutôt que de poser ou défausser…

…sauf si :

- la personne en question a autre chose à faire que de défausser
- vous avez une carte à poser, attendue par d’autres, en *réponse* à une finesse
- il reste 2 jetons indice ou moins
- cela marque des cartes *inutiles* collatérales

Dans ces cas particuliers, tout dépend du contexte et si l’indice à donner est *urgent* ou non.

Concernant les jetons indice, le dernier est toujours critique. On évite en général de le consommer pour autre chose qu’un indice *urgent*,
au cas où la personne après vous en aurait justement un à donner.  
Si vous donnez votre indice avec un stock de 2 jetons, on retombe alors au seuil critique de 1. S’il y a 3 jetons et plus, en revanche,
on conserve un stock raisonnable et vous ne devriez pas vous priver d’en consommer.

#### 📌 Convention 8b – Prévisibilité et *jouabilité implicite*

<a href="#convention-8b"></a>

Si vous avez une carte devenue ***jouable*** implicitement, vos partenaires s’attendent à ce qu’à votre tour vous la posiez, donniez un indice,
mais **pas** à ce que vous défaussiez.

Cela veut dire que votre équipe est susceptible de ne pas protéger une carte de votre main, même *unique* et en ***position défausse***.
Elle ne la considère pas réellement ***en danger***, puisque vous avez une carte à poser.

Dans cette situation, si vous décidez tout de même de défausser, sachez que vous prenez le risque de perdre une carte *unique*.

Cette convention se veut assez souple et il vous arrivera de faire ce choix, notamment s’il n’y a plus aucun jeton indice et que la personne
après vous en a besoin pour donner un indice *urgent* important.

#### 📌 Convention 9c – *Finesse* vs *défausses consécutives*

<a href="#convention-9c"></a>

Si vous voulez donner une *finesse* en marquant une carte en ***position défausse***, vous **devez** utiliser un indice *couleur* plutôt
que *valeur*, si aucun d’eux ne cause de marquage collatéral.

Ex :

- Le `Y1` a été posé. Bob a un `Y2` en ***position finesse*** et Carole un `Y3` en ***position défausse***.
- Je marque le `Y3` d’un indice `jaune`.
- Bob *répond* à la *finesse* en posant son `Y2`.
- Carole pose son `Y3`.

[![L’indice jaune crée une finesse](images/finesse-vs-defausses-consecutives.png)
](https://hanabi.devnotebook.fr/eyJmaXJld29ya3MiOlt7ImNvbG9yIjoiYmx1ZSIsInZhbHVlIjoxfSx7ImNvbG9yIjoiZ3JlZW4ifSx7ImNvbG9yIjoicmVkIn0seyJjb2xvciI6InllbGxvdyIsInZhbHVlIjoxfSx7ImNvbG9yIjoid2hpdGUifV0sImRpc2NhcmRQaWxlIjp7IndoaXRlIjpbIjMiXX0sInBsYXllcnMiOlt7ImlkIjoiMTU2M2RmYWQtMWNmNi00NzcyLTllYTUtMjhiOTY3ZmZjMzA0IiwibmFtZSI6IkFsaWNlIiwiaGFuZCI6W3siaWQiOiIwNjIwZGY3OC02NDQ1LTQwNjgtYmRiMy03NzUyODMyYTc1NmUifSx7ImlkIjoiZWYwZmZmYzgtN2VmNC00ODI2LWI2ZTYtYzM1Nzc4Yzc1ZTIwIn0seyJpZCI6ImZlYzc4MGIxLWQ1MTYtNGNjYy04NDg2LTZlOTFiZmFkNThlNiJ9LHsiaWQiOiI2YmI5Nzc2OC1lNmQzLTRlZmQtYjdhZi01MGNlM2ZmZThkMWUifV19LHsiaWQiOiJhMThlNzA0Yi1kY2IxLTQ2OGQtOGJiMy05MmUzNWUwNWZiNmYiLCJuYW1lIjoiQm9iIiwiaGFuZCI6W3siaWQiOiJmYTllNGRiYi1iZmY1LTQ0YjEtYmY2NS1mMzA2YWY0ZGU1ZTkiLCJyZWFsVmFsdWUiOjIsInJlYWxDb2xvciI6InllbGxvdyJ9LHsiaWQiOiI4OWJhNGVhNS04ZWZhLTQ1YmEtYmY0NS03YjdlYzE3YjhkZmIifSx7ImlkIjoiMjIwMDdmNzMtYWQ3Yi00N2RjLWFkNjYtNGRiOGY2MzE1MzJjIn0seyJpZCI6ImYwOGUxYmJmLWYyMGEtNDQ1ZS1hZTFiLTFjYjc4YWQzM2FiNiJ9XX0seyJpZCI6ImVkZTI2NGFmLTM0ZDUtNDFmZi04ZjIzLWNiODRiYjNmODcwNSIsIm5hbWUiOiJDYXJvbGUiLCJoYW5kIjpbeyJpZCI6IjA5YTNmZjExLTA0YWUtNGM4NS1iZDE0LWMxYTIxN2ZkZWU1ZiIsInJlYWxWYWx1ZSI6NCwicmVhbENvbG9yIjoiZ3JlZW4ifSx7ImlkIjoiNzdiYzNiNGItMWRmZi00ZDY3LTg5NDMtZDY5ZmY0ZDNiMTRhIiwicmVhbENvbG9yIjoiYmx1ZSIsInJlYWxWYWx1ZSI6MX0seyJpZCI6IjgwMGYyNWU3LTQ5ZTEtNDMxYS05ZWZlLWIxNDVhZmQxMDhjYiIsInJlYWxDb2xvciI6InJlZCIsInJlYWxWYWx1ZSI6Mn0seyJpZCI6IjcyYWE3MWY0LTg5ZjktNDZiMy1iYmQ5LWRmZDc3NjA3OWQyYSIsInJlYWxDb2xvciI6InllbGxvdyIsInJlYWxWYWx1ZSI6MywiY2x1ZTEiOnsiY29sb3IiOiJ5ZWxsb3cifX1dfV19)

Si dans la même configuration, je donne l’indice `3`, à la place, Bob ne répond pas à l’indice.

[![L’indice 3 évite les défausses consécutives](images/defausses-consecutives-vs-finesse.png)
](https://hanabi.devnotebook.fr/eyJmaXJld29ya3MiOlt7ImNvbG9yIjoiYmx1ZSIsInZhbHVlIjoxfSx7ImNvbG9yIjoiZ3JlZW4ifSx7ImNvbG9yIjoicmVkIn0seyJjb2xvciI6InllbGxvdyIsInZhbHVlIjoxfSx7ImNvbG9yIjoid2hpdGUifV0sImRpc2NhcmRQaWxlIjp7IndoaXRlIjpbIjMiXX0sInBsYXllcnMiOlt7ImlkIjoiMTU2M2RmYWQtMWNmNi00NzcyLTllYTUtMjhiOTY3ZmZjMzA0IiwibmFtZSI6IkFsaWNlIiwiaGFuZCI6W3siaWQiOiIwNjIwZGY3OC02NDQ1LTQwNjgtYmRiMy03NzUyODMyYTc1NmUifSx7ImlkIjoiZWYwZmZmYzgtN2VmNC00ODI2LWI2ZTYtYzM1Nzc4Yzc1ZTIwIn0seyJpZCI6ImZlYzc4MGIxLWQ1MTYtNGNjYy04NDg2LTZlOTFiZmFkNThlNiJ9LHsiaWQiOiI2YmI5Nzc2OC1lNmQzLTRlZmQtYjdhZi01MGNlM2ZmZThkMWUifV19LHsiaWQiOiJhMThlNzA0Yi1kY2IxLTQ2OGQtOGJiMy05MmUzNWUwNWZiNmYiLCJuYW1lIjoiQm9iIiwiaGFuZCI6W3siaWQiOiJmYTllNGRiYi1iZmY1LTQ0YjEtYmY2NS1mMzA2YWY0ZGU1ZTkiLCJyZWFsVmFsdWUiOjIsInJlYWxDb2xvciI6InllbGxvdyJ9LHsiaWQiOiI4OWJhNGVhNS04ZWZhLTQ1YmEtYmY0NS03YjdlYzE3YjhkZmIifSx7ImlkIjoiMjIwMDdmNzMtYWQ3Yi00N2RjLWFkNjYtNGRiOGY2MzE1MzJjIn0seyJpZCI6IjQxYzJiMzVmLTBjZDgtNDJhZC05ZDA4LTQxNzIwNzFjNzE3NSIsInJlYWxDb2xvciI6InllbGxvdyIsInJlYWxWYWx1ZSI6M31dfSx7ImlkIjoiZWRlMjY0YWYtMzRkNS00MWZmLThmMjMtY2I4NGJiM2Y4NzA1IiwibmFtZSI6IkNhcm9sZSIsImhhbmQiOlt7ImlkIjoiMDlhM2ZmMTEtMDRhZS00Yzg1LWJkMTQtYzFhMjE3ZmRlZTVmIiwicmVhbFZhbHVlIjo0LCJyZWFsQ29sb3IiOiJncmVlbiJ9LHsiaWQiOiI3N2JjM2I0Yi0xZGZmLTRkNjctODk0My1kNjlmZjRkM2IxNGEiLCJyZWFsQ29sb3IiOiJibHVlIiwicmVhbFZhbHVlIjoxfSx7ImlkIjoiODAwZjI1ZTctNDllMS00MzFhLTllZmUtYjE0NWFmZDEwOGNiIiwicmVhbENvbG9yIjoicmVkIiwicmVhbFZhbHVlIjoyfSx7ImlkIjoiNzJhYTcxZjQtODlmOS00NmIzLWJiZDktZGZkNzc2MDc5ZDJhIiwicmVhbENvbG9yIjoieWVsbG93IiwicmVhbFZhbHVlIjozLCJjbHVlMSI6eyJ2YWx1ZSI6M319XX1dfQ==)

Dans cette situation, Il voit que je n’avais aucune contrainte à donner un indice `jaune`. Si je ne l’ai pas fait, c’est que je n’avais pas le choix :
que mon indice n’indique **pas** une *finesse* mais tente d’éviter les *défausses consécutives* du `Y3`, que Bob a lui aussi en 
***position défausse***.

En général, il est préférable de ne jamais avoir à marquer une carte en ***position défausse*** de cette manière, pour éviter des 
*défausses consécutives*. En effet, s’il n’y avait pas de `3` dans la défausse, Carole croirait que l’indice `3` est *offensif*, et qu’on lui 
donne une *mono-finesse*.  
Cette convention offre toutefois l’opportunité de le faire, si on a besoin, et de manière moins ambiguë.

#### 🔧 Technique 7 – Trop de jetons

<a href="#technique-7"></a>

S’il y a 7 jetons indice en stock et que vous voulez empêcher quelqu’un de défausser sa carte en ***position défausse***, défaussez à sa
place. Le nombre maximum de jetons sera atteint et il devra trouver autre chose à faire de son tour.

Cette technique est pratique, notamment en début de partie, quand il faut attendre qu’une personne pose tous ses `1` alors qu’une autre a des
cartes très utiles, mais ne pouvant pas encore être marquées (ex : un `2` qui ne correspond pas aux `1` visibles).  
Avec un peu de chance, un tour plus tard, cette carte « temporairement sauvée » sera utilisable pour une *finesse* ou marquable avec une 
carte tout juste piochée.

Attention, par souci de prévisibilité et comme vu auparavant, évitez de défausser si vous avez une carte à poser.

#### 💡 Astuce 11 – Évitez les bombes successives

<a href="#astuce-11"></a>

Si la personne avant vous cause une bombe en ayant l’air de *répondre* à un indice, ne risquez pas de bombe supplémentaire.

- Si c’est vous qui avez reçu l’indice « causant » la bombe, c’est sans doute que votre carte marquée n’est **pas** ***jouable***.
- Si ce n’est pas vous, n’essayez pas de *répondre*, vous aussi, à l’indice.

Tout d’abord, peut-être que **personne n’a fait d’erreur** ici. Celle ayant donné l’indice peut avoir une **excellente raison de 
causer une bombe** (pour protéger des cartes, notamment). 

Si ce n’est pas le cas, il est souvent difficile d’être certain·e de l’origine de la bombe. Est-ce que l’indice était mauvais ? Est-ce que 
la personne qui l’a causée a mal compris quelque chose ? Si vous n’en avez aucune idée, ne risquez pas de bombe supplémentaire.

#### 💡 Astuce 12 – Désordre avec *correction*

<a href="#astuce-12"></a>

Vous pouvez lancer une *finesse* ou une *renversée* en marquant des cartes dans le désordre, tant qu’un indice de *correction* 
arrive à temps.

Ex :

- Le `G1` a été posé. Bob a dans cet ordre les `G4` et `G3`, et Carole le `G2` en ***position finesse***.
- Je marque `vert` les deux cartes de Bob. 

[![Les G4 et G3 ne sont pas dans le bon ordre](images/finesse-en-desordre.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2C!(G4)%2CXX%2C!(G3)%2C%3C-G%7C(G2)%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=G1)

- Celui-ci pense avoir le `G3` puis une autre carte `verte`, mais attend pour les poser, car il a vu la *renversée*.
- Carole *répond* à la *renversée* en posant son `G2`.
- J’indique `4` à Bob
- Il comprend que le `G3` n’est pas où il pensait et qu’il a aussi le `4`. Il peut poser les deux un à un.

#### Conclusion

Vous connaissez toutes les variantes de la *finesse* et maîtrisez suffisamment la ***position finesse*** pour pouvoir y *répondre*, peu 
importe combien de cartes *manquantes* elle compte. Vous savez retarder la défausse de cartes *utiles* via des indices de temporisation.

Si vous êtes prêt·e à parier que vos partenaires répondront correctement à ces conventions, vous êtes mûr·e pour le **bluff**. 
Bienvenue au niveau **expérimenté**.


### Expérimenté – niveau 1 

<a href="#expérimenté-niveau-1"></a>

Applicable dès que les membres de l’équipe *répondent* sans problème aux *finesses*, *renversées*… et surtout aux ***mono-finesses***.

Maintenant que les « règles » avancées sont maîtrisées, on peut les outrepasser !


#### 🔧 Technique 8a – *Bluff finesse*  (en anglais : *finesse bluff*)

<a href="#technique-8a"></a>

Si vous marquez une carte pour une *finesse*, la personne après vous doit, en *réponse*, poser sa carte en ***position finesse*** 
pour pallier la carte *manquante*, et ce, **sans connaître réellement** sa carte. Vous pouvez donc la bluffer !

Ex : 

- Le `Y1` et le `G2` ont été posés. Le `Y2` est en ***position finesse*** chez Bob (mais n’est pas marqué) et Carole a le `G4` en main.
- Je donne l’indice `vert` à Carole, pour lui faire poser son `G4`.

[![Exemple de bluff finesse](images/bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(Y2)%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2C!(G4)%2C%3C-G&highestCardsPlayed=G2%2CY1)

- Bob voit qu’il manque le `G3`. Il *répond* en posant sa carte en ***position finesse***, qui s’avère être un `Y2`.
- Carole voit que Bob a posé une carte `jaune`, en *réponse* à un indice `vert`. Elle comprend que Bob a cru à une carte *manquante*, 
ce qui veut dire que la carte verte marquée dans sa main n’est **pas** ***jouable***. C’est donc probablement le `G4` et pas le `G3`. Elle ne la 
pose pas, mais en connaît désormais la couleur, voire la valeur.
- Bob sait que Carole ne risque plus de causer une bombe, que l’indice a reçu une *réponse* et est *résolu*.

Attention, si vous bluffez Bob, en marquant une carte de Carole, celle-ci ne posera pas la carte marquée. Assurez-vous qu’elle n’a pas 
de carte ***en danger*** à ce moment-là !  
En effet, Bob croyant à une *finesse*, ne protégera pas de carte ***en danger***.

Le bluff a deux avantages énormes par rapport à un indice *offensif* classique :

- il ne marque pas la carte ***jouable***, et donc pas non plus les cartes collatérales qui vont avec.
- il marque une autre carte, qui est dorénavant protégée, voire totalement connue de son ou sa propriétaire.

Ex : 

- Bob a trois cartes `4` en ***position finesse***, dont la première est ***jouable*** et la troisième, identique.
- Je marque un `2` en ***position défausse***, *utile*, mais non ***jouable***, à Carole.

[![Les autres 4 gênants ne sont pas marqués](images/bluff-marquage-collateral.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(R4)%2C(X4)%2C(R4)%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2C!(B2)%2C%3C-2&highestCardsPlayed=G2%2CBX%2CR3)

- Bob *répond* au bluff en posant son `4`.
- Les `4` restants ne sont pas marqués, contrairement au `2` de Carole qui ne risque désormais plus la défausse.

Attention, s’il est **facile** de  *répondre* à un bluff, puisque cela semble être une *finesse* (cas de Bob), **assez facile** 
de ne pas causer de bombe, parce qu’on a compris que notre carte n’est pas ***jouable*** (cas de Carole), il est **moins facile** 
de donner un indice de bluff valide (cas d’Alice).

Voici comment éviter les bluffs invalides.

Tout d’abord, vous ne pouvez (quasiment) jamais bluffer une autre personne que celle juste après vous. 
En effet, ne voyant la carte manquante nulle part, la personne qui vous suit a toutes les chances de *répondre* elle-même au bluff à la place 
de celle que vous attendiez.

Par ailleurs, pour être valide, le bluff doit être compréhensible par la personne qui reçoit l’indice. Il faut être sûr qu’elle ne puisse 
pas y voir une longue (mono) *finesse*. Pour éviter cela, bluffez avec un indice *couleur* ou avec un indice d’une *valeur* 
**inférieure ou égale** à celle de la carte à faire poser.

Ex de bluff **invalide** : 

- Je marque `4` la troisième carte de la main de Carole. C’est un `G4`, alors que seul le `G2` a été posé.

[![Le 4 donne l’impression qu’un 3 est manquant](images/bluff-invalide-sans-marquage.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(Y2)%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2C!(G4)%2CXX%2CXX%2C%3C-4&highestCardsPlayed=G2%2CY1)

- Bob *répond* en posant son `Y2`, pensant poser le `G3`.
- Carole a reçu l’indice `4`, mais **n’en connaît pas la couleur**. Comme un `Y2` vient d’être posé suite à l’indice, elle pense que je lui 
ai marqué un `Y4`, et essaie donc de poser la carte en ***position finesse*** qu’elle pense être le `Y3`. Elle cause une bombe. 

On considère ce bluff comme invalide, car il ne peut pas être distingué d’une *longue finesse* classique par Carole. Or, si un même
indice peut donner lieu à plusieurs interprétations différentes, on perd tout l’intérêt des techniques et des conventions : la **prévisibilité**.

Enfin, la personne recevant l’indice doit pouvoir être **sûre** que la carte posée est la *réponse* à l’indice qu’elle a reçu. Pour cette raison,
bluffer pour faire poser une carte déjà marquée est considéré comme invalide.

Ex : 

- Bob a un `Y2` marqué `2` en main. Je marque `rouge` une carte de Carole.

[![La carte R a l’air jouable](images/bluff-invalide-avec-marquage.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2C(Y)2%2CXX%2CXX%2CXX%7CXX%2CXX%2C!(RX)%2CXX%2CXX%2C%3C-R&highestCardsPlayed=G2%2CY1%2CR1)

- Bob pose le `Y2`.
- Carole ne sait pas pourquoi le `Y2` a été posé. Est-ce à cause de l’indice qu’elle a reçu ? Dans ce cas, elle ne doit pas poser 
sa carte `rouge`. Qu’en est-il si elle a un `R2` ***jouable*** ? Peut-être que l’indice était bien pour lui faire poser sa carte et que 
Bob avait suffisamment d’informations pour déduire que son `2` était ***jouable*** ? Cela génère souvent beaucoup trop 
d’incertitudes pour le bénéfice que l’on peut en retirer, et n’est dès lors pas considéré comme valide.

**Note** : lorsqu’une carte est posée en *réponse* à un bluff, et que tout le monde constate qu’elle ne peut **pas** correspondre à la 
carte marquée par l’indice, on dit que le bluff est « *révélé* ».

#### 🔧 Technique 8b – *Mono-bluff*  (en anglais : *self bluff*)

<a href="#technique-8b"></a>

Le *bluff finesse* peut reposer sur une *mono-finesse*. C’est alors la même personne qui reçoit l’indice, y *répond* et *révèle*  le bluff.

Ex :

- Le `Y1` a été posé, aucun `3` n’est ***jouable***. Bob a un `G4` inconnu et ***jouable*** en ***position finesse***, et un `Y3`.
- Je marque son `Y3` d’un indice `3`.

[![La carte marquée et la carte posée en réponse sont dans la même main](images/mono-bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(G4)%2C!(Y3)%2CXX%2CXX%2CXX%2C%3C-3%7CXX%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=G3%2CY1%2CR1)

- Bob sait qu’il s’agit d’un indice *offensif* et qu’un `2` est manquant. Il ne le voit nulle part, et pose donc sa carte la plus récente : son `G4`.  

Une carte non marquée et ne correspondant pas à l’indice a été posée en *réponse*, le bluff est donc *révélé*.

#### 🔧 Technique 9 – *Défausse panique*  (en anglais : *screaming discard*)

<a href="#technique-9"></a>

Si vous avez un carte ***jouable*** connue, mais que vous défaussez plutôt que de la poser, alors cela constitue un signal d’alerte pour la
personne après vous, l’encourageant à **ne pas défausser**. On appelle cela une « *défausse panique* ».

Deux cas typiques :

- Elle a une ou plusieurs cartes ***en danger*** et il ne vous reste pas assez de jeton(s) indice pour l’en informer.
- Elle doit défausser pour récupérer le jeton indice nécessaire à la personne après elle, pour protéger une carte ***en danger***.

Si votre partenaire ne voit pas de raison à votre défausse en regardant les mains de l’équipe, il ou elle doit se forcer à donner un indice, plutôt
que de défausser. Une fois le jeton récupéré, on pourra lui donner un indice explicite pour confirmer que sa carte en ***position défausse***
était bien ***en danger***.

#### 📌 Convention 18 – *Défausse gentleman*  (en anglais : *gentleman discard*)

<a href="#convention-18"></a>

Si quelqu’un défausse une carte qu’il sait ***jouable***, c’est qu’il en voit un autre exemplaire en ***position finesse*** dans la main 
de ses partenaires. La personne avec le deuxième exemplaire devra donc le poser, à la place de celle ayant défaussé.

Exemple :

- Je connais mon `Y3` et mon `B4`, tous deux ***jouables***.
- Je défausse mon `B4`.
- Carole comprend qu’elle en a un en ***position finesse***, et le pose, au lieu de défausser son `G2`.
- Au tour suivant, je défausse mon `Y3`.
- Bob, dont on avait protégé le `W3`, pose son `3` en ***position finesse***.

[![Mon Y3 fait doublon](images/defausse-gentleman.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CY3%2CB4%2CXX%7CXX%2CXX%2C(Y)3%2C(W)3%7C(B4)%2CXX%2CXX%2C(G2)&highestCardsPlayed=B3%2CR1%2CGX%2CY2%2CWX&discardedCards=W3)

Cette astuce assure une défausse sûre (l’autre personne aurait peut-être défaussé une carte *utile*), et rend immédiatement un jeton indice. 

Cela ajoute une complexité malvenue si la personne que l’on cible a déjà plein de cartes à poser ou d’indices à donner, ou si 
ses cartes en ***position défausse*** sont *inutiles*.

#### 🔧 Technique 10 – *Semi-bluff*

<a href="#technique-10"></a>

On peut parler de *semi-bluff* lorsqu’on fait poser, à partir d’un indice, une ou plusieurs cartes marquées qui ne correspondent pas, 
puis compléter la *finesse* correspondant à l’indice.

Ex :

- Le `G1` et le `B1` ont été posés. Bob a un `B2` et un `G2` marqués `2` dans sa main, mais ignore qu’ils sont ***jouables***.
- Je marque `vert` le `G3` de Carole.

[![La carte manquante n’est pas celle en position finesse](images/semi-bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2C(B)2%2CXX%2C(G)2%7C!(G3)%2CXX%2CXX%2CXX%2CXX%2C%3C-G&highestCardsPlayed=G1%2CB1)

- Bob croit *répondre* à la *finesse* en posant son `2` en ***position finesse***. Il s’avère que c’est le `B2` !
- Carole ne pose pas sa carte verte puisqu’elle sait qu’il y a déjà un `G2` dans la main de Bob.
- Bob continue de poser ses `2` en ***position finesse*** jusqu’à tomber sur celui correspondant au `G3`.

Cette technique est souvent utile en début de partie, quand on a indiqué des `1` à quelqu’un, qui en a pioché un nouveau en les posant.

Ex :

- J’avais marqué `1` le `R1` et le `B1` de Bob. Il a posé le `R1` et a pioché le `G1`.
- Je marque `vert` le `G2` de Carole.

[![La carte manquante n’est pas marquée](images/semi-bluff-sans-marquage.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(G1)%2CXX%2CXX%2C(B)1%2CXX%7CXX%2CXX%2C!(G2)%2CXX%2CXX%2C%3C-G&highestCardsPlayed=R1%2CGX%2CBX)

- Bob croit *répondre* à la *finesse* en posant son `1` en ***position finesse***. Il s’avère que c’est le `B1` !
- Carole ne pose pas sa carte verte puisqu’elle sait qu’il y avait un `G1` en ***position finesse*** dans la main de Bob,
**au moment où elle a reçu l’indice**.
- Bob continue de poser ses `1` en ***position finesse*** jusqu’à tomber sur celui correspondant au `G2`. Il se rappelle que le `G1` a changé
de place dans sa main depuis mon indice, suite à la pose de son `B1`.

Pour rappel, un bluff pour faire poser une carte déjà marquée (comme le `B1` dans cet exemple) n’est pas autorisé. Une fois le `B1` posé, 
le `G2` est toujours « en attente » et l’indice n’est pas *résolu* tant que le `G1` n’a pas été posé.

#### 💡 Astuce 5d – *Vol* d’indice  (en anglais : *skip/steal*)

<a href="#astuce-5d"></a>

Si vous donnez un indice à une personne *éloignée*, alors que celle qui vous suit aurait pu le donner elle-même, on dit que vous lui 
*volez* l’indice.

Voici un exemple de vol courant et tout à fait légitime :

- Carole a un `G2` ***jouable*** qu’elle ne connaît pas, en ***position finesse***, et David un `5` ***en danger***.
- Je donne l’indice `5` à David.

[![Mon vol est un indice défensif](images/vol-legitime.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7C(G2)%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2C!(X5)%2C%3C-5&highestCardsPlayed=G1)

On peut considérer mon indice comme un *vol*, puisque Bob et Carole auraient pu le donner à ma place.  
Mais je sais que Carole aura un `G2` à poser, si Bob lui donne un indice. Autrement dit, qu’ils sont potentiellement tous les deux occupés.  
Mon indice permet à Bob de créer une *finesse*, et même de bluffer. Sans cela, Carole n’aurait pas pu tout de suite *répondre* en
posant sa carte, car elle aurait du protéger le  `5`.  
Par contre, il aurait été dommage de marquer moi-même le `G2`, laissant Bob marquer le `5`. Rien ne me dit qu’il n’a pas de meilleur indice
que moi à donner, pour faire poser le `G2`.

Certains  *vols* peuvent paraître beaucoup moins légitimes. Par exemple, si dans l’exemple précédent j’avais marqué le `G2` alors que David
n’avait aucune carte ***en danger***, j’aurais clairement *volé* Bob.

Il n’y a pas de règle établie indiquant à la personne spoliée ce qu’elle doit faire à la place de donner elle-même l’indice, et cela fait débat
dans la communauté du jeu. En tout cas, elle doit vous faire confiance, puisque vous avez préféré donner l’indice à sa place. 

Les raisons d’un *vol* peuvent être très diverses :  vous voulez que la personne **défausse**, qu’elle **pose** une carte, vous ne voulez **pas** défausser vous-même, vous pensez qu’elle a un **autre indice** que le vôtre à donner…

- En général en tout début de partie, cela vous évite surtout de défausser des cartes (typiquement des `2`), quand vous voyez que la personne
après vous a des cartes peu utiles en ***position défausse*** (typiquement des `4`).  
- En milieu de partie, quand votre main est pleine, ou presque, de cartes marquées non ***jouables***, il est courant de *voler* des indices le
temps que vos partenaires renouvellent un peu leur main.
- C’est courant également, à tout moment dans la partie, si donner l’indice à la place de votre partenaire lui laisse le temps de poser plusieurs
de ses cartes.

Attention, en  *volant* un indice, vous empêchez éventuellement la personne après vous d’en donner un meilleur. Typiquement, peut-être 
qu’elle aurait aimé bluffer. Le vol d’une telle opportunité doit toujours être mûrement réfléchi.

**Remarque** : Si la personne avant vous, vous *vole* un indice **pour faire poser une carte** à celle qui vous suit, réfléchissez si cela vous invite plutôt à poser une carte, à défausser ou à donner un autre indice. Si vous choisissez de poser une carte, **celle-ci doit être marquée**. En effet, si vous posiez votre plus récente, non marquée, comment la personne recevant l’indice pourrait-elle comprendre que 
sa carte est ***jouable*** et qu’il ne s’agissait pas d’un bluff ?

#### 💡 Astuce 13a – *Indice de temporisation*

<a href="#astuce-13a"></a>

Si vos partenaires ont beaucoup de cartes à poser, mais pas vous, et qu’il y a déjà beaucoup de jetons indice en stock, il peut être intéressant
de donner un indice, même *transparent*. Cela vous évite la défausse d’une éventuelle carte *utile*, laissant un tour de plus aux autres pour
poser leurs cartes et, qui sait, rendre les vôtres ***jouables*** !

Un indice de ce genre doit rester ponctuel. Peut-être que votre équipe aimerait vous voir défausser.  
Selon le contexte et leur expérience, vos partenaires apprécieront ou trouveront l’indice malvenu.

#### 💡 Astuce 14 – Répartir les cartes jouables au sein de l’équipe

<a href="#astuce-14"></a>

Dans la mesure du possible, évitez que toutes les cartes ***jouables*** soient concentrées dans une même main (voire plusieurs).  
Ex : qu’une personne (ou deux dans une partie à 5) n’ait 3 cartes ***jouables*** marquées alors que les autres n’en ont aucune.

Le risque de cette situation, c’est qu’au fur et à mesure que les personnes ayant des cartes ***jouables*** les posent, elles en piochent
de nouvelles ***jouables***. Pendant ce temps-là, les autres leur donnent de nouveaux indices et la répartition des cartes n’évolue pas. 
C’est particulièrement problématique en fin de partie, quand il ne reste que quelques tours de jeu.

Ex : S’il ne reste que 2 tours de jeu et que quelqu’un a quatre cartes ***jouables*** en main, il n’aura pas le temps de toutes les poser. 
En revanche, si chaque membre de l’équipe en avait une, cela ne prendrait qu’un tour ou deux.

Pour prévenir cette situation, évitez de marquer une carte de quelqu’un ayant déjà deux cartes ***jouables*** en main (sauf si l’indice 
est *urgent*). Donnez plutôt un indice à la personne après lui, le temps que les cartes ***jouables*** soient posées.

Même en début de partie, garder ce principe à l’esprit peut être utile. Si quelqu’un a quatre `1` en main, il peut être préférable de ne 
lui en faire poser que deux (via des indices *couleur*, des *finesses*…) pour faire poser les autres par une autre personne.
De cette manière, les `1` seront posés plus vite, évitant surtout à d’autres de défausser des cartes *utiles* en attendant.

#### 🔧 Technique 11 – *Indice bombeur*

<a href="#technique-11"></a>

Si vous devez absolument protéger une carte ***en danger*** de la personne suivante et que vous ne pouvez pas (ou qu’il n’y a que 
vous pour plusieurs cartes ***en danger***), il peut être préférable de causer une bombe. C’est très facile, marquez une carte 
de la personne pour qu’elle la croie ***jouable*** (de préférence *inutile*, tant qu’à faire).

Même si c’est très rare, cette opération peut se montrer très rentable. Si elle vous permet de sauver un `2`, par exemple.

#### 📌 Convention 19a – *Promesse* de bluff

<a href="#convention-19a"></a>

On appelle « *promesse* de bluff » le ***positionnement finesse*** implicite de la carte marquée.  
Si je crée un *bluff finesse* en marquant `rouge` une carte alors que seul le `R1` a été posé (et qu’il n’y a pas de `R2` visible), 
son ou sa propriétaire la considère désormais comme un `R3`. L’équipe doit s’attendre à ce qu’il soit posé sans indice complémentaire
une fois le `R2` en place.

Je peux choisir de bluffer **sans respecter de *promesse*** (par exemple, en marquant un `R4` et un `R5` uniques 
en ***position défausse*** à la place).   
La personne après moi *répondra* au bluff comme prévu, et les deux cartes `rouges` *uniques* seront protégées : très pratique sur le
moment ! En revanche, la personne à qui j’ai donné l’indice pensera dorénavant, à tort, avoir un `R3` en main. Un indice de correction devra
donc lui être donné d’ici à ce que le `R2` soit posé. (Une correction simple possible est de marquer un `R3` dans la main d’une autre
personne.)

#### 📌 Convention 19b – Position finesse implicite

<a href="#convention-19b"></a>

La ***position finesse*** pour une carte n’est pas toujours la plus évidente et peut même changer implicitement.

Ex :

- Le `R1` a été posé, mais pas le `R2`. Je fais poser une carte à Bob par un *bluff finesse*, en marquant `rouge` le `R3` de Carole.
- Désormais, pour Carole, cette carte `rouge` est la ***position finesse***  pour le `R3`. **Pas pour le `R2` !**
- Si plus tard, je marque `rouge` le `R4` de Bob pour une *renversée*, Carole devra *répondre* en posant son `R2` 
en ***position finesse*** (ex : sa carte la plus récente), **ensuite seulement** celle marquée `rouge` par le bluff.

Un autre cas assez simple, arrive régulièrement avec les cartes protégées.

Ex :

- Les `Y3` et `G3` sont uniques et aucun `3` n’est ***jouable***. Je marque un `3` en ***position défausse***, à Bob. 
Personne ne *répond* à cet indice : normal, c’était juste pour protéger ce `3`. Plusieurs tours passent, durant lesquels un `B2` est finalement
posé.
- Je marque le `B4` de Carole, d’un indice `B`.
- Bob sait que sa ***position finesse*** pour un `B3` n’est pas sa carte marquée `3` – pourquoi aurais-je marqué un `B3` quand j’ai donné
l’indice `3`, alors qu’il n’y avait pas de `B2`  ? Il *répond* donc à la *finesse* en posant sa carte la plus récente.

Un dernier cas plus délicat est la *perte* de ***position finesse*** avec le temps. C’est très pratique avec l’extension **Poudre Noire**.

Ex :

- On joue avec cette extension. Dès le début, on a marqué `1` trois cartes de ma main, dont une en ***position défausse***.
- Je pose seulement les deux premières, considérant la troisième comme étant un potentiel `Bk1`. À ce stade, tous les `1` sont posés, 
sauf le `G1` et le `Bk1`.
- Si quelqu’un a le `G2` en main et que personne ne le marque `vert` – me faisant poser ainsi mon  dernier `1` via *finesse* – pendant 
plusieurs tours (sans bonne raison), alors je peux le considérer définitivement comme étant le `Bk1`. Il perd ainsi sa ***position finesse*** 
pour le `G1`.
- Si finalement une *finesse* `verte` est déclenchée, et que je dois *répondre* `G1`, je ne poserai alors pas ce « `Bk1` » mais ma carte 
la plus récente.

#### 📌 Convention 20a – *Défausse positionnelle*  (en anglais : *positional discard*)

<a href="#convention-20a"></a>

Si quelqu’un défausse une carte qui n’est ni en ***position défausse***, ni marquée comme *inutile*, c’est pour indiquer une carte
***jouable*** à l’emplacement correspondant dans la main d’une autre personne de l’équipe.

Ex : Je défausse la carte du milieu de ma main, et la personne suivante comprend qu’elle peut poser la carte du milieu de sa main.

Une telle défausse n’est rentable **qu’en fin de partie**, quand on peut compter les cartes *utiles* restantes  et que l’on sait qu’on 
n’en a aucune dans sa main. Cela permet de donner un indice **même quand il n’y a plus aucun jeton !**

Cela permet également d’indiquer la dernière carte ***jouable*** de la partie, quand aucun indice ne le permet.

Ex : 

- Il ne reste plus que Bob à jouer après moi. Je veux lui faire poser son `B4`. Il a en main en ***position finesse*** le `B5` puis le `R4`, puis 
le `B4`.

[![Ma défausse cible la troisième carte de Bob](images/defausse-positionnelle.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(B5)%2C(R4)%2C(B4)%2CXX%2CXX&highestCardsPlayed=B3%2CR5)

- Si je donne l’indice `4`, c’est le `R4` qui se retrouve en ***position finesse***, si je marque `bleu`, c’est le `B5`. À la place, je défausse ma
troisième carte la plus récente.
- Bob comprend que la carte correspondante est ***jouable***, et pose son `B4`.

#### 📌 Convention 20b – *Bombe positionnelle*

<a href="#convention-20b"></a>

Si quelqu’un cause une bombe en tentant de poser une carte **non** ***jouable***, c’est pour indiquer une carte ***jouable*** à
l’emplacement correspondant dans la main d’une autre personne de l’équipe.

Ex : Je tente de poser la carte la plus ancienne de ma main, et cause une bombe. La personne suivante comprend qu’elle peut poser sa 
carte la plus ancienne.

L’objectif est le même qu’avec la *défausse positionnelle*, mais la carte utilisée est cette fois en ***position défausse***, ou marquée *inutile*
(ex : une carte marquée `1` en fin de partie). Une *défausse positionnelle* n’est pas possible pour une telle carte, car elle ressemblerait à une
défausse « normale ».

L’inconvénient de causer une bombe, c’est que cela augmente le stress et rapproche de la défaite. Si l’équipe fait régulièrement des erreurs, 
ne se fait pas assez confiance ou est trop concentrée sur le score, alors la personne cible peut se montrer frileuse et ne pas oser *répondre*
à l’indice pour ne pas causer la défaite.

#### Conclusion

Vous savez outrepasser la convention *finesse* pour bluffer, et faire poser des cartes sans jetons indice (via *défausse gentleman* ou
*positionnelle*). Vous connaissez des moyens de temporiser pour fluidifier la répartition des cartes de votre équipe.

Comment donner un indice *offensif* en marquant des cartes inutiles ? Rendez-vous au chapitre suivant.


### Expérimenté – niveau 2

<a href="#expérimenté-niveau-2"></a>

Applicable dès que les membres de l’équipe savent bluffer. Vous pouvez désormais jouer le mode **Avalanche**, ajoutant une nouvelle
profondeur et complexité au jeu.

Il est temps d’introduire un nouveau type d’indice : la *finesse ordurière* et (spoiler), le bluff associé.

#### 🔧 Technique 12a – *Finesse ordurière*  (en anglais : *trash finesse*)

<a href="#technique-12a"></a>

Si vous marquez une carte comme si elle était ***jouable***, alors qu’elle est *inutile*, c’est que la carte ***jouable*** 
qu’elle « paraît être » pour son ou sa propriétaire, se trouve en ***position finesse*** dans la main d’une personne précédente.

Ex :

- Tous les `1` ont été posés, sauf le `Y1`, que Bob a justement en ***position finesse***.
- Je marque `1` trois cartes (non `jaunes`) de Carole.

[![Exemple de finesse ordurière](images/finesse-orduriere.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(Y1)%2CXX%2CXX%2CXX%2CXX%7CXX%2C!(R1)%2C!(G1)%2C!(G1)%2CXX%2C%3C-1&highestCardsPlayed=B1%2CR1%2CG1%2CW1%2CYX&discardedCards=&playersName=&nbRemainingCardsInDeck=)

- Bob comprend que s’il ne fait rien, Carole va poser son `1` en ***position finesse***, alors qu’il n’est pas ***jouable***.
Il pourrait donner un indice *transparent* `Y`, pour indiquer à Carole qu’elle n’a pas le `Y1`. Mais 2 jetons indice auraient tout de même
été gaspillés pour pas grand-chose. Il devine que c’est lui qui a le `Y1`, et le pose.
- Carole voit que tous les `1` ont été posés. Elle sait maintenant qu’elle a en main trois « **déchets** », 3 cartes *inutiles*, qu’elle peut 
défausser sans risque.

Ce type d’indice a les mêmes avantages que les bluffs :

- Il fait poser une carte, sans la marquer (évitant le marquage collatéral qui va avec).
- Il évite la défausse de cartes *utiles*, en en proposant d’autres à défausser d’abord.

Il est le plus souvent utilisé en marquant `1`, puisque toutes les autres cartes correspondantes doivent avoir été posées. C’est 
toutefois possible avec toute autre *valeur* et même un indice *couleur*.

Attention, cela ne fonctionne que si la personne *éloignée* peut être **sûre** que sa carte est *inutile*.  
Typiquement, si vous jouez avec l’extension **Poudre Noire** et que le `Bk1` n’est pas connu de tous, utiliser la *finesse ordurière* 
avec un indice `1` peut ressembler à un *bluff finesse* classique sur la couleur `noir`. La personne recevant l’indice ignorera si 
son `1` est *inutile* ou non et dans le doute le conservera. On perd alors tout l’intérêt de l’indice : faire poser une carte tout en 
en protégeant d’autres (en indiquant des cartes *inutiles* à défausser).

Par convention, toutes les cartes marquées par l’indice sont considérées comme *inutiles*.

Contrairement au nom qu’on lui donne souvent sur [BGA](https://boardgamearena.com/), il ne s’agit pas d’un bluff. Ce type d’indice 
fonctionne donc **à distance**. Si l’on reprend le même exemple avec une personne de plus et le `Y1` dans la main de Carole, 
cela fonctionne toujours. Je marque le `1` *inutile* de David, Bob comprend que l’indice n’est pas pour lui et c’est Carole qui *répond* 
en posant son `Y1`.

#### 📌 Convention 21 – *Finesse ordurière* et *semi-bluff*

<a href="#convention-21"></a>

La *finesse ordurière* interagit de manière peu intuitive avec la convention du *semi-bluff* et différemment selon le contexte.

Ex : 

- Tous les `3` ont été posés, sauf le `B3`. Bob a une carte marquée `bleue` en main.
- Je marque `3` deux cartes de David (non `B3`).
 
[![La carte jouable est celle marquée](images/finesse-orduriere-sans-semi-bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CB(3)%2CXX%7CXX%2CXX%2CXX%2CXX%7CXX%2C!(G3)%2C!(R3)%2CXX%2C%3C-3&highestCardsPlayed=G3%2CY3%2CB2%2CW4%2CR5)

- Bob comprend qu’il doit poser une carte pour indiquer à David que les siennes ne sont pas ***jouables***. Un seul `3` 
est ***jouable*** et il a une carte marquée qui correspond.
- Il pose donc sa carte marquée `bleue`.
- Arrivée au tour de David, tous les `3` sont posés. Il sait alors que les siens sont *inutiles*.

Mais, que se passe-t-il s’il reste plusieurs `3` à poser ?

Ex : 

- Tous les `3` ont été posés, exceptés le `Y3` et le `B3`. Bob a une carte marquée `bleue` en main et Carole n’a aucun `3`.
- Je marque `3` deux cartes de David (non `Y3` et non `B3`).

[![La carte à jouer n’est pas celle marquée](images/finesse-orduriere-avec-semi-bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(Y3)%2CXX%2CB(3)%2CXX%7CXX%2CXX%2CXX%2CXX%7CXX%2C!(G3)%2C!(R3)%2CXX%2C%3C-3&highestCardsPlayed=G3%2CY2%2CB2%2CW4%2CR5)

- Bob comprend qu’il doit poser une carte pour indiquer à David que les siennes ne sont pas ***jouables***. Il pourrait poser sa
carte marquée `B` en supposant qu’il s’agisse du `3`. Cependant, comme la carte est marquée, cela ne constitue pas une *réponse* 
à l’indice et n’empêche donc pas la bombe.
- Il pose par conséquent sa carte non marquée la plus récente.
- David a vu poser une carte non marquée en *réponse* à l’indice `3`. Cela implique nécessairement que les siennes ne sont **pas** ***jouables***.
Il peut les défausser sans risque.
- Bob ne sait toujours pas si sa carte `bleue` est le `3`.

#### 🔧 Technique 12b – *Renversée ordurière*  (en anglais : *reverse trash finesse*)

<a href="#technique-12b"></a>

Vous pouvez renverser la *finesse ordurière*, pour indiquer des cartes *inutiles* à la personne précédant celle avec la carte ***jouable***.

Exemple typique où cette technique a une plus-value :

- Le seul `1` ***jouable*** est le `Bk1`, en ***position finesse*** dans la main de Carole, qui a aussi un autre `1`, en ***position défausse***. 
- Je marque `1` deux cartes de la main de Bob. 

[![Aucune des cartes marquées ne peut être jouable](images/renversee-orduriere-explicite.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C!(X1)%2CXX%2C!(X1)%2CXX%2CXX%2C%3C-1%7C(Bk1)%2CXX%2CXX%2CXX%2C(X1)&highestCardsPlayed=G3%2CY5%2CB3%2CW5%2CR4%2CBk2)

- Bob voit *l’unique* `Bk1` et sait donc que ses `1` sont *inutiles*. Il en défausse un, ou fait autre chose.
- Carole comprend que j’étais sûr de ne pas causer de bombe en marquant ces cartes : parce qu’elle a le `Bk1`.
- Elle pose son `Bk1` en ***position finesse***.

Si l’on avait donné l’indice `1` à Carole directement, plusieurs de ses cartes seraient marquées. Elle en déduirait alors qu’on 
n’avait pas d’autre choix que de donner cet indice : que son `Bk1` est en ***position défausse***.

Le second avantage de cette technique est le même que pour la *finesse ordurière* : vous indiquez des cartes *inutiles* à la personne à
qui vous donnez l’indice, retardant d’autant la défausse de ses cartes *utiles*.  
Attention toutefois, vous privez ici Bob d’une opportunité de bluff.

Dans cet exemple, la renversée est « explicite » : il n’y a qu’un seul exemplaire de la carte qui est ***jouable***,
donc celles marquées sont **obligatoirement** *inutiles*.  
Mais, une *finesse ordurière renversée* peut également être « implicite » : la personne n’est pas sûre que ses cartes marquées 
sont *inutiles*, mais elle voit celle ***jouable*** en ***position finesse*** dans la main d’un autre membre de l’équipe. Dans le doute, 
elle considère les siennes comme *inutiles* et lance une *défausse gentleman*.

Exemple de *renversée ordurière* « implicite » :

[![Une des cartes marquées pourrait être jouable](images/renversee-orduriere-implicite.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C!(X1)%2CXX%2C!(X1)%2CXX%2CXX%2C%3C-1%7C(G1)%2CXX%2CXX%2CXX%2C(X1)&highestCardsPlayed=GX%2CY5%2CB3%2CW5%2CR4)

Le fait qu’une telle renversée doivent donner lieu obligatoirement à une *défausse gentleman*, relève plus de la convention que 
de la technique. Cela n’est pas vraiment joué sur [BGA](https://boardgamearena.com/).  
Si vous l’y utilisez, assurez-vous que Bob a une vraie plus-value à utiliser la  *défausse gentleman* ou il risque de causer une bombe
en tentant de poser la carte !

#### 🔧 Technique 8c – *Bluff finesse* dans le désordre

<a href="#technique-8c"></a>

Vous pouvez bluffer la personne après vous en marquant des cartes dans le désordre dans la main de quelqu’un d’autre.

Ex : 

- Les `Y1` et `G2` ont été posés et un `G4` a été défaussé. Le `Y2` est en ***position finesse*** chez Bob. Carole a un `G5` 
au milieu de sa main 
et un `G4` en ***position défausse***.
- Je donne l’indice `vert` à Carole.

[![Les deux cartes vertes sont dans le désordre](images/bluff-en-desordre.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(Y2)%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2C!(G5)%2CXX%2C!(G4)%2C%3C-G&highestCardsPlayed=Y1%2CG2&discardedCards=G4)

- Bob voit qu’il manque le `G3` mais que s’il en a un et le pose, Carole essaiera de poser sa carte verte ***position finesse*** 
(le `G5`) et causera une bombe. Il comprend qu’il s’agit d’un *bluff finesse*, me fait confiance et pose sa carte en ***position finesse***.
- Carole voit que le `Y2` a été posé via un indice `vert` et qu’il s’agit d’un *bluff finesse*. Elle ne pose pas ses cartes `vertes`.

Comme tout bluff, cela devient dangereux si Bob ne *répond* pas. S’il n’a aucun moyen de *corriger* l’indice, Bob doit faire
confiance et *répondre*. Réciproquement, c’est peut-être à éviter si, justement, une *correction* est possible pour faire poser autre chose à Carole.

Attention, ce type de bluff « exotique » a un défaut particulier : il ne respecte **pas** la *promesse* de bluff puisque Carole croit 
désormais avoir un `G4` en ***position finesse*** verte.  
Il faut donc être prêt à devoir lui donner un indice de correction, plus tard, pour ne pas qu’elle ne pose la mauvaise carte quand 
le `G4` deviendra ***jouable***.

#### 📌 Convention 10c – Extension Avalanche de couleurs (mode **Avalanche**)

<a href="#convention-10c"></a>

Comme le premier mode introduit par l’extension, celui-ci utilise les **10 cartes multicolores**.

Le gros changement, c’est qu’elles sont, cette fois, **réellement** multicolores. C’est-à-dire en même temps, rouges, bleues, jaunes,
blanches…  
Un indice `rouge` marquera donc à la fois les cartes rouges et les cartes multicolores dans la main de votre partenaire. Même principe pour 
un indice `blanc`, `vert`… Et bien, sûr, on ne peut plus indiquer directement la couleur `multicolore`.

Ce mode complexifie considérablement le jeu, mais facilite un peu la protection des cartes grâce au marquage collatéral.

Par convention, on considère :

- qu’une carte marquée de deux couleurs différentes est marquée `multicolore`.
- que si via un indice *couleur* une carte devient marquée `multicolore`, mais que cela marque aussi une autre carte, c’est cette autre 
carte qui est ***jouable***.

Ex : 

- Bob a en main un `M4` marqué `G` et un `R3` ***jouable***, plus ancien.
- Je marque `rouge` ces deux cartes. Le `M4` devient ainsi marqué `multicolore`, et l’autre carte marquée `rouge`.
- Bob sait que si j’avais voulu lui faire poser sa carte multicolore, j’aurais pu choisir une autre couleur pour ne marquer qu’elle.
Il pose donc la seconde carte marquée : son `R3`.

[![La carte ciblée est le R3](images/multicolore-priorite.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2C!G(M4)%2C!(R3)%2C%3C-R&highestCardsPlayed=M1%2CR2)

Cette seconde partie de la convention est assez contre-intuitive, car à contre-sens de la ***position finesse***. Elle est cependant 
avantageuse dans la plupart des parties et, dès lors, utilisée (notamment sur [BGA](https://boardgamearena.com/)).

- **Bénéfice** : faire poser une carte « derrière » une autre `multicolore`.
- **Inconvénient** : ne pas pouvoir bénéficier d’un marquage collatéral (pour protéger une carte *unique*  se trouvant « derrière »).

#### 📌 Convention 10d – Cartes multicolores vs couleurs terminées

<a href="#convention-10d"></a>

Avec le mode  **Avalanche**, n’importe quel indice *couleur* marque les cartes `multicolores`.

Par convention, les cartes marquées d’une couleur, dont la série est terminée, sont considérées comme `multicolores`.

Ex : 

- Le `G5` est posé, contrairement au `M5`. Je marque deux cartes vertes à la personne suivante via un indice *couleur*.
- Bob pose sa carte verte en ***position finesse***. Il ne la considère pas comme étant *inutile*.

Cette convention permet d’indiquer clairement à la personne qui reçoit l’indice que la cible est une carte `multicolore`. Cette information
lui laisse plus de possibilités pour donner elle-même des indices.

#### 📌 Convention 10e – *Finesse* multicolore

<a href="#convention-10e"></a>

Avec le mode  **Avalanche**, si je veux faire poser une carte `multicolore` partiellement marquée, via *finesse*, mon indice **doit**
être de la même couleur que la carte à faire poser (quand c’est possible sans effet particulier).

Ex :

- Bob a un `M2` marqué `jaune` en main, mais ne sait pas qu’il est ***jouable*** .
- Je marque `jaune` le `M3` de Carole.
- Bob sait qu’il s’agit d’une *finesse* et pose la carte manquante en ***position finesse*** : sa carte `jaune`.

[![La carte multicolore est de la même couleur que l’indice](images/finesse-multicolore-avec-correspondance.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CYX(M2)%2CXX%2CXX%7C(Bk1)%2C!(M3)%2C(RX)%2C(RX)%2C(GX)%2C%3C-Y&highestCardsPlayed=M1)

Dans cette configuration, mon indice `jaune` ne marque qu’une seule carte. Il en aurait été de même avec la couleur `blanc`.
Étant donné que la carte manquante est déjà marquée `jaune`, je **dois** choisir `jaune` plutôt que `blanc`.

Si je choisis `blanc`, cela a une autre signification.

Ex : 

- Même situation, mais cette fois le `M2` est la carte la plus récente de Bob, et non plus sa carte marquée `jaune`.
- Je marque `blanc` le `M3` de Carole.
- Bob sait qu’il s’agit d’une *finesse* et que j’aurais pu utiliser la couleur `jaune` sans aucune contrainte pour lui faire poser sa carte
`jaune`. Il ne pose donc pas cette carte, mais sa plus récente.

[![La couleur de la carte et de l’indice diffèrent](images/finesse-multicolore-sans-correspondance.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(M2)%2CXX%2CYX%2CXX%2CXX%7C(Bk1)%2C!(M3)%2C(RX)%2C(RX)%2C(GX)%2C%3C-W&highestCardsPlayed=M1)

La différence est subtile, mais assez logique : si je ne choisis pas la couleur la plus évidente, c’est que j’ai une bonne raison de le faire.  
Si une *couleur* ou une *valeur* d’indice permet de lever toute ambiguïté sans marquage collatéral gênant, elle **doit** être utilisée.
Sinon, on est dans le second cas de cette convention.

S’il n’y a aucune manière de lever toute ambiguïté, alors la convention de base concernant les multicolores s’applique : la 
***position finesse*** est celle de la carte déjà marquée, peu importe sa couleur et celle de l’indice.

#### 💡 Astuce 5e – Réponse dans les temps

<a href="#astuce-5e"></a>

Si votre équipe utilise le bluff, il est très **important** de ne **pas retarder** vos *réponses* aux indices.

Avant, vous pouviez souvent, sans grand risque, donner un indice direct ou créer une autre *finesse*, plutôt que de *répondre* à une *finesse*. Surtout quand la personne attendant votre *réponse* avait autre chose à poser.

Ex :

- Eve a créé une *finesse* pour mon `G3`, en marquant le `G4` de Carole, qui a également un `R4` ***jouable*** et connu en main.
- Plutôt que de poser ma carte la plus récente et *répondre* à la *finesse*, je crée une autre *finesse* en marquant le `Y2` de David, puisque
Bob a un `Y1` en ***position finesse***.

[![Je retarde ma réponse, malgré la finesse](images/retarder-reponse.png)
](https://hanabi.devnotebook.fr/?playersHand=(G3)%2CXX%2CXX%2CXX%7C(Y1)%2CXX%2CXX%2CXX%7CXX%2C!(G4)%2CR4%2CXX%2C%3C-G%2C4%7CXX%2C%3F(Y2)%2CXX%2CXX%2C%3C-3%2CY%7CXX%2CXX%2CXX%2CXX&highestCardsPlayed=B3%2CG2%2CR3%2CWX%2CYX)

Sans bluff, je sais que ma carte la plus récente est un `G3`, et mes partenaires savent que je le sais, même si je retarde ma *réponse*.

Mais avec le bluff, rien ne m’assure que ma carte est un `G3`. Ex :

- Eve a marqué le `G4` de Carole, comme dans l’exemple précédent. Carole a également un `R4` ***jouable*** et connu en main.
- Plutôt que de poser ma carte la plus récente et *répondre* à l’indice, je crée une *finesse* en marquant le `Y2` de David, puisque
Bob a un `Y1` en ***position finesse***.

[![Je retarde ma réponse, malgré le bluff potentiel](images/retarder-reponse-bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=(W1)%2CXX%2CXX%2CXX%7C(Y1)%2CXX%2CXX%2CXX%7CXX%2C!(G4)%2CR4%2CXX%2C%3C-G%2C4%7CXX%2C%3F(Y2)%2CXX%2CXX%2C%3C-3%2CY%7CXX%2CXX%2CXX%2CXX&highestCardsPlayed=B3%2CG2%2CR3%2CWX%2CYX)

Comme je n’ai pas répondu, le bluff n’est pas *révélé*. Bob ne voyant pas le `G3` manquant, pense l’avoir en main, en ***position finesse***.
Carole, elle, pense avoir un `G3` plutôt qu’un `G4`.  
Bref, il y a de bonnes chances que quelqu’un cause une bombe, en particulier dans une situation où Carole n’aurait pas d’autre carte à poser.

#### 💡 Astuce 5f – Le bluff empêche de créer des *finesses* ?

<a href="#astuce-5f"></a>

Le bluff modifie le timing du jeu et demande plus de rigueur quant au moment où une *finesse* ou un bluff est créé.

Dans l’exemple précédent, Eve a marqué `vert` le `G4` de Carole, pour que je pose une carte manquante. Mais ce faisant, je ne crée pas la
*finesse* `jaune`.

[![Je ne crée pas de finesse au cas où il y aurait bluff](images/bluff-potentiel.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(Y1)%2CXX%2CXX%2CXX%7CXX%2C!(G4)%2CR4%2CXX%2C%3C-G%7CXX%2C(Y2)%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX&highestCardsPlayed=B3%2CG2%2CR3%2CWX%2CYX)

Mais alors, juste parce que, « peut-être », l’indice d’Eve est un bluff, cela me prive d’une opportunité de création de *finesse* ?

Oui.

En fait, avec l’expérience, c’est à Eve de gérer ce dilemme. Elle doit être consciente qu’en donnant l’indice `vert`, elle m’empêche de créer la
finesse `jaune`.  
À mon tour de jeu, je lui fais confiance, et si elle a jugé que c’était préférable, alors je *réponds* tout de suite.  
C’est là où la prévisibilité a toute son importance. Si Eve est pratiquement certaine que je créerai la *finesse* `jaune` à mon tour — et 
donc que ma main ne bougera pas — alors son indice peut attendre le tour suivant.

Cette situation est assez courante, car valable pour tous les indices *urgents*. Par exemple, si Bob a une carte ***en danger***, alors je dois
la protéger et ne peux **pas** *répondre* à l’indice tout de suite. Eve aurait donc pu attendre le tour suivant pour le donner.

#### 💡 Astuce 15a – Ouverture

<a href="#astuce-15a"></a>

Lors de l’ouverture (= les premiers tours de jeu), il est important d’être le plus efficient possible pour protéger et poser un maximum
de cartes, quitte à dépenser un peu plus de jetons indice pour cela.

Exemple 1 :

- Bob a deux `1`, un `M2` en ***position défausse***, et Carole un autre `1` en ***position défausse***. On joue le mode **Avalanche**.

[![Je protège le M2](images/ouverture-avec-couleur.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2C(R1)%2C(B1)%2CXX%2C(M2)%7CXX%2CXX%2CXX%2CXX%2C(G1))

La première idée serait de donner l’indice `1` à Bob. C’est un mauvais choix.  
Tout d’abord, via cet indice, le `M2` n’est pas protégé, et il risque d’être défaussé une fois les `B1` et `R1` posés. Ensuite, Bob risque
d’hésiter à marquer le `G1`, au cas où il se trouverait déjà dans sa main. S’il ne le fait pas, Carole risque de le défausser. S’il
le fait, il retarde d’un tour la pose de ses `1` et à mon prochain tour, je n’aurai probablement rien d’autre à faire que de défausser (et risquer
la perte d’une carte *utile*).

Donner l’indice `R` ou `B`, à la place, est préférable.  
Cela protège le `M2` et permet à Bob de donner l’indice `G` à Carole. Au tour suivant, je pourrai donner un autre indice
et marquer le second `1` dans la main de Bob, plutôt que de défausser.

Finalement, pour un jeton de plus, on a sauvé un `M2`, simplifié la pose du `G1` et retardé ma défausse d’une carte (qui sait, peut-être
le `G2` !).

Exemple 2, similaire :

- Bob a trois `1` en ***position finesse***, et Carole et David ont respectivement un `B2` et un `G1` en ***position défausse***.

[![Je protège le B2](images/ouverture-avec-bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(R1)%2C(G1)%2C(M1)%2CXX%7CXX%2CXX%2CXX%2C(B2)%7CXX%2CXX%2CXX%2C(G1))

Si je donne l’indice `1` à Bob, on perd sûrement le `B2` et l’on sera plusieurs à devoir défausser en attendant les trois tours nécessaires 
pour qu’il pose tous ses `1`.

À la place, je bluffe le `R1` en marquant le `B2` de Carole. Elle, marque le `G1` de David et moi le `M1` de Bob.  
Les trois  `1` sont posés dès le second tour de Bob, et le `B2` est protégé. Pour un seul jeton de plus, c’est largement rentable. Peut-être que
l’autre `B2` était la dernière carte de la pioche !

En fait, même sans le `B2`, cela offre plus de choix, et donc de flexibilité à votre équipe. Avec plus de choix, vos partenaires sont susceptibles 
de créer des *finesses*, bluffs et autres, et de retarder la défausse de cartes importantes. C’est une des priorités de l’ouverture :
protéger un maximum de `2`, puisqu’on ne peut pas les marquer via des indices *défensifs* comme les `5`.

#### 💡 Astuce 15b – Ouverture et priorité

<a href="#astuce-15b"></a>

Lors de l’ouverture – comme après, d’ailleurs – les cartes difficiles à marquer doivent l’être en priorité.

Exemple :

- Bob a deux `1` et deux `5`, dont le `Bk5`.

[![Je privilégie le Bk5](images/ouverture-priorite-noire.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(R1)%2C(Bk5)%2C(B5)%2C(G1)%2CXX%7CXX%2CXX%2CXX%2CXX%2CXX)

- Si aucun bluff ou *finesse* n’est possible avec la main de Carole, je donne l’indice `5` plutôt que `1`.

Cela présente un avantage dans le cas où Bob piocherait un `5` en posant son `1`. Il sera alors pénible d’extraire le `Bk5`. Pas de problème
d’extraction, en revanche, si je marque tout de suite le `Bk5`.

Autre exemple :

- On joue en mode **Avalanche**, et Bob a en ***position finesse***  les cartes `Y2`, `Y1`, puis `B1`.

[![Je privilégie la mono-finesse](images/ouverture-priorite-mono-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(Y2)%2C(Y1)%2C(B1)%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2CXX)

- Je donne l’indice `2` pour une *mono-finesse* `Y`, plutôt que de marquer les `1`.

Que l’indice soit `1` ou `2`, deux cartes sont posées, et il faut un second indice pour la troisième.  
L’intérêt de donner l’indice `2` en priorité, c’est de se prémunir du cas où Bob pioche un `M2`. Si je n’ai pas encore marqué le `Y2`, 
une *extraction* sera  nécessaire pour le faire poser.

Attention, ces exemples ne tiennent pas compte du reste des cartes présentes. Il peut être préférable de privilégier les indices `1`, si un `2`
correspondant est proche d’être défaussé, par exemple.

#### 📌 Convention 22 – Pas de protection précoce (en anglais : no early save)

<a href="#convention-22"></a>

Un indice marquant une carte non ***en danger*** est **toujours** considéré comme un indice *offensif* (même si la carte est *unique* et en
 ***position défausse***).

Ex :

- Toutes les cartes `bleues` viennent d’être posées, sauf le `B5`, qui est justement marqué `B` dans la main de Bob. Il a aussi deux cartes 
*uniques* en ***position défausse*** : le `R5` puis le `G4`, non ***jouables***. Le `M5` a également été posé.

[![Je ne donne pas l’indice 5](images/pas-de-sauvegarde-precoce.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CB(5)%2C(G4)%2C(R5)%7CXX%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=B4%2CG2%2CR3%2CW4%2CY5%2CM5&discardedCards=G4)
- Deux indices vont être nécessaires pour protéger ces deux cartes *uniques*, mais Bob, d’après le principe de *jouabilité implicite*, va poser
son `B5` sans indice supplémentaire. Je défausse à mon tour plutôt que de donner un indice à Bob.

Tout d’abord, oui, il y a deux cartes à protéger, mais non, je n’ai pas besoin de commencer maintenant. Nous sommes une équipe, et 
Carole pourra tout à fait donner l’indice `5` et moi l’indice `4` après le tour de Bob.

**Quel est l’intérêt d’une telle convention ?**

Déjà, elle permet régulièrement d’économiser des indices. Par exemple, peut-être que Bob piochera une carte `rouge` ou `verte` ***jouable***
et on pourra d’un seul indice *couleur* *offensif*, protéger le `R5` ou le `G4`.

Mais surtout, elle permet, plus souvent, les indices *offensifs* sur des cartes en ***position défausse***.

Par exemple :

- Même situation, mais cette fois le `5` en ***position défausse*** de Bob est ***jouable***. Je lui donne l’indice `5`.

[![Je donne l’indice 5 direct](images/pas-une-sauvegarde-precoce.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2C!B(5)%2C(G4)%2C!(R5)%2C%3C-5%7CXX%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=B4%2CG2%2CR4%2CW3%2CY5%2CM5&discardedCards=G4)

- Bob voit que sa carte `B` est marquée `5`, ce qui ne justifie pas l’indice puisqu’il l’aurait posé de toute manière. Il en déduit donc que son 
autre `5` est la cible de l’indice, et qu’il est ***jouable***. Au cas où, il commence par poser son `B5`,  plus sûr, car connu totalement. 
Il posera son autre `5` au tour suivant.

Le « au cas où » couvre un cas rarissime, mais possible, notamment pour une équipe de 3 : quand Bob a trois cartes *uniques* en 
***position défausse***. Dans ce cas, je lui marque son `5`, puis Carole son `4`, et je protège ensuite la 3ᵉ carte ***en danger***.  
Bob comprendra qu’il s’agit de trois indices *défensifs*, car trois indices *offensifs* consécutifs n’auraient pas vraiment de sens. (Pourquoi lui
marquer trois cartes ***jouables*** si rapidement, alors qu’il ne peut en poser qu’une par tour ?)

Remarquez que dans cet exemple, l’indice `5` est direct, mais il peut tout à fait être une *(mono-)finesse*. Ex :

- Même situation, mais cette fois le `5` en ***position défausse*** de Bob ne sera ***jouable*** qu’après que le `W4` ait été posé. Bob 
l’a justement en ***position finesse***. Je lui donne l’indice `5`.

[![Je donne l’indice 5](images/pas-une-sauvegarde-precoce-mais-une-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(W4)%2CXX%2C!B(5)%2C(G4)%2C!(W5)%2C%3C-5%7CXX%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=B4%2CG2%2CR5%2CW3%2CY5%2CM5&discardedCards=G4)

- Même raisonnement pour Bob. Il savait que son `B5` était ***jouable*** et donc que l’indice *offensif* concerne son autre `5`. Comme Carole
n’a rien de ***jouable*** en ***position finesse***, ce n’est pas une *renversée*, et comme aucun `5` n’est ***jouable*** actuellement, c’est
qu’il a une carte manquante en ***position finesse***. Il pose sa carte la plus récente (avant son `B5` !), au cas où il s’agirait d’un 
*mono-bluff*. Aux prochains tours, il posera son `B5` puis son `W5`.

Notez que cette convention présente tout de même des désavantages : 

- Peut-être que pendant l’attente, des cartes *inutiles* collatérales vont être piochées et devront être marquées par les indices.
- L’équipe doit être vigilante au nombre de jetons indice, pour qu’il en reste assez au moment de donner tous les indices *défensifs*.
- Comme toute convention, son côté arbitraire peut paraître contre-intuitif et causer des différences d’interprétation entre les personnes qui 
la connaissent et les autres.
- Si on ne joue ni le mode **Avalanche**, ni l’extension **Poudre noire**, on n’a pas forcément besoin des bénéfices de cette convention. 
Souvent déjà, on peut utiliser un indice *offensif* *couleur* pour faire poser une carte en ***position défausse***.

#### 💡 Astuce 16 – *Finesse superposée*

<a href="#astuce-16"></a>

Il est possible de faire reposer plusieurs *finesses* simultanées sur une même personne.

Dans ce cas, elle doit comprendre que sa première carte en ***position finesse*** *répond* à la première *finesse*, 
que la carte suivante *répond* à la deuxième *finesse*, etc.

Ex :

- Le `B1` a été posé, mais ni le `R1`, ni le `B2`. Carole a justement un `R1` puis un `B2` en ***position finesse***.
- Je marque le `R2` de David.
- Bob marque le `B3` d’Eve.

[![Carole doit répondre à deux finesses en même temps](images/finesse-superposee.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7C(R1)%2C(B2)%2CXX%2CXX%7CXX%2CXX%2C!(R2)%2CXX%2C%3C-R%2CB%7CXX%2C%3F(B3)%2CXX%2CXX%2C%3C-R%2CB&highestCardsPlayed=B1)

- Carole comprend qu’elle a deux cartes à poser pour *répondre* aux deux *finesses*. Elle choisit de poser l’une ou l’autre d’abord, selon 
la situation.

Le risque ici, c’est qu’une des deux *finesses* ne reçoit pas de *réponse* tout de suite :  

- Si le `R1` est posé en premier, Eve doit attendre le `B2` avant de poser sa carte bleue. 
- Si le `B2` est posé en premier, David doit attendre le `R1` avant de poser sa carte rouge.

Selon comment sont marqués les `B3` et `R2` (si leurs propriétaires connaissent uniquement la *couleur* ou également la *valeur* 
de leur carte), Carole devra choisir de poser en premier plutôt son `B2` ou son `R1`, afin d’éviter que les cartes suivantes ne soient 
posées trop tôt.

Si David ou Eve ne comprend pas que sa carte n’est pas ***jouable*** tout de suite, il ou elle causera une bombe. 
Ce genre d’indice est donc à privilégier avec une équipe expérimentée, susceptible de reconnaître la *finesse superposée*.

#### 📌 Convention 23a – Ordre des *réponses*

<a href="#convention-23a"></a>

Lorsque vous devez *répondre* à plusieurs *finesses* en parallèle, répondez toujours à celle nécessitant de poser une carte non marquée
d’abord, au cas où il s’agirait d’un bluff.

Ex :

- David a créé une *longue finesse* en marquant `bleu` le `B4` de Carole. Bob a répondu une première fois en posant son `B1` en 
***position finesse***. Il a compris que sa carte marquée `2` est le `B2`, et il voit mon `B3` en ***position finesse***.
- À mon tour de jeu, je donne l’indice `vert` à David, lui marquant son `G2` (non ***jouable*** puisque le `G1` n’a pas encore été posé).

[![Bob doit répondre à plusieurs finesses en parallèle](images/ordre-reponses.png)
](https://hanabi.devnotebook.fr/eyJmaXJld29ya3MiOlt7ImNvbG9yIjoiYmx1ZSIsInZhbHVlIjoxfSx7ImNvbG9yIjoiZ3JlZW4ifSx7ImNvbG9yIjoicmVkIn0seyJjb2xvciI6IndoaXRlIiwidmFsdWUiOjJ9LHsiY29sb3IiOiJ5ZWxsb3cifV0sImRpc2NhcmRQaWxlIjp7fSwicGxheWVycyI6W3sibmFtZSI6IkFsaWNlIiwiaGFuZCI6W3sicmVhbFZhbHVlIjozLCJyZWFsQ29sb3IiOiJibHVlIn0se30se30se31dfSx7Im5hbWUiOiJCb2IiLCJoYW5kIjpbeyJyZWFsQ29sb3IiOiJyZWQiLCJyZWFsVmFsdWUiOjF9LHt9LHt9LHsia25vd25WYWx1ZSI6MiwicmVhbENvbG9yIjoiYmx1ZSJ9XX0seyJuYW1lIjoiQ2Fyb2xlIiwiaGFuZCI6W3t9LHsia25vd25Db2xvciI6ImJsdWUiLCJyZWFsVmFsdWUiOjQsImNsdWUxIjp7ImNvbG9yIjoiYmx1ZSIsIm51bWJlciI6MX19LHt9LHt9XX0seyJuYW1lIjoiRGF2aWQiLCJoYW5kIjpbe30se30seyJjbHVlMSI6eyJjb2xvciI6ImdyZWVuIiwibnVtYmVyIjoyfSwia25vd25Db2xvciI6ImdyZWVuIiwicmVhbFZhbHVlIjoyfSx7fV19XX0=)

- Bob sait que deux personnes attendent son `B2` pour pouvoir poser leur carte. Cependant, l’indice `vert` indique également une carte 
*manquante*. Il pose sa carte en ***position finesse*** pour le `G1` et *révèle* le bluff puisqu’il s’agit en fait d’un `R1`.

Sans cette *réponse*, rien n’aurait indiqué à David que sa carte `verte` n’était pas ***jouable*** et Carole aurait sûrement causé une bombe.
À l’inverse, Alice et Carole devrait normalement voir un `B2` et un `B3`, leur évitant de poser leur carte avant le `B2`.

#### 💡 Astuce 17 – Protection des cartes *utiles*

<a href="#astuce-17"></a>

La convention finesse présente un désavantage important : elle interdit de marquer une carte ni ***jouable***, ni ***en danger***, 
sauf pour créer une *finesse*. Cela interdit donc de marquer un `2` non *unique* en ***position défausse***. 
Il est possible malgré tout de protéger les cartes *utiles*.

Avant de voir comment, quelles cartes valent la peine d’être protégées ?

1. les cartes *uniques*, évidemment.
2. les cartes ***jouables***, bien sûr.
3. les cartes à petits chiffres (ou grands pour les noires), puisque la pose de plus de cartes repose sur elles.
4. les cartes « bientôt ***jouables*** » (ex : le `B3` si seul le  `B1` a été posé).
5. les cartes « bientôt ***jouables*** » en comptant les cartes marquées dans les mains de l’équipe (ex : le `B4` si seul le  `B1` a été 
posé, mais que le `B3` est marqué `3` dans la main de quelqu’un).

Attention, si plusieurs personnes ont beaucoup de cartes *utiles* marquées en main, et qu’elles piochent en plus des cartes *uniques*, 
alors cela peut bloquer leur main. À vous d’évaluer l’utilité de la carte par rapport à la gêne qu’elle occasionne dans la main de 
son ou sa propriétaire. Si vous jouez à 5 et qu’une personne a sa main bloquée, ce n’est pas gênant, mais si c’est deux personnes sur 3…
cela devient très compliqué.

Voici plusieurs façons de protéger une carte non ***en danger*** :

- en la marquant via :
	- le marquage collatéral (ex : j’indique `R` pour faire poser le `R2`, marquant également le `R5`)
	- le *bluff finesse*
- en évitant sa défausse :
	- en faisant poser autre chose à son ou sa propriétaire
	- en laissant cette personne donner des indices
	- en lui marquant des cartes *inutiles* à défausser d’abord

#### 🔧 Technique 13 – *Bluff ordurier*  (en anglais : *trash bluff*)

<a href="#technique-13"></a>

À l’instar du *bluff finesse* pour une *finesse*, on peut utiliser une *finesse ordurière* pour bluffer.

Ex :

- Tous les `1` ont été posés, sauf le `Y1`. Bob a un `G2` en ***position finesse***.
- Je marque `1` trois cartes (non `jaunes`) de Carole.

[![Exemple de bluff ordurier](images/bluff-ordurier.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(G2)%2CXX%2CXX%2CXX%2CXX%7CXX%2C!(W1)%2C!(B1)%2C!(B1)%2CXX%2C%3C-1&highestCardsPlayed=B1%2CG1%2CR1%2CW1%2CYX)

- Bob comprend que s’il ne fait rien, Carole va poser son `1` en ***position finesse***, alors qu’il n’est pas ***jouable***. 
Il se dit qu’il a le `Y1` en ***position finesse*** et pose en fait son `G2`.
- Carole voit que l’indice `1` a fait poser un `G2`. Comme l’indice a donné lieu à une *réponse*, elle comprend ce qu’a cru Bob 
et sait maintenant qu’elle a en main trois  « **déchets** », 3 cartes *inutiles*, pouvant être défaussées sans risque.

Cette technique est très pratique, mais ajoute de la complexité. Comme pour la *finesse ordurière*, à utiliser uniquement si au moins 
une de ces raisons est valable :

- l’indice protège une ou plusieurs cartes *utiles* de la personne recevant l’indice (car elle défaussera d’abord celles marquées par l’indice)
- la carte à poser ne peut pas être marquée directement sans polluer la main de son ou sa propriétaire
- la carte à poser ne peut pas être marquée directement sans ambiguïté (ex : l’indice ressemble à une renversée, il marque une autre carte
qui va à tort paraître ***jouable***…)

#### 💡 Astuce 18 – Renversée non rentable

<a href="#astuce-18"></a>

Régulièrement, créer une *finesse renversée* n’est pas rentable et il vaut mieux s’abstenir, quitte à défausser. C’est particulièrement vrai
quand on joue à 3.

Le principal intérêt d’une *renversée*, est de faire poser plusieurs cartes avec un seul jeton indice, et éventuellement de fluidifier le jeu, si 
la personne à qui vous donnez l’indice à autre chose à faire que d’en donner un elle-même.

Ces avantages sont limités si :

- Vous empêchez une carte d’être bluffée, ce qui aurait ainsi permis de protéger des cartes utiles.
- Cela force la personne qui reçoit l’indice à défausser en attendant de pouvoir poser sa carte (parce qu’elle n’a rien d’autre à faire).
Si elle avait donné elle-même l’indice, elle n’aurait pas eu à défausser.
- Cela vous retarde pour poser une carte, parce qu’à ce tour, vous avez préféré donner l’indice.
- Cela ne permet pas d’économiser un jeton : typiquement, si bluffer la carte permettait d’en protéger une ***en danger***.

Ex :

- Le `Y1` et le `R1` ont été posés. Bob a le  `Y3` marqué 3 en main (mais n’en connaît pas la couleur), Carole le `Y2` en ***position finesse***
et David deux cartes marquées
`5`, le `B2` ***en danger***, puis le  `R3`. 
- Je donne la renversée `jaune` en marquant le `Y3`.

[![Il vaut mieux laisser Bob donner l’indice](images/renversee-non-rentable.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2C!(Y)3%2CXX%2CXX%2C%3C-Y%7C(Y2)%2CXX%2CXX%2CXX%7C(R3)%2C(B2)%2CX5%2CX5&highestCardsPlayed=Y1%2CR1&discardedCards=B2%2CG2)

- À son tour, Bob donne l’indice `2` à David, pour protéger le `B2` *unique*.
- Carole *répond* à la renversée.
- David défausse son `R3` et pioche un `5`, ce qui va bloquer sa main.

Ici, la *renversée* n’a pas fait économiser de jeton indice, puisque le `Y2` aurait pu être bluffé avec le `B2`. Il a fallu 2 indices : 
un pour la *renversée*, et un pour la protection, au lieu d’un pour le bluff et un pour le `Y3`. Pire, si j’ai le `Y4` en main, cela coûte même 
un jeton de plus, car une *renversée* aurait pu m’être donnée pour faire poser le `Y3` !

À côté de ça, David a dû défausser alors que sa main était déjà très chargée, ce qui a fait perdre un `R3` bientôt ***jouable*** et 
a finalement bloqué sa main. Si j’avais défaussé, Bob aurait donné le bluff, et David aurait pu marquer le `Y3` à ma place
pour s’éviter de défausser.

#### 🔧 Technique 14a – *Défausse gentleman en couches* (en anglais : `layered GD`)

<a href="#technique-14a"></a>

Vous pouvez utiliser la *défausse gentleman* pour faire poser plusieurs cartes.

Ex :

- Bob a en ***position finesse*** deux cartes ***jouables*** qu’il ne connaît pas : le `G2` puis le `R3`.
- J’ai un `R3` en main que je connais. Je le défausse.

[![La défausse gentleman fait poser deux cartes](images/defausse-gentleman-en-couche.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CR3%7C(G2)%2C(R3)%2CXX%2CXX%2CXX&highestCardsPlayed=R2%2CG1)

- Bob ne voit l’autre `R3` nulle part. Il sait donc qu’il l’a en main, et essaie de le poser. À la place, il pose le `G2`.
- Il comprend que le `R3` est à la suite dans sa main et le posera au prochain tour.

Bob doit continuer de poser ses cartes jusqu’à tomber sur la copie de celle défaussée.

Comme toute *défausse gentleman*, cela évite d’avoir à utiliser un jeton indice.  
Attention à ne pas en abuser. Si Bob a un *indice urgent* à donner, par exemple, il est sans doute préférable de poser 
moi-même le `R3`. Le `G2` sera de toute façon toujours en ***position finesse*** et je pourrai utiliser un bluff ou une *finesse* 
au tour prochain pour le faire poser.

#### 📌 Convention 15d – Extension Poudre Noire (bluffs avec Bk4)

<a href="#convention-15d"></a>

Pas mal de joueurs et de joueuses créent des bluffs **invalides** avec l’Extension Poudre Noire, en marquant un `Bk4`.

Ex :

- Bob a un `W1` en ***position finesse***. 
- Je commence la partie en donnant l’indice `4` à Carole en marquant son `Bk4`.

[![Je tente de bluffer un 1 avec un indice 4 sur un Bk4](images/bluff-invalide-bk4.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(W1)%2CXX%2CXX%2CXX%2CXX%7CXX%2C!(BK4)%2CXX%2CXX%2CXX%2C%3C-4&highestCardsPlayed=BX%2CGX%2CRX%2CWX%2CYX%2CBkX)

Bob va poser son `1`, et le `Bk4` est désormais protégé, car marqué, ce qui semble idéal.

Imaginez maintenant une autre situation, où Bob a toujours le  `W1` en ***position finesse***, mais cette fois, Carole a les `W2`, `W4`, `W3`
en ***position finesse***. 
- Je commence la partie en marquant son `4`.

[![Je crée une longue finesse blanche pour faire poser les cartes du 1 au 4](images/longue-self-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(W1)%2CXX%2CXX%2CXX%2CXX%7C(W2)%2C!(W4)%2C(W3)%2CXX%2CXX%2C%3C-4&highestCardsPlayed=BX%2CGX%2CRX%2CWX%2CYX%2CBkX)

Dans ces deux situations, Carole a accès exactement aux mêmes informations : elle a reçu un indice `4` auquel Bob a *répondu* en posant 
un `W1`. Que devrait-elle faire ?  
À son tour de jeu, aucun bluff n’est *révélé*, car rien n’indique que le `1` et le `4` sont incompatibles. Ne voyant aucun `W2` ou `W3` en
***position finesse***, elle devine qu’ils sont dans sa main et va donc poser sa carte la plus récente.  
Dans le cas d’un indice sur le `Bk4`, cela cause donc une bombe.

La seconde situation étant beaucoup plus rare que la première, l’équipe peut choisir de faire une exception et d’autoriser le bluff qui marque un
`Bk4`.  
Attention, cette exception vous interdit dès lors certaines *finesses* ou techniques, sans quoi certaines ambiguïtés comme celle illustrée par
les deux situations précédentes risquent d’arriver.
De plus, le cadre dans lequel on autorise ce genre d’exception est totalement arbitraire, car en opposition avec la logique qui découle des
conventions expliquées précédemment. Il est donc plus difficilement compréhensible par une nouvelle personne intégrant l’équipe.

Si vous pensez tout de même avoir besoin de ce bluff et l’autorisez, vous devez en définir le cadre. Si la carte posée en réponse par Bob est 
un `2` plutôt qu’un `1`, alors que le bluff était autorisé, est-ce une *finesse* ou un bluff ? Qu’en est-il si une partie des cartes manquantes 
est en ***position finesse*** dans la main de quelqu’un ? Et si c’est un indice `3` sur un `Bk3`, plutôt qu’un `Bk4` ?  
Par ailleurs, au niveau expert, cette exception rend encore plus complexes les *longues promesses* et la distinction entre le bluff et le
*jeu en couche*.

Pour toutes ces raison, ce bluff ne devrait jamais être autorisé :
- par défaut, sans accord préalable au sein de l’équipe
- pour faire poser autre chose qu’un `1`
- en marquant une autre carte qu’un `Bk4` (ex :  un `Bk3`), car une telle carte est beaucoup moins importante qu’un `Bk4`
- si le `2` ou `3` de la couleur du `1` posé est en ***position finesse*** et visible et/ou connu par la personne qui reçoit l’indice `4`

#### 📌 Convention 15e – Extension Poudre Noire (*bluffs orduriers* )

<a href="#convention-15e"></a>

Pas mal de joueurs et de joueuses créent des *bluffs orduriers* **invalides** avec l’Extension Poudre Noire, quand le `Bk1` n’est pas visible.

Ex :

- Bob a un `W2` ***jouable*** en ***position finesse***. Le `Bk1` ainsi que tous les `1` ont été posés, sauf le `Bk1`. Carole a un `R1` 
en main.
- Je donne l’indice `1` à Carole en marquant son `R1` inutile.

[![Je crée un bluff ordurier ambigu](images/bluff-invalide-1.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(W2)%2CXX%2CXX%2CXX%2CXX%7CXX%2C!(R1)%2CXX%2CXX%2CXX%2C%3C-1&highestCardsPlayed=B1%2CG1%2CR1%2CW1%2CY1%2CBk4)

Bob va poser son `2`, et la carte de Carole est désormais marquée.

Imaginez maintenant une autre situation, où Bob a toujours le  `W2` en ***position finesse***, mais cette fois, Carole a un `Bk2` en 
***position finesse*** et son `1` est le `Bk1`. 
- Je donne l’indice `1` à Carole en marquant son `Bk1`.

[![Je crée un bluff en marquant un Bk1](images/bluff-sur-bk1.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(W2)%2CXX%2CXX%2CXX%2CXX%7C(Bk2)%2CXX%2CXX%2C!(Bk1)%2CXX%2C%3C-1&highestCardsPlayed=B1%2CG1%2CR1%2CW1%2CY1%2CBk4)

Dans ces deux situations, Carole a accès exactement aux mêmes informations : elle a reçu un indice `1` auquel Bob a répondu en posant un `W2`. Que devrait-elle faire ?

À son tour de jeu, rien ne lui permet de savoir s’il s’agit d’un *bluff ordurier* ou d’un *bluff finesse*. Dans le premier cas, elle peut 
se débarrasser de son `1`. Dans le second, c’est une carte *unique* à ne surtout pas défausser. Notez que l’indice est utile dans les deux cas.

Pour lever l’ambiguïté, une convention est nécessaire. Dans le doute, le `1` ne doit pas être défaussé, puisqu’il peut être *unique*, et 
car le *bluff finesse*, plus élémentaire, devrait être privilégié au *bluff ordurier*.

Sur BGA, cette convention reste toutefois très souple et les joueurs et joueuses ont tendance à plutôt se baser sur le contexte.  
Pour limiter les erreurs d’interprétation, il peut être sage de ne créer de *bluff ordurier* marquant un `1`, que si au moins une de ces
conditions est valide :

- Si on ne joue pas avec l’extension Poudre Noire, bien sûr.
- Si le `Bk1` est visible ou connu par la personne recevant l’indice.
- S’il faut au moins 2 cartes `noires` non marquées et en ***position finesse*** à la personne qui reçoit l’indice, pour qu’un bluff avec 
une *promesse* `noire` valide soit possible.  
Par exemple, dans la seconde situation proposée, pour Carole il manque deux cartes (le `Bk3` et le `Bk2`). Si Bob pense poser un `Bk3` en
*répondant* à l’indice, pour une *promesse* valide, il faut encore que Carole ait un `Bk2` inconnu en ***position finesse***. C’est très
possible.  
Si, toutefois, le `Bk4` n’avait pas encore été posé, il faudrait que Carole ait un `Bk3` **et** un `Bk2` inconnus en ***position finesse*** 
pour que l’indice soit un *bluff finesse*. C’est beaucoup moins probable.

#### 💡 Astuce 13b – Nombre de Défausses Restantes (NDR)

<a href="#astuce-13b"></a>

Hanabi est une course où un maximum de cartes doivent être posées avant la fin de la pioche. Si l’on joue en mode classique avec cinq
couleurs, on doit poser 25 cartes. Sachant que la pioche s’écoule lorsque l’on pose ou défausse une carte, on peut calculer le nombre
maximum de cartes que l’on a le droit de défausser, avant qu’il ne soit impossible de poser toutes les cartes. 

Pour cela, on utilise cette formule : **score actuel** + **nb cartes dans la pioche** + **nb de personnes dans l’équipe** - **score maximal**.
Ce qui donne **13** (0 + 34 + 4 - 25) au début de la partie, quand on joue à 4 avec les 5 couleurs de base.

On peut calculer le NDR à tout moment de la partie.

Ex :

[![Le NDR vaut 1](images/ndr.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=B1%2CR4%2CG5%2CW2%2CY3&nbRemainingCardsInDeck=8)

Le NDR vaut ici **1** (15 + 8 + 3 - 25).

Ce calcul aide à déterminer s’il est judicieux ou non de défausser. Si le NDR atteint `0` et que vous défaussez, il passera à `-1`, 
ce qui veut dire qu’au mieux, vous poserez toutes les cartes sauf **1**. S’il est à `-4`, c’est que votre score maximal sera de 21/25 !

**Remarque** : avec l’extension **Flamboyants**, les 2 récompenses vous permettant de récupérer une carte depuis la défausse
vous octroient un NDR de 2 supplémentaire, tant que vous les obtenez avant la fin de la pioche.

#### Conclusion

Vous savez utiliser la *finesse ordurière* et le bluff associé. Vous connaissez bien les avantages et les risques du bluff et savez
quand il est opportun de l’utiliser.

Peut-on outrepasser un bluff ? Quelles autres techniques sont utilisables ? Rendez-vous au chapitre suivant.


### Expérimenté – niveau 3

<a href="#expérimenté-niveau-3"></a>

Applicable quand les membres de l’équipe sont à l’aise avec tous types d’indice.

C’est le moment de laisser libre cours à votre créativité. Les techniques expliquées ici ne sont pas moins puissantes, mais sont
beaucoup plus circonstancielles.

#### 🔧 Technique 15 – Finesse + bluff

<a href="#technique-15"></a>

Avec un seul indice, il est possible de faire poser deux cartes : une via *finesse* et l’autre par bluff.

Ex :

- Le `G2` et le `R3` ont été posés, mais pas la suite. Bob a un `G3` et Carole un `R4`, tous deux  en ***position finesse***.
- Je marque `vert` le `G5` de David.

[![Le B3 est posé par finesse et le R4 par bluff](images/finesse-plus-bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(G3)%2CXX%2CXX%2CXX%7C(R4)%2CXX%2CXX%2CXX%7CXX%2CXX%2C!(G5)%2CXX%2C%3C-G&highestCardsPlayed=G2%2CR3)

- Bob *répond* en pensant avoir le `G3` puis le `G4` en ***position finesse***. Il pose d’abord son `G3`.
- Carole ne voit pas le `G4` nécessaire pour compléter la *finesse*. Elle pense donc l’avoir en ***position finesse*** et pose finalement 
son `R4`.
- Bob et David voient qu’un `R4` a été posé sans indice, ce qui *révèle* un bluff. Ainsi, ils ne poseront donc pas ce qu’ils pensaient 
tous deux être le `G4` dans leur main.

Cette technique est pratique, mais comporte un risque majeur :  Bob peut vouloir marquer lui-même la carte en ***position finesse*** 
de Carole (puisqu’elle ne correspond pas à la *finesse* qu’on lui a donnée).  
Dans ce cas, Carole ne jouera pas sa carte grâce au bluff initial, mais grâce à l’indice de Bob. Dès lors, impossible pour 
Bob et David de savoir qu’ils n’ont pas eux-mêmes le `G4` en main.

Pour éviter cela, il faut être suffisamment sûr·e que Bob préférera poser sa carte plutôt que donner un indice : typiquement parce 
qu’il n’y a plus de jeton indice, éventuellement s’il n’est pas *urgent* de donner cet indice.  
C’est également moins risqué si cette technique est familière à Bob. Il suspectera votre intention et posera sa carte plutôt que 
donner un indice tout de suite, au cas où Carole, après lui, poserait la sienne « comme par magie ».

Si vous êtes à la place de Bob, vous pouvez reconnaître cette situation quand on a l’air de vous donner une *mono-finesse* pile au moment
où vous souhaiteriez faire poser la carte en ***position finesse*** de la personne après vous.

#### 📌 Convention 24 – *Défausse panique positionnelle*

<a href="#convention-24"></a>

Quand vous n’avez aucune carte à poser, vous pouvez utiliser une *défausse panique* en défaussant votre seconde carte en 
***position défausse***. Vous sautez alors **1** carte donc la personne après vous comprend qu’elle a **1** carte ***en danger***.

Si vous défaussez votre troisième carte en ***position défausse***, vous sautez cette fois **2** cartes. La personne après vous a 
alors **2** cartes ***en danger***.

Et ainsi de suite.

C’est très pratique si vous n’avez aucun jeton indice et que vous n’avez pas de carte à poser. Cette défausse sera considérée alors
comme une *défausse panique*.

Attention, cette convention implique de nouvelles contraintes. Et si la carte à défausser est elle-même *unique* ?  
Dans ce cas, c’est à la personne avant vous de défausser, pour générer un jeton indice et vous éviter la *défausse panique*.

#### 🔧 Technique 16 – *Bluff distant* et *bluff renversé*

<a href="#technique-16"></a>

La grande majorité du temps, vous ne pouvez bluffer que la personne juste après vous, mais il y a deux exceptions notables.

##### Cas 1

Si la personne après vous pense pouvoir *répondre* en posant une carte **marquée**.

Ex : 

- Aucun `1` n’a encore été posé et Bob en a deux marqués `1` en main (les `B1` et `G1`). Carole a un `W1` inconnu en ***position finesse***.
- Je donne l’indice `rouge` à David, marquant ainsi son `R2`.

[![Je bluffe Carole en donnant un indice à David](images/bluff-distant.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2C(B)1%2C(G)1%2CXX%7C(W1)%2CXX%2CXX%2CXX%7CXX%2CXX%2C!(R2)%2CXX%2C%3C-R&highestCardsPlayed=GX%2CBX%2CRX%2CWX%2CYX)

- Bob pense évidemment que l’un de ses `1` est le `R1` et pose son `1` en ***position finesse***.
- À ce stade, rien n’indique à David que sa carte `rouge` n’est pas ***jouable***. C’est donc à Carole de _répondre_  à l’indice. Elle pose 
ce qu’elle pense être un `R1`, mais c’est en fait un `W1`.
- Le `W1` non marqué a été posé à partir d’un indice d’une autre couleur. C’est donc un bluff, et il a été *révélé* à toute l’équipe. David sait
désormais que sa carte est un `R2`, et Bob que son second `1` n’est **pas** `rouge`.

##### Cas 2

Si la personne après vous sait qu’elle ne peut **pas** *répondre* en posant une carte sans causer de bombe. Typiquement, 
parce que sa main est pleine de cartes marquées, qui ne correspondent pas à l’indice. (Notez que dans ce cas, si votre partenaire n’a rien 
à poser, autant lui laisser donner l’indice.)

Ex :

- La main de Bob est bloquée. Ses trois cartes en ***position défausse*** sont marquées `5`, l’un d’eux est aussi marqué `B` et donc
***jouable***. La dernière carte est marquée `4`. Carole a un `G3` inconnu et ***jouable*** en ***position finesse***. Le `Y2` a été posé
mais pas le `Y3`.
- Je donne l’indice `Y` sur le `Y4` de Bob.

[![Je bluffe Carole en donnant un indice à David](images/bluff-distant-par-blocage.png)
](https://hanabi.devnotebook.fr/eyJmaXJld29ya3MiOlt7ImNvbG9yIjoiYmx1ZSIsInZhbHVlIjo0fSx7ImNvbG9yIjoiZ3JlZW4iLCJ2YWx1ZSI6Mn0seyJjb2xvciI6InJlZCIsInZhbHVlIjozfSx7ImNvbG9yIjoid2hpdGUiLCJ2YWx1ZSI6M30seyJjb2xvciI6InllbGxvdyIsInZhbHVlIjoyfV0sImRpc2NhcmRQaWxlIjp7fSwicGxheWVycyI6W3sibmFtZSI6IkFsaWNlIiwiaGFuZCI6W3t9LHt9LHt9LHt9XX0seyJuYW1lIjoiQm9iIiwiaGFuZCI6W3sia25vd25WYWx1ZSI6NH0seyJrbm93blZhbHVlIjo1fSx7Imtub3duVmFsdWUiOjV9LHsia25vd25Db2xvciI6ImJsdWUiLCJrbm93blZhbHVlIjo1fV19LHsibmFtZSI6IkNhcm9sZSIsImhhbmQiOlt7InJlYWxDb2xvciI6ImdyZWVuIiwicmVhbFZhbHVlIjozfSx7fSx7fSx7fV19LHsibmFtZSI6IkRhdmlkIiwiaGFuZCI6W3siY2x1ZTEiOnsiY29sb3IiOiJ5ZWxsb3cifSwicmVhbFZhbHVlIjo0LCJyZWFsQ29sb3IiOiJ5ZWxsb3cifSx7fSx7fSx7fV19XX0=)

- Bob constate que la carte marquée par l’indice n’est pas ***jouable***, mais qu’il ne peut pas avoir la carte manquante, et que toute l’équipe
le sait. Il remarque que Carole à une carte ***jouable*** en ***position finesse*** et me fait confiance. Il pose son `B5`.
- Carole croit répondre à une *renversée* `jaune` mais pose en fait son `G3`.

Une carte qui ne correspond pas à l’indice à été posée par la première personne pouvant donner une *réponse*. Le bluff est donc *révélé*.

Notez que ce bluff peut être renversé.

Ex :

- La main de Bob est bloquée. Ses trois cartes en ***position défausse*** sont marquées `5`, l’un d’eux est aussi marqué `B` et donc
***jouable***. La dernière carte est marquée `jaune`. Carole a un `G3` inconnu et ***jouable*** en ***position finesse***. Le `Y2` a été posé
mais pas le `Y3`.
- Je donne l’indice `4` sur le `Y4` de Bob.

[![Je bluffe Carole en donnant un indice à Bob](images/bluff-renverse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C!Y(4)%2C(X)5%2C(X)5%2CB5%2C%3C-4%7C(G3)%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX&highestCardsPlayed=B4%2CG2%2CR3%2CW3%2CY2)

- Bob voit bien qu’il s’agit d’un indice *offensif*, mais qu’il y a une carte manquante et que personne ne l’a, lui encore moins. Il voit que Carole
a une carte ***jouable*** en ***position finesse***, me fait confiance et pose son `B5`.
- Carole croit répondre à une *finesse* `jaune` mais pose en fait son `G3`.

Une carte qui ne correspond pas à l’indice à été posée par la première personne pouvant donner une *réponse*. Le bluff est donc *révélé*.

Attention, comme la *finesse* + bluff, le *bluff distant* présente un risque. Si Bob ne comprend pas et donne lui aussi un indice pour faire poser
le `G3`, alors la pose du `G3` sera dû à son indice et plus au vôtre. Le bluff ne sera pas *révélé* et David causera sûrement une bombe.

#### 🔧 Technique 17 – *Poussée ordurière*  (en anglais : *trash push*)

<a href="#technique-17"></a>

Si vous marquez une carte *inutile* en ***position défausse***, cela indique que la carte suivante (en ***position défausse***) 
est ***jouable***.

Ex : 

- Tous les `1` ont été posés, et le `B4` est ***jouable***. Bob a en ***position défausse*** un `1`, puis le  `B4` mais pas d’autres `1`.
- Je lui donne l’indice `1`.

[![Je fais poser la carte après celle marquée](images/poussee-orduriere.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2C(BX)%2C(X4)%2C(B4)%2C!(X1)%2C%3C-1&highestCardsPlayed=G2%2CB3%2CR1%2CW2%2CY2)

- Bob sait désormais que sa carte est *inutile*, ce qui n’a aucun intérêt puisque son `1` est de toute façon la prochaine 
qu’il aurait défaussée. Mais, comme il connaît la *poussée ordurière*, il comprend que la véritable cible de l’indice est sa carte suivante, 
et qu’elle est ***jouable***. Il pose donc son `B4`.

Cet indice n’a d’intérêt que s’il n’y a pas de moyen de marquer la carte ***jouable*** directement, ou sans polluer la main de la personne.

L’indice ne doit marquer **que** des cartes en  ***position défausse***, sinon il peut être considéré comme  *transparent*, 
juste pour retarder la défausse des cartes non marquées. 

Attention, pour être compatible avec la *finesse ordurière*, tout le monde doit être en mesure de savoir que les cartes marquées sont 
*inutiles* dès que l’indice est donné.

#### 🔧 Technique 18a – *Double bluff*

<a href="#technique-18a"></a>

Vous pouvez bluffer deux personnes d’un coup.

Ex : 

- Les `B1`, `R1` et `G1` n’ont pas été posés. Les `R1` et `G1` sont justement en ***position finesse*** chez Bob et Carole.
- Je donne l’indice `2` sur le `B2` au milieu de la main de David.

[![Exemple de double bluff](images/double-bluff.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(R1)%2CXX%2CXX%2CXX%7C(G1)%2CXX%2CXX%2CXX%7CXX%2C!(B2)%2CXX%2CXX%2C%3C-2&highestCardsPlayed=BX%2CRX%2CGX)

- Bob *répond* selon lui à une « *finesse* `bleue` », pensant avoir le `B1` en ***position finesse*** : il pose son `R1`.
- Carole comprend que si elle ne *répond* pas, David pensera, lui, devoir terminer une « *finesse* `rouge` » avec un `R2` 
marqué par l’indice. Pour éviter cela, elle *répond* donc elle aussi, posant alors son `G1`.
- David comprend qu’il s’agît là d’un *double bluff*, que son `2` n’est **pas** ***jouable***.

Attention, le gros risque de cette technique et le même qu’en cas de *finesse + bluff* : Bob pourrait préférer profiter de
la ***position finesse*** de Carole pour donner lui-même une *finesse* ou un bluff.
Il devient alors très difficile de faire savoir à Bob et David qu’il s’agissait d’un *double bluff* et pas d’une *finesse* sans causer une bombe.

Surtout, n’utilisez **pas** le *double bluff* quand la personne après vous a une carte *unique* à protéger.

Attention également à la *promesse* de bluff. Celle-ci est beaucoup plus vague avec le *double bluff*, et vous pouvez décider collectivement 
de ne pas en tenir compte dans ce cadre, si vous jugez que ça n’en vaut pas la peine. Voici tout de même plus de détails.  
Si pour faire poser deux `1` je marque un `3` plutôt qu’un `2`, le deuxième `1` sera posé à la place d’un `2` manquant. Une *promesse* valide 
pour le `3` est donc un `3` d’une couleur où déjà un `1` est posé.

Ex :

[![L’indice promet au 3 d’être rouge ou jaune](images/double-bluff-avec-promesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(R1)%2CXX%2CXX%2CXX%7C(G1)%2CXX%2CXX%2CXX%7CXX%2C!(X3)%2CXX%2CXX%2C%3C-3&highestCardsPlayed=BX%2CRX%2CGX%2CY1%2CWX)

Ici, la *promesse* est donc un `Y3` ou un `R3`.

#### 🔧 Technique 18b – *Double bluff ordurier*

<a href="#technique-18b"></a>

De la même manière, vous pouvez bluffer deux personnes d’un coup avec un *bluff ordurier*.

Ex : 

- Tous les `3` ont été posés, sauf les `B3`, `R3` et `Bk3`. Bob et Carole ont respectivement un `Bk4` et un `Y5`, tous deux ***jouables***, 
non marqués et en ***position finesse***.
- Je donne l’indice `3` à David, marquant une carte *inutile*.

[![La pose du Bk4 ne révèle pas le bluff](images/double-bluff-ordurier.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(Bk4)%2CXX%2CXX%2CXX%7C(Y5)%2CXX%2CXX%2CXX%7CXX%2C!(G3)%2CXX%2CXX%2C%3C-3&highestCardsPlayed=Bk5%2CY4%2CR2%2CB2%2CG3%2CW3)

- Bob *répond* selon lui à une *finesse ordurière* ou a un *bluff ordurier*. Il pose sa carte en ***position finesse***, pour éviter
que David ne cause une bombe en tentant de poser la sienne.
- Carole  constate que pour l’instant, un `Bk4` a été posé à partir d’un indice `3`. Rien n’indique donc à David que son `3` marqué 
n’est pas le `Bk3`. Elle comprend qu’elle doit, elle aussi, *répondre* pour éviter la bombe, et pose son `Y5`.
- David sait maintenant que son `3` n’est pas ***jouable*** puisque deux personnes ont *répondu* à l’indice avec des cartes sans 
rapport entre elles. Il peut défausser son `3`.

Cette technique est très peu risquée, puisque Bob ne peut pas retarder sa *réponse*. Il sait que cela causerait une bombe.
Le seul risque, c’est si Carole a une carte ***en danger***. C’est la situation classique dans laquelle les bluffs sont à éviter.

#### 📌 Convention 25 – *Finesse indirecte*

<a href="#convention-25"></a>

Si une *finesse* pourtant évidente est donnée en deux indices plutôt qu’un seul, c’est qu’elle est *indirecte* : que la carte manquante 
est en seconde ***position finesse***.

Ex :

- Aucune carte n’a été posée. Carole a un `Bk5` et un `R5`, mais celui en ***position finesse*** n’est pas celui qui est ***jouable***. David a un seul 4 en main : le `Bk4`.

[![Le 5 ciblé par l’indice n’est pas celui en position finesse](images/finesse-indirecte.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7C!(R5)%2CXX%2C!(Bk5)%2CXX%2C%3C-5%2C4%7CXX%2C%3F(Bk4)%2CXX%2CXX%2C%3C-5%2C4)

- Je marque `5` les deux `5` de la main de Carole.
- Bob hésite à donner l’indice `R`, pour indiquer quel `5` est ***jouable***, mais donne finalement l’indice `4` à David.
- Carole s’apprête à poser son `5` en ***position finesse*** –  attendu par David –  mais réfléchit et constate que celui-ci
n’a aucun autre `4`. Il n’y avait donc aucune raison de ne pas le marquer directement pour donner la finesse `Bk`. Elle comprend que 
c’était tout simplement impossible, et pose à la place son `5` en seconde ***position finesse*** : le  `Bk5`.

Cette convention repose sur le contexte de la situation. Pour éviter trop d’ambiguïté, ne l’utilisez que si l’indice `4` ne marque pas de carte
collatérale.

#### 🔧 Technique 19 – *Finesse préemptive*

<a href="#technique-19"></a>


Si une personne *éloignée* a les deux dernière cartes d’une série en main, la première, ***jouable*** et en ***position finesse***, 
la seconde, en ***position défausse***, et que vous marquez cette dernière d’un indice apparemment *défensif*, c’est pour créer une
*finesse* avec la carte en ***position finesse*** de la personne **précédente**.

Ex :

- Le `R4` est ***jouable*** et Bob et Carole l’ont tous deux en ***position finesse***.
- Je marque `5` le `R5` de Carole, en ***position défausse***.

[![Bob pose son R4](images/finesse-preemptive.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(R4)%2CXX%2CXX%2CXX%7C(R4)%2CXX%2CXX%2C!(R5)%2C%3C-5%7CXX%2CXX%2CXX%2CXX&highestCardsPlayed=R3)

- Bob grimace en se disant qu’il voulait justement marquer `R` les deux cartes de Carole, ou bien bluffer le `R4`. Pour l’instant, mon indice 
tout seul marque juste une carte *unique* ***en danger*** et ne permet pas à Carole de poser de carte. Si Bob doit lui aussi donner un
indice, alors le mien n’a pas d’intérêt. Il me fait confiance, conclue qu’il s’agit d’une *finesse* et y répond en posant son `R4`.
- Carole pose son `R5` et la *finesse* est *résolue*, grâce à mon indice finalement *offensif*.

**Remarque** : Cela ne fonctionne que s’il s’agit des dernières cartes de la série. Un indice `rouge` donné à Carole lui aurait fait poser toutes
les cartes, grâce à la *jouabilité implicite*. Lui donner deux indices (`5` puis `rouge`) n’aurait donc pas de sens.

Le principal inconvénient de cette technique est le « vol » de la position de bluff. Bob aurait probablement pu protéger une carte
ailleurs en créant un bluff, surtout si l’on joue à beaucoup.

De plus, il est parfois préférable de marquer le `5` de Carole comme indice *défensif* plutôt que de tenter de lui faire poser son `4`. 
Ça peut être le cas si le nombre de jetons indice est dangereusement bas au vu des prochaines cartes à protéger, ou s’il est 
impossible de faire poser le `R4` sans marquer de cartes *inutiles*  collatérales.

**Note** : Si l’équipe est très familière de cette technique, elle peut tenter son corollaire : Carole peut alors défausser 
sa carte la plus récente, car elle est sûre que la *finesse* était « préemptive » et donc qu’elle a la même carte que Bob.

#### 📌 Convention 20c – *Bombe positionnelle facultative*

<a href="#convention-20c"></a>

Si quelqu’un effectue une *bombe positionnelle* alors qu’une *défausse positionnelle* était possible, c’est pour indiquer une carte ***jouable***
à l’emplacement correspondant dans la main de **deux** autres personnes de l’équipe.

Ex :

- Il reste deux cartes dans la pioche, et toutes les cartes ont été posées, sauf les `R4`, `R5` et `G5`. Il n’y a plus aucun jeton indice disponible.
- Les cartes les plus récentes de Bob et Carole sont justement ***jouables***, et je sais où se trouve la dernière carte *utile*, dans ma main.

[![Ma bombe fait poser deux cartes](images/bombe-positionnelle-facultative.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CX5%7C(R4)%2CXX%2CXX%2CXX%7C(G5)%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX&highestCardsPlayed=W5%2CR3%2CY5%2CG4%2CB5&nbRemainingCardsInDeck=2)

- Je cause une bombe en tentant de poser ma carte la plus récente.
- Bob devine que je connais la position de toutes les cartes *utiles* restantes, sans quoi je ne risquerais pas cette bombe. Si je voulais 
faire poser le `G5`, j’aurais juste utilisé une *défausse positionnelle*, voire une défausse normale, pour lui laisser un jeton indice.
- Il connaît la *bombe positionnelle facultative*, et pose son `R4`.
- Carole fait la même déduction et pose son `G5`.

Cette convention est très arbitraire (et donc peu jouée sur **BGA**), car même si quelqu’un d’expérimenté observera que le choix de la bombe
plutôt que de la défausse signifie « quelque chose de bizarre », rien de logique ne lui permet de savoir quoi faire à son tour.  
Elle reste cependant assez peu risquée (surtout s’il s’agit de la position la plus récente), et au pire, seule une personne  *répondra* au lieu
des deux.

#### 📌 Convention 26 – Double indice inversant

<a href="#convention-26"></a>

Si vous avez plusieurs cartes marquées et que vous savez celle en ***position finesse*** ***jouable***, alors si on vous
redonne le même indice pour les remarquer, alors vous devez les poser dans le sens inverse.

Ex :

- Je marque les trois cartes jaunes d’Eve (dans l’ordre `Y4`, `Y3` et `Y2`), pour faire poser le `Y1` de Bob via *finesse*.

[![Ma finesse Y va nécessiter une correction](images/double-indice-inversant.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(Y1)%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7CXX%2C!%3F(Y4)%2C!%3F(Y3)%2C!%3F(Y2)%2C%3C-Y%2CY)

- Bob pose son `Y1`.
- Carole redonne le même indice `jaune` à Eve.
- Eve pose sa carte `jaune` la plus ancienne (son `Y2`). Aux tours suivants, elle pose son `Y3` et son `Y4`, dans le bon
ordre.

Cette convention offre une chance d’économiser des indices lorsque les cartes sont dans l’ordre inverse. On ne doit
l’utiliser que dans ce cas-là. Si par exemple l’ordre des cartes était `4`-`2`-`3` ou `2`-`4`-`3`, marquer le `4` serait suffisant et beaucoup plus
explicite pour faire poser les trois cartes. Il reste seulement la combinaison `3`-`4`-`2`, qui nécessitera beaucoup d’indices.

Cette convention vaut également pour deux indices *valeurs*.

Ex :

- Carole a deux `2` ***jouables***, mais avec un `M2` *inutile* juste devant.
- Je marque ces trois cartes d’un indice `2`.
- Bob sait qu’il doit apporter un indice de correction, mais ne veut pas marquer plus de cartes inutiles. Il redonne le même
indice `2`.

[![Ma finesse Y va nécessiter une correction](images/double-indice-valeur-inversant.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%2CXX%7C(M1)%2C!%3F(M2)%2C!%3F(W2)%2C!%3F(B2)%2CXX%2C%3C-2%2C2&highestCardsPlayed=B1%2CG5%2CM3%2CR4%2CW1%2CY3)

- Carole pose ses `2` dans le sens inverse, et s’arrête une fois qu’il ne lui reste que son `M2`. Comme tous les `2` ont alors
été posés, elle peut s’en défausser.

#### 🔧 Technique 20 – *Renversée auto-annulative* (en anglais : *self color bluff*)

<a href="#technique-20"></a>

Vous pouvez donner à quelqu’un un indice *couleur* sur une de ses cartes déjà marquées, pour lui faire poser sa carte non marquée en 
***position finesse***.

Ex :

- Bob a un `Bk4` ***jouable*** en ***position finesse*** et un `R2` marqué `2` en main. Personne n’a de carte `rouge` ***jouable***  en ***position finesse***.
- Je donne l’indice `R` à Bob. Sa carte est maintenant marquée `R2`.
- Bob regarde dans la main de ses partenaires, mais ne voit pas de carte ***jouable*** en ***position finesse***.
Il comprend que s’il ne fait rien, Carole causera une bombe en croyant *répondre* à une *renversée* `rouge`.
Il pose sa carte non marquée en ***position finesse*** (le `Bk4`), sachant très bien qu’elle ne peut pas être un `R1`.
- Carole comprend que l’indice n’était pas pour elle puisqu’il a fait poser une carte non marquée, comme avec un bluff. Elle n’a pas besoin
de *répondre* car l’indice est *résolu*.

[![Bob répond en sachant que sa carte ne correspond pas à l’indice](images/renversee-auto-annulative.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(Bk4)%2C(X4)%2CXX%2C(X4)%2C!(R)2%2C%3C-R%7CXX%2CXX%2CXX%2CXX%2CXX&highestCardsPlayed=Bk5%2CB1%2CRX&discardedCards=B4)

Cette technique offre une façon supplémentaire de faire poser une carte sans la marquer (et donc sans le marquage collatéral qui va avec).
Elle est beaucoup moins puissante qu’un bluff, car ne protège aucune carte.

Elle ne fonctionne (quasiment) qu’avec un indice *couleur*, puisque la personne qui le reçoit doit être sûre qu’elle n’a pas la carte manquante
(ici le `R1`). Sans ça, elle devrait continuer de poser ses cartes en ***position finesse*** jusqu’à la trouver. 
(Ex : Le `R2` était marqué `R` et je le marque `2`. Bob essaiera de poser un `R1` sans savoir qu’il n’en a pas.)

Attention, cette technique comporte le même risque qu’un bluff. Si Bob ne *répond* **pas** à l’indice et préfère faire autre chose,
une personne après lui risque de causer une bombe.

**Note** : Cette technique semble très peu utilisée sur [BGA](https://boardgamearena.com/), mais quelqu’un d’expérimenté devrait
naturellement comprendre qu’une *réponse* est requise,  et poser sa carte non marquée en ***position finesse***, pour éviter une bombe.

#### 🔧 Technique 21 – *Bluff retardé* (en anglais : *delayed bluff*)

<a href="#technique-21"></a>

Vous pouvez créer un *bluff* qui ne sera révélé qu’à un tour ultérieur, et ce, sans que personne n’ait l’occasion de causer une bombe.

Ex :

- Tous les `1` ont été posés, sauf le `Bk1`, présent dans la main de Bob, derrière un `Bk5` et un `R3`, tous deux ***jouables*** et en
***position finesse***. Mon `Bk3` est connu et Carole a un `Bk4` marqué `4` en main.

[![Exemple de bluff multiple à trois joueurs](images/bluff-retarde.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CBk3%2CXX%7C(Bk5)%2C(R3)%2CXX%2C!(Bk1)%2CXX%2C%3C-1%7CXX%2CXX%2C(Bk)4%2CXX%2CXX&highestCardsPlayed=R2%2CB2%2CW4%2CG2%2CY4%2CBkX)

- Je donne l’indice `1` à Bob, marquant son `Bk1`. Bob sait qu’il s’agit d’un indice *offensif* et pose donc la première 
carte manquante  à la *finesse* `Bk`.
- Carole voit un `Bk5` posé depuis un `Bk1` marqué, ainsi que le `Bk3` dans ma main. Il manque donc le `Bk4` et le `Bk2` pour que 
le `1` devienne ***jouable***. Elle pose sa carte marquée `4`, car c’est la ***position finesse*** pour le `Bk4`.
- Je pose mon `Bk3` devenu ***jouable***.
- Bob constate qu’il manque le `Bk2` pour rendre son `1` ***jouable***, et puisqu’il ne le voit pas, pose son `R3`.

Une carte ne correspondant pas à l’indice est posée, *révélant* qu’il s’agissait d’un bluff et non pas d’une *finesse* : l’indice est *résolu*.
Personne n’a eu l’occasion de causer une bombe.

#### Conclusion

Vous savez quelles situations risquent de causer une bombe, et quand fournir une *réponse*, peu importe le type d’indice. 
Cela vous permet de créer des techniques de bluff « avancées ».

On peut encore parler d’un niveau de jeu supérieur : le jeu « en *couches* ».


### Expert

<a href="#expert"></a>

Le jeu en *couches* introduit beaucoup de complexité, mais permet de faire poser encore plus de cartes avec un seul indice.

#### 📌 Convention 27 – Jeu en *couches*  (en anglais : *layered*)

<a href="#convention-27"></a>

Si la première carte en ***position finesse*** est ***jouable***, alors la seconde peut être utilisée dans une *finesse*.
Si les deux premières sont ***jouables***, alors même principe pour la troisième, et ainsi de suite.

C’est sur cette convention que reposent les techniques suivantes : la *finesse généreuse*, la *finesse généreuse renversée*
et la *finesse méfiante*.

En vérité, vous utilisez déjà le jeu en *couches* d’une certaine manière, avec le *semi-bluff* et la *défausse gentleman en couches* :
la carte ciblée est la énième carte en ***position finesse***.

#### 🔧 Technique 22 – *Finesse généreuse* et *renversée généreuse*  (en anglais : *layered finesse*)

<a href="#technique-22"></a>

Quand quelqu’un *d’éloigné* a plusieurs cartes ***jouables*** en ***position finesse***, vous pouvez créer une *finesse* pour la dernière. 
Cela donnera lieu à autant de *réponses* que nécessaire, pour que la carte marquée par l’indice devienne ***jouable***.

Les cartes précédentes, ne faisant pas partie de la *finesse* seront, elles aussi, posées en bonus.

Ex : 

- Les `R3` et `Y2` sont ***jouables*** et en ***position finesse*** chez Carole. David a un `Y3` en main.
- Je donne l’indice `Y` sur le `Y3`.

[![Un R3 est posé en bonus](images/finesse-genereuse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7C(R3)%2C(Y2)%2CXX%2CXX%7CXX%2C!(Y3)%2CXX%2CXX%2C%3C-Y&highestCardsPlayed=R2%2CY1)

- Bob voit le `Y2` manquant et comprend que je cherche à faire poser 3 cartes. Il joue quelque chose d’indépendant à mon indice, sans
marquer ni le `R3` ni le  `Y2` de Carole.
- Carole *répond* en pensant poser  le `Y2`, mais pose en fait son `R3`.
- David comprend qu’il s’agit d’une *finesse généreuse* et joue quelque chose d’indépendant à mon indice.
- Même chose pour moi et Bob.
- Carole a vu avec surprise que la carte qu’elle a posée au tour précédent ne correspondait pas à l’indice. Pourtant, cela ne pouvait pas 
être un bluff, sinon Bob y aurait *répondu*, cherchant à poser le `Y2`. C’est donc bien elle qui a cette carte : à fortiori la seconde 
en ***position finesse*** au tour précédent. Elle pose son `Y2`.
- David peut désormais poser son `Y3` marqué.

Cela fonctionne **uniquement** parce que Bob ne *répond* **pas** à mon indice. Cela invalide dès lors la possibilité d’un bluff
(même distant). Notez que Bob n’aurait pas pu donner lui-même cet indice (mais seulement un *bluff finesse* classique).  
De la même manière, on peut faire poser **plusieurs** cartes bonus en plus de celles de la *finesse*.

Cette technique est utilisable, même quand on joue à 3, quand elle est *renversée*.

Ex :

- Les `R3` et `Y2` sont ***jouables*** et en ***position finesse*** chez Carole. Bob a un `Y3` en main.
- Je donne l’indice `Y` sur le `Y3`.

[![Un R3 est posé en bonus](images/renversee-genereuse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7CXX%2C!(Y3)%2CXX%2CXX%2CXX%2C%3C-Y%7C(R3)%2C(Y2)%2CXX%2CXX%2CXX&highestCardsPlayed=R2%2CY1)

- Bob se dit qu’il y a peu de chance pour qu’il ait un `Y2` en main alors que Carole aussi, et comme par hasard, juste après 
une autre carte ***jouable*** en ***position finesse***. Il soupçonne la *renversée généreuse* et joue quelque chose d’indépendant à 
mon indice et sans marquer les cartes de Carole.
- Carole pense *répondre* à une *renversée* `jaune` classique, mais pose en fait son `R3`.
- Je joue quelque chose d’indépendant à mon tour.
- Bob a vu poser un `R3` à partir de l’indice `jaune` qu’il a reçu. Il a donc confirmation que sa carte n’est **pas** ***jouable*** 
puisque l’indice a occasionné une *réponse*. Une fois encore, il joue quelque chose d’indépendant à l’indice.
- Carole a vu avec surprise que la carte qu’elle a posée au tour précédent ne correspondait pas à l’indice. Pourtant, si Bob 
par deux fois n’a pas causé de bombe, c’est qu’il attend toujours une *réponse*. Elle comprend qu’il s’agissait d’une 
*renversée généreuse* et pose son `Y2`.
- Bob peut enfin poser son `Y3` marqué.

Dans les deux cas, un risque repose sur Bob. S’il ne reconnaît pas la technique, il risque de *répondre* à l’indice et de causer une bombe.

#### 🔧 Technique 23 – *Finesse méfiante* (en anglais : *clandestine finesse*)

<a href="#technique-23"></a>

Quand quelqu’un a plusieurs cartes ***jouables*** en ***position finesse***, vous pouvez créer une *finesse* pour la dernière, avec 
un indice volontairement « flou ». Cela donnera lieu à autant de *réponses* que nécessaire, pour que la carte marquée par l’indice 
devienne  ***jouable***.

Ex : 

- Les `R2` et `Y2` sont ***jouables*** et en ***position finesse*** chez Bob. Carole a un `Y3` en main.
- Je donne l’indice `3` sur le `Y3`.

[![Carole attend avant de poser son 3](images/finesse-mefiante.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CXX%7C(R2)%2C(Y2)%2CXX%2CXX%2CXX%7CXX%2C!(Y3)%2CXX%2CXX%2CXX%2C%3C-3&highestCardsPlayed=R1%2CY1%2CBX)

- Bob pose sa carte `R2` en ***position finesse***, pensant *répondre* avec un `Y2`. 
- Carole s’apprête à poser sa carte marquée, mais suspecte une *finesse méfiante*. Elle joue quelque chose d’indépendant à mon indice.
- Même chose pour moi.
- Bob a vu que Carole n’a pas essayé de poser son `3` (ce qui aurait d’ailleurs causé une bombe), comme si elle se doutait qu’il 
n’est pas ***jouable*** et qu’elle attendait une *réponse* au préalable. Il comprend qu’il s’agissait d’une *finesse méfiante*, qu’il a bien 
un `Y2`, et le pose.
- Carole peut maintenant poser son `Y3` marqué.

Cette technique est la version avancée du *semi-bluff*. Elle repose sur les mêmes interprétations, sauf que cette fois, aucune des cartes 
***jouables*** de Bob n’était marquée au moment de l’indice.

Contrairement à la *finesse généreuse*, la première carte posée correspond cette fois à l’indice. C’est donc à la personne qui le reçoit de 
se méfier et de suspecter la technique employée. Sans quoi elle causera une bombe.

Notez que vous pouvez combiner *finesse méfiante* et *finesse généreuse*. On peut imaginer une carte bonus ***jouable*** entre les 
deux `2` de cet exemple  (ex : un `B1`).

#### 📌 Convention 23b – Ordre des *réponses* et jeu en *couches*

<a href="#convention-23b"></a>

Si vous être en train de *répondre* à un indice par du jeu en *couches*, vous devez continuer à poser les cartes dans l’ordre, même si on 
vous donne d’autres *finesses* en parallèle. Ensuite, éventuellement, vous répondrez aux autres *finesses*.

Ex : 

- Seuls les `R2` et `Y1` on été posés.
- J’ai pu créer une finesse généreuse en marquant `jaune` le `Y3` de David, puisque que Carole avait en ***position finesse*** les `R3`, `B1` 
et `Y2`. Elle y a *répondu* une première fois en posant son `R3`.
- Bob sait qu’elle va poser un `B1` et marque tout de suite le `B2` de David.

[![Carole continue à jouer le jeu en couches](images/ordre-reponses-jeu-en-couches.png)
](https://hanabi.devnotebook.fr/eyJmaXJld29ya3MiOlt7ImNvbG9yIjoicmVkIiwidmFsdWUiOjN9LHsiY29sb3IiOiJ5ZWxsb3ciLCJ2YWx1ZSI6MX0seyJjb2xvciI6ImJsdWUifV0sImRpc2NhcmRQaWxlIjp7fSwicGxheWVycyI6W3sibmFtZSI6IkFsaWNlIiwiaGFuZCI6W3t9LHt9LHt9LHt9XX0seyJuYW1lIjoiQm9iIiwiaGFuZCI6W3t9LHt9LHt9LHt9XX0seyJuYW1lIjoiQ2Fyb2xlIiwiaGFuZCI6W3t9LHsicmVhbENvbG9yIjoiYmx1ZSIsInJlYWxWYWx1ZSI6MX0seyJyZWFsQ29sb3IiOiJ5ZWxsb3ciLCJyZWFsVmFsdWUiOjJ9LHt9XX0seyJuYW1lIjoiRGF2aWQiLCJoYW5kIjpbe30seyJyZWFsQ29sb3IiOiJ5ZWxsb3ciLCJyZWFsVmFsdWUiOjMsImNsdWUxIjp7ImNvbG9yIjoieWVsbG93IiwibnVtYmVyIjoxfX0seyJjbHVlMSI6eyJjb2xvciI6ImJsdWUiLCJudW1iZXIiOjJ9LCJyZWFsQ29sb3IiOiJibHVlIiwicmVhbFZhbHVlIjoyfSx7fV19XX0=)

- Carole sait qu’elle a deux cartes *manquantes* (le `B1` et  le `Y2`) et continue à *répondre* à la *finesse généreuse* en posant le `B1`.
- David pose son `B2`.
- Au prochain tour, Carole et David compléteront la *finesse* `jaune`.

Cette convention n’est pas forcément logique et est en effet en opposition avec l’ordre des *réponses* présenté précédemment.
Elle simplifie néanmoins les choses pour la personne qui doit *répondre* en couches. Ici, Carole *répond* aux *finesses* dans l’ordre 
des indices qu’elle a reçus.

Si le `B1` est en ***position finesse*** au moment où Bob donne son indice, cela fonctionne également. Carole pose d’abord son `Y2` et,
au prochain tour, elle posera son `B1` en se rappelant qu’il aura bougé d’un emplacement dans sa main.

Notez que l’inconvénient de cette convention, c’est qu’elle interdit à Bob de bluffer Carole. En effet, puisqu’elle doit d’abord finir sa première *réponse*,
elle ne *révélera* pas le bluff avant le tour de David, qui causera une bombe.

#### 📌 Convention 19c – Longue *promesse* de bluff

<a href="#convention-19c"></a>

La convention de *promesse* de bluff peut être étendue pour *promettre* plusieurs cartes.

Ex :

- Le `B1` a été posé, contrairement au `B2`. Bob a une carte ***jouable*** non marquée en ***position finesse*** et Carole et David ont
respectivement un `B3` et un `B4` en ***position finesse***.
- Je donne l’indice `B`  à David.

[![Le bluff promet un B3 non marqué](images/longue-promesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(R1)%2CXX%2CXX%2CXX%7C(B3)%2CXX%2CXX%2CXX%7C!(B4)%2CXX%2CXX%2CXX%2C%3C-B&highestCardsPlayed=B1%2CRX)

- Bob *répond* au bluff en posant son `R1`.
- Carole constate que ce bluff *promet* un `B3` à David, ce qui est un mensonge. Un `B3` est donc disponible quelque part et
comme elle ne le voit pas, c’est qu’il se trouve en ***position finesse*** dans sa main.
- David a bien vu le `B3` et la *réponse* au bluff. Il devine que l’indice lui marquait un `B4`.
- La partie continue, et Carole retient où se trouve le `B3` dans sa main, évitant de le défausser, et ce, **sans qu’on ait besoin de le
marquer**.
- Dès que le `B2` est posé, Carole et David posent leurs `B3` et `B4`.

Cette convention permet de riches *promesses*, mais elle nécessite une bonne mémoire (pour Carole, dans cet exemple).

#### 📌 Convention 15f – Promesse de `Bk1`

<a href="#convention-15f"></a>

Si quelqu’un à un `Bk1` en ***position finesse*** il peut être en mesure de déduire où il se trouve. Dans ce cas, on ne doit **pas** le marquer
pour le protéger car ce n’est pas nécessaire. Un tel indice doit donc être considéré comme *offensif* et non pas *défensif*.

Voici un cas simple où cela peut se produire :

- C’est le premier tour de jeu, je donne l‘indice `1` à Bob pour marquer sa carte en ***position défausse***.

[![Je marque un 1 en position défausse chez Bob](images/bk1-deduction.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2C!(R1)%2C%3C-1%7C(Bk1)%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX&highestCardsPlayed=BX%2CGX%2CRX%2CWX%2CYX%2CBkX)

- Bob pose ce `1`, car il voit le `Bk1` en ***position finesse*** chez Carole.
- Carole sait qu’elle a un `Bk1` en main, et donc qu’elle ne doit pas défausser au moins une de ses cartes.
- Les tours passent et sans information sur sa main, Carole a défaussé trois cartes (à chaque fois celle en ***position défausse***, 
comme d’habitude). Il ne lui reste donc plus qu’une des cartes qu’elle avait en main au moment où Bob a posé son `1` et elle se trouve
actuellement en ***position défausse***. C’est donc nécessairement le `Bk1` *promis* et Carole va éviter de défausser cette carte.

Cela ne fonctionne que parce que le `Bk1` était en ***position finesse***. Sans quoi, l’équipe aurait du le protéger une fois qu’il devenait 
***en danger***. Carole ne sait pas où se trouve la carte avant d’avoir défaussé les trois autres.

#### 🔧 Technique 18c – Bluff multiple

<a href="#technique-18c"></a>

Le *double bluff* peut être étendu à 3, voire 4 cartes, le temps que le bluff soit *révélé*.

Ex :

- Aucune carte `B` ou `Bk` n’a été posée, alors que la série `R` va jusqu’au `R3`. Personne n’a d’information sur sa main.
- Je donne l’indice `5` à Carole, marquant son `R5`.

[![Quatre cartes sont posées afin de révéler le bluff](images/bluff-multiple.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7C(B1)%2CXX%2CXX%2CXX%7C!(R5)%2C(B2)%2CXX%2CXX%2C%3C-5%7C(B3)%2CXX%2CXX%2CXX%7C(Bk5)%2CXX%2CXX%2CXX&highestCardsPlayed=R3%2CBX%2CBkX)

- Bob pose son `B1`, pensant *répondre* à une *finesse* `R`.
- Carole voit un `1` posé depuis un indice `5`, et le `B3` correspondant en ***position finesse*** chez David.
Elle pense donc avoir en ***position finesse*** un `B2` puis un `B4`, avec un `B5`. Elle pose son `B2`.
- David sait qu’il ne s’agit pas d’une *finesse*, mais d’un bluff, puisque la carte marquée par l’indice ne correspond pas aux cartes
posées. Cependant, Carole ne le sait pas et elle continuera de *répondre* à l’indice en posant ses cartes jusqu’à ce que son `5` devienne
***jouable***. David *répond* donc lui-même, en posant son `B3`.
- Eve a la même réflexion : le bluff n’est toujours pas *révélé* à Carole, et elle doit donc *répondre*, pour lui éviter une bombe. Elle pose 
son `Bk5`.

Une carte qui ne correspond pas à l’indice est posée, preuve connue de tout le monde qu’il s’agissait d’un bluff. Le bluff est donc *révélé*, et l’indice est *résolu* : plus aucune *réponse* n’est attendue.

#### 🔧 Technique 24 – *Finesse ordurière généreuse* (en anglais : *layered trash finesse*)

<a href="#technique-24"></a>

Vous pouvez créer une *finesse ordurière*, faisant poser en bonus une ou plusieurs cartes en ***position finesse***, avant que la *finesse*
ne soit *résolue*.

Ex :

- Carole a en ***position finesse*** un `R4` ***jouable***, puis la dernière carte `verte` qui reste à poser.
- Je marque une carte *inutile* de David d’un indice `vert`.

[![Je marque la carte verte inutile de David](images/finesse-orduriere-genereuse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7C(R4)%2C(G5)%2CXX%2CXX%7CXX%2C!(GX)%2CXX%2CXX%2C%3C-G&highestCardsPlayed=B5%2CG4%2CR5%2CW3%2CY4)

- Bob défausse.
- Carole sait qu’il s‘agit d’une *finesse ordurière*, et *répond* en posant sa carte en ***position finesse***, sans quoi David causera une 
bombe en tentant de poser la sienne. Elle constate avec surprise qu’il ne s’agit pas du `G5`.
- David défausse sa carte `verte`.
- Au prochain tour, Carole continue de poser ses cartes jusqu’à trouver le `G5`.

Comme pour la *finesse généreuse*, tout repose sur le fait que Bob ne *réponde* pas à l’indice. Cela élimine dès lors la possibilité d’un bluff, 
et c’est donc forcément du *jeu en couches*.

#### 🔧 Technique 14b – *Défausse gentleman corrective* (en anglais : *patched gentleman discard*)

<a href="#technique-14b"></a>

Vous pouvez utiliser une *défausse gentleman en couches* « invalide », et ainsi faire poser une carte manquante.

Dans ce cas, puisque la personne ayant la copie de la carte défaussée s’apprête à poser une carte pas encore ***jouable***, quelqu’un doit
*répondre* en posant la carte manquante.

Ex :

- Carole a en ***position finesse*** deux cartes qu’elle ne connaît pas : le `B3` non ***jouable***, puis le `R3` ***jouable***.
- J’ai un `R3` en main que je connais. Je le défausse.

[![Bob joue une carte en bonus pour éviter la bombe](images/defausse-gentleman-corrective.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%2CR3%7C(B2)%2CXX%2CXX%2CXX%2CXX%7C(B3)%2C(R3)%2CXX%2CXX%2CXX&highestCardsPlayed=R2%2CB1)

- Bob comprend que je veux faire poser le `R3` à Carole à ma place. Pourtant, s’il ne fait rien, Carole va d’abord causer 
une bombe en tentant de poser son `B3`. Il reconnaît une *finesse* et pose son `B2`, qui était en ***position finesse***.
- Carole ne voit l’autre `R3` nulle part. Elle sait donc qu’elle l’a en main, et essaie de le poser. À la place, elle pose le `B3` et comprend 
alors comment le `B2` a pu être posé « sans indice ». Elle sait que le `R3` est à la suite dans sa main et le posera au prochain tour.

Théoriquement, plusieurs cartes peuvent ainsi être posées en *réponse* avant le tour de la personne ciblée par la *défausse gentleman*.
Une fois encore, cette technique génère une complexité supplémentaire. Évitez d’aller jusqu’à tenter une *longue finesse renversée*
via votre *défausse gentleman*.

Cette technique est assez peu risquée. Au pire, si la personne après vous ne vous fait pas assez confiance, elle pensera qu’un indice 
de correction est nécessaire, et choisira juste de marquer le `R3` via un nouvel indice. La *finesse* `bleue` sera alors annulée et devra être 
redonnée de manière plus conventionnelle.


#### 🔧 Technique 14c – *Défausse gentleman finesse*

<a href="#technique-14c"></a>

Si vous défaussez une carte *connue* mais pas encore ***jouable***, alors cela doit créer une *finesse* par *défausse gentleman*.

Ex :

- Carole a en ***position finesse*** un `B3` qu’elle ne connaît pas, non ***jouable***, puisque le `B2` n’a pas été posé. Bob a justement
cette carte en ***position finesse***.
- J’ai un `B3` en main que je connais. Je le défausse.

[![Je défausse un B3 non jouable](images/defausse-gentleman-finesse.png)
](https://hanabi.devnotebook.fr/eyJmaXJld29ya3MiOlt7ImNvbG9yIjoiYmx1ZSIsInZhbHVlIjoxfV0sImRpc2NhcmRQaWxlIjp7fSwicGxheWVycyI6W3siaWQiOiJmMTJkNjljOS0zM2I5LTQ3N2MtODNkZC04NzAxOGJhODFkYTEiLCJuYW1lIjoiQWxpY2UiLCJoYW5kIjpbeyJpZCI6IjdlZTUzNmFjLWNhZmYtNDBiOC1hODlkLTE4ODkxMjk4YzljNiJ9LHsiaWQiOiIwOTRiYmRjYS1iZWY3LTQ1MmItYmVjNS0xMTdiM2Q4OTQxNmIifSx7ImlkIjoiN2VlNDE1YWEtYjlkOS00ZTliLTllOWQtNTUyZWQwY2YyYmM1In0seyJpZCI6IjJmZDg3YmRjLTM2YTktNGMxYi1iNGFhLWU4NjA3NjMzNTlmMiJ9LHsiaWQiOiJhYjMzMDhiOS1hN2U3LTQ4ZjgtODQxMy0xYzA4NjljZWYzMjkiLCJrbm93bkNvbG9yIjoiYmx1ZSIsImtub3duVmFsdWUiOjN9XX0seyJpZCI6IjA1NjgyNDMyLTVhMTgtNGEzYi05NTA2LTU2YTg2YjZiMWNiMyIsIm5hbWUiOiJCb2IiLCJoYW5kIjpbeyJpZCI6IjZjMTNhMWRhLWIyYmMtNGViNy1iNzQ3LWIyM2EyYTNkYzBkNyIsInJlYWxWYWx1ZSI6MiwicmVhbENvbG9yIjoiYmx1ZSJ9LHsiaWQiOiI5YTAwZGNhNy1iZDIyLTQ2YmQtYWJlOS02MmRjZTI1NzQzZGMifSx7ImlkIjoiYzk4ZjgzOGItYjk1ZC00Y2VmLTk5ZDEtNzgzMmRkNWVmMDU0In0seyJpZCI6IjNjMjk5M2U2LWVmOTctNDQ2Yi1hMGNkLWZiNjllMDg2ZmVjMiJ9LHsiaWQiOiIxNmUyZmM0MS1iOTEyLTQ4NmItODU3Yi02NWUxOWVhN2NjNGQifV19LHsiaWQiOiI3NjM0ZmRiMy0xZDFhLTQwYzUtOTQ2MS0xMzdiZWI3MzJiMWIiLCJuYW1lIjoiQ2Fyb2xlIiwiaGFuZCI6W3siaWQiOiJlOTNjNDAwNC1iNTY4LTRhOGItYjhlOC0zYTk3OGZlNjRhNDkiLCJyZWFsVmFsdWUiOjMsInJlYWxDb2xvciI6ImJsdWUifSx7ImlkIjoiNGIxYjc5YzAtZTk3Yy00YzQwLWE3NTItOTQyNThkNTBiZDY5In0seyJpZCI6IjcxYWUwZDY1LTlhODItNDhhYy1hYzc3LTQyNTM0YmIzN2U0YyJ9LHsiaWQiOiI5OWY1NWYxMi03MTljLTRmMDYtOTdjNC05NzgwYThkY2UwNTEifSx7ImlkIjoiOTA3YjRkZDctYzFmZC00Nzk5LTk4MzMtNDViNzkyZGNjZjZmIn1dfV19)

- Bob trouve dommage que je défausse ma carte, même s’il voit effectivement l’autre exemplaire en ***position finesse*** chez Carole. 
Il comprend que ma défausse d’une carte connue (et qui plus est, très *utile*) n’a d’intérêt que pour créer une *finesse*. Comme il manque
un `B2`, il devine où il se trouve et le pose.
- Carole a reçu une *défausse gentleman*, et elle constate que le `B3` est devenu ***jouable***. Elle le pose.

Cette technique introduit pas mal de complexité, et ne devrait être utilisée qu’en cas de pénurie de jetons indice, ou quand les mains sont
proches d’être bloquées ou ***en danger***. Si je n’ai aucun jeton disponible à mon tour de jeu, et que le `B2` est en ***position défausse***
chez Bob (parce que tout le reste de sa main est marqué `5,`par exemple) alors c’est une très bonne opportunité d’utiliser cette technique.

L’intérêt, en plus d’être utilisable même sans jeton indice, c’est de pouvoir déclencher une *finesse* même sans carte cible. Dans l’exemple
précédent, si j’avais un jeton indice, je ne pourrais pas marquer le `B3` de Carole. Elle croirait soit qu’il s’agit du `B4` (si je lui donne 
l’indice `bleu`), soit qu’il s’agit d’un bluff sur un autre `3` (si je lui donne l’indice `3`). Dans les deux cas, elle ne jouera pas sa carte marquée,
et devra peut-être défausser une carte *utile* à la place.

Attention, si à mon tour ma main est bloquée et que je n’ai plus de jeton indice, alors je dois forcément défausser une carte marquée. Dans
ce cas, ma défausse ne doit pas déclencher de *réponse*. Je l’ai fait par défaut, plutôt que de défausser une carte *unique*.

Théoriquement, cette technique peut faire poser plusieurs cartes manquantes, et elle est compatible avec la *défausse gentleman corrective*.

#### 🔧 Technique 25 – *Finesse corrective* (en anglais : *patched layered finesse*)

<a href="#technique-25"></a>

Vous pouvez créer deux *finesses* simultanément, lorsque la première est **en couches** et nécessite la seconde pour pouvoir être
*résolue*. C’est la même mécanique que pour la *défausse gentleman corrective*.

Ex :

-  Le `R2` et le `Y1` ont été posés. Carole a en ***position finesse*** un `R3`, un `Y3`, puis un `B1`. Les trois seraient  ***jouables*** 
si seulement un `Y2` était posé. C’est justement la carte que David a en ***position finesse***. 
Si elle était déjà posée, une *finesse généreuse* serait possible en marquant `B` le `B2` d’Eve.

[![David pose son Y2 pour éviter la bombe](images/double-finesse.png)
](https://hanabi.devnotebook.fr/?playersHand=XX%2CXX%2CXX%2CXX%7CXX%2CXX%2CXX%2CXX%7C(R3)%2C(Y3)%2C(B1)%2CXX%7C(Y2)%2CXX%2CXX%2CXX%7CXX%2CXX%2C!(B2)%2CXX%2C%3C-B&highestCardsPlayed=R2%2CY1%2CBX)

- Je marque `B` le `B2` d’Eve.
- Bob réfléchit et voit que toutes les cartes nécessaires pour que la carte marquée devienne ***jouable*** lui sont visibles.
Il joue quelque chose d’indépendant à mon indice.
- Carole pose son `R3`, s’attendant plutôt à voir un `B1`. Ce n’est pas le cas, pourtant l’indice ne pouvait pas être un bluff, sinon 
Bob y aurait  *répondu*. Elle comprend que c’est du jeu *en couches* et qu’elle doit continuer de poser ses cartes jusqu’à trouver le
`B1` permettant de *résoudre* la *finesse*.
- David sait ce que Carole a déduit, et constate qu’au prochain tour, cela causera une bombe, puisque le `Y3` n’est pas 
***jouable***. Il ne voit nulle part de `Y2` en ***position finesse*** et pose donc le sien.
- Eve a vu deux personnes poser des cartes non marquées. Sa carte `B` n’est donc pas ***jouable*** et elle n’a pas de *réponse* à
donner. Elle fait quelque chose d’indépendant à son tour.
- Même chose pour moi et Bob.
- Carole a vu avec surprise un `Y2` non marqué posé, après son `R3`, mais comme il n’y a pas de bluff, un jeu *en couche* est toujours en
cours. Elle pose son `Y3` et comprend alors comment le `Y2` a été posé. 
- Au prochain tour, elle posera enfin son `B1` et Eve son `B2`.

Cette technique n’est pas très risquée, et au pire, si Bob ne la connaît pas ou doute de la réaction de David, il peut donner
l’indice `Y` à Carole (ou à David) et expliciter la deuxième *finesse* pour faire poser le `Y2`. Carole déduira alors où se trouve son `B1`
un tour plus tôt.

#### 💡 Astuce 19 – Refus de *finesse ordurière*

<a href="#astuce-19"></a>

Quand on vous donne une *finesse ordurière* ou un *bluff ordurier*, vous pouvez laisser la personne **après** vous y *répondre* 
« à votre place ».

Ex :

- Bob a une carte connue ***jouable***. De plus, lui et Carole ont respectivement un `Y3` et un `G3` ***jouables*** en ***position finesse***.
- Je donne l’indice `1`, marquant un `B1` ***non jouable*** à David.

[![Bob refuse de jouer la finesse ordurière](images/refus-finesse-orduriere.png)
](https://hanabi.devnotebook.fr/eyJmaXJld29ya3MiOlt7ImNvbG9yIjoiYmx1ZSIsInZhbHVlIjoyfSx7ImNvbG9yIjoiZ3JlZW4iLCJ2YWx1ZSI6Mn0seyJjb2xvciI6InJlZCJ9LHsiY29sb3IiOiJ3aGl0ZSIsInZhbHVlIjoxfSx7ImNvbG9yIjoieWVsbG93IiwidmFsdWUiOjJ9XSwiZGlzY2FyZFBpbGUiOnsiZ3JlZW4iOlsiMyJdfSwicGxheWVycyI6W3siaWQiOiJmOGI2M2VlZS03YmM0LTRmNWEtYWEzNS1jMTE2ODQ3ODJkMDciLCJuYW1lIjoiQWxpY2UiLCJoYW5kIjpbeyJpZCI6ImM0NTI5ZDI3LTczMmEtNDE0ZS05ZTM5LTM0YjM1MTY1MWYyNyJ9LHsiaWQiOiJmN2IxNTBjZS0wNzRlLTRkOGQtOTlmZi1hZjFhZTlhYTlhNzEifSx7ImlkIjoiNzgwNGNiYjktYmViOC00NmY0LThmYmMtZTQxZTNlYzVjMzUyIn0seyJpZCI6ImM0OTgyMzc1LWIzOWYtNDRjYS04MDk3LTMzZTk1NDJhNWNmMSJ9XX0seyJpZCI6IjRmZjhmNTIyLTk1NGYtNDc0YS04NjE0LTYxYjViYTU3ZTRiOSIsIm5hbWUiOiJCb2IiLCJoYW5kIjpbeyJpZCI6ImY5M2FiNTY5LTIzZTQtNDNjZC04MTdmLTU2OWIxNzM2OTEwYiIsInJlYWxDb2xvciI6InllbGxvdyIsInJlYWxWYWx1ZSI6M30seyJpZCI6Ijk2ZWQ4ODMzLTc2NjEtNGEyZC1iNGE1LTU2MTQ0YmQ5YjRjMyIsImtub3duVmFsdWUiOjJ9LHsiaWQiOiI2MTNlNGUyZi1jZmYxLTRiZTQtYjVmMC00YTQ2YzllYzRmMTcifSx7ImlkIjoiZDQ0NjFiZTMtZDcyZC00YzFiLTg4MTMtZmVhOGY2MjQyMWQwIiwia25vd25WYWx1ZSI6Miwia25vd25Db2xvciI6IndoaXRlIn1dfSx7ImlkIjoiYTkwNmE0NjgtYjhlNi00MjE1LWI5YjEtM2JjM2M1N2RiZTBjIiwibmFtZSI6IkNhcm9sZSIsImhhbmQiOlt7ImlkIjoiNDc3N2UzNzAtNjRjZC00YmQxLTljZGYtOThlMjM1MjVlMDViIiwicmVhbFZhbHVlIjozLCJyZWFsQ29sb3IiOiJncmVlbiJ9LHsiaWQiOiJiYTVkM2JlYi01ZGUzLTRiYjItYjBkNC0yYTA2M2I5ZDg3ZjUifSx7ImlkIjoiYzU0ZTJjNTQtYmJlMC00OGFkLWIxMzAtMDRjMGYyNDBmM2IwIn0seyJpZCI6IjIyN2YzMjkzLWZiMWQtNGNiNC04ZDcyLWRlMDNiZGFkNTY2MCJ9XX0seyJpZCI6IjRlNzdlNGFkLTY4ZTMtNDk2Ny1hOWFkLWJlNjk3NTlkZWVmZSIsIm5hbWUiOiJEYXZpZCIsImhhbmQiOlt7ImlkIjoiMTRkOTNlN2YtNjI0NC00M2RiLTg2ZmEtYjk2ZjlhMmNlZTg1In0seyJpZCI6Ijc3ODEyNTkxLWE3ZTAtNGNhMS05YmU4LWRjMTJiY2U1MWIxYyIsInJlYWxWYWx1ZSI6MSwicmVhbENvbG9yIjoiYmx1ZSIsImNsdWUxIjp7InZhbHVlIjoxfX0seyJpZCI6IjIyMjE4NTEyLTFkNjgtNDRlZS1hZDExLWM4NmYzOGY0YTEyYyJ9LHsiaWQiOiJjNDc0NWE4My00M2YyLTQ5ODEtYThhOS1kNzEyMjcwY2ZhMzUifV19XX0=)

- Bob voit que le `1` marqué nécessite en *réponse* de poser une carte non marquée, pour indiquer à David que sa carte est *inutile*. 
Comme Carole n’a pas de `R1`, ce n’est donc pas une *finesse ordurière distante*, mais bien à lui de *répondre*.
- Il pose plutôt son `W2` connu.
- Carole sait qu’elle doit *répondre* à l’indice, sans quoi David risque de causer une bombe. Elle pense avoir le `R1` mais pose en fait 
son `G3`.
- David sait désormais que son `1` est *inutile*, et il le défausse.
- À nouveau à son tour, Bob pose la carte qui était en ***position finesse*** lorsque l’indice `1` a été donné : son `Y3`.
- Carole comprend qu’elle n’a pas le `R1` derrière son `G3`, et qu’il ne s’agissait pas d’une *finesse ordurière généreuse*, mais d’un 
*refus de finesse ordurière*. L’indice est *résolu*.

Pour éviter les incompréhensions, si vous utilisez cette astuce, assurez-vous d’abord :

- que la personne après vous puisse effectivement *répondre* sans causer une bombe, et ce, avec une carte lui étant totalement inconnue.
De cette manière, la personne suivante sera sûre qu’elle ne doit pas *répondre* elle-même.
- de ne pas défausser. La personne ayant donné cet indice s’attendait à ce que vous posiez une carte. Vous en avez peut-être une
***en danger***.
- que la carte en ***position finesse*** de la personne après vous soit *unique*. Ainsi, vous pourrez poser la vôtre au prochain tour 
sans risquer que ce soit la même.

Attention, cette astuce n’est pas compatible avec la *finesse ordurière corrective* (non présentée dans ce document, car trop 
rarement utilisable). On pourrait également vouloir l’utiliser dans le carde d’une *finesse* ou d’un *bluff finesse*, mais elle est incompatible
avec la *finesse corrective*.

### Pour aller plus loin

Le site <https://hanabi.github.io> (en anglais) documente de manière beaucoup plus complète les techniques et conventions pour Hanabi. 
Attention, la convention standard pour la ***position défausse*** qu’il présente n’est pas la même que dans ce document. Par ailleurs, s’il 
est très intéressant, beaucoup des conventions qu’on y trouve (notamment celles avancées) ne sont pas compatibles avec ce document, 
qui est plutôt destiné au jeu tel que pratiqué sur [Board Game Arena (BGA)](https://boardgamearena.com/).

Cet [autre document](https://docs.google.com/document/d/1VzgN6WoeYwh5NYtHECzUNZqFvpVcg-EdObU-YNMlyhc) (en anglais) liste aussi 
de nombreuses techniques et conventions, en comparant comment elles sont jouées, sur [BGA](https://boardgamearena.com/) et ailleurs.


## Référentiel

### Lexique

<a href="#lexique"></a>

- [Board Game Arena (BGA)](https://boardgamearena.com/) : plate-forme de jeux en ligne sur laquelle Hanabi est disponible.
- *bluff finesse* : indice faisant poser une carte ne correspondant pas à celle marquée, à quelqu’un pensant *répondre* à une *finesse*.
- *bluff ordurier* : indice faisant poser une carte ne correspondant pas à celle marquée, par quelqu’un pensant *répondre* à 
une *finesse ordurière*.
- *bluff retardé* : bluff qui n’est *révélé* qu’à un tour ultérieur.
- *bombe positionnelle* : bombe faisant poser la carte au même emplacement dans la main d’un autre personne de l’équipe.
- *bombe positionnelle facultative* : bombe faisant poser les cartes au même emplacement dans la main de deux autres personnes 
de l’équipe.
- carte ***en danger*** : carte *unique* en ***position défausse***.
- carte ***jouable*** : carte que l’on peut poser actuellement, car la précédente de la même série a été posée.
- carte *inutile* : exemplaire d’une carte déjà posée.
- carte *manquante* : carte devant être posée en *réponse* à un indice ayant marqué une carte non ***jouable***.
- carte *unique* : carte dont il ne reste qu’un seul exemplaire, dans la main des membres de l’équipe ou dans le paquet. C’est le cas des `5`, 
et des autres cartes dont tous les autres exemplaires sont déjà dans la défausse.
- carte *utile* : carte que l’on peut ou pourra poser (quand les précédentes de la même série le seront).
- causer une bombe : tenter de poser une carte qui n’est pas ***jouable*** actuellement. À la 3ᵉ bombe, l’équipe perd immédiatement la
partie. Selon la version du jeu, les bombes sont matérialisées via des jetons ou des tuiles représentant un paysage.
- *côté pioche* : côté de la main où une personne place les nouvelles cartes qu’elle pioche.
- défausser une carte : la mettre « à la poubelle », dans la défausse. Elle ne peut plus être utilisée durant la partie, mais on peut la consulter.
- *défausse gentleman* : défausse d’une carte ***jouable*** pour laisser quelqu’un d’autre poser sa copie de la carte défaussée à la place.
- *défausse gentleman corrective* : *défausse gentleman en couche*, nécessitant que quelqu’un d’autre pose une carte *manquante* pour
être *résolue*.
- *défausse gentleman en couche* : défausse d’une carte ***jouable*** pour laisser quelqu’un d’autre poser une ou plusieurs cartes 
en ***position finesse***, jusqu’à poser sa copie de la carte défaussée.
- *défausse gentleman finesse* : *défausse gentleman*, dont la carte défaussée n’est pas encore ***jouable*** et doit donner lieu
à une *réponse* pour créer une *finesse*.
- *défausse positionnelle* : défausse faisant poser la carte au même emplacement dans la main d’une autre personne.
- *extraction* : enchaînement de plusieurs indices, pour faire poser une carte ne pouvant pas se trouver en ***position finesse*** avec 
un seul indice.
- double indice inversant : deux indices identiques marquant les mêmes cartes, pour les faire poser dans le sens inverse.
- *finesse* : indice faisant poser une ou plusieurs cartes *manquantes* en ***position finesse***, en en marquant une qui la/les suit.
- *finesse généreuse* : *finesse* faisant poser une ou plusieurs cartes bonus, en plus de celles de la *finesse*.
- *finesse indirecte* : *finesse* dont la réponse est la seconde carte en ***position finesse***.
- *finesse méfiante* : *finesse* faisant poser une ou plusieurs cartes en plus de celles de la *finesse*, grâce à « la méfiance »  
de la personne recevant l’indice.
- *finesse ordurière* : indice faisant poser une carte *manquante*, pour éviter qu’une carte *inutile* ne soit posée.
- *finesse ordurière généreuse* : *finesse ordurière* faisant poser une ou plusieurs cartes bonus par du *jeu en couches*.
- *finesse ordurière renversée* : voir *renversée ordurière*.
- *finesse préemptive* : indice faisant poser une carte *manquante*, en marquant une carte ***en danger***.
- *finesse renversée* : voir *renversée*.
- *finesses superposées* : multiples finesses dont les *réponses* se trouvent chez une même personne, qui a plusieurs cartes
*manquantes* en ***position finesse***.
- *indice de correction* : indice en complément d’un précédent, pour marquer la carte réellement ***jouable*** et éviter une bombe.
- indice *défensif* : indice marquant une carte ***en danger*** pour lui éviter la défausse.
- indice *offensif* : indice faisant poser une ou plusieurs cartes.
- indice *offensif* *direct* : indice marquant une carte ***jouable***, pour la faire poser directement (sans *finesse* ou *renversée*).
- indice pour défausse : indice marquant une ou plusieurs cartes *inutiles*,  à défausser.
- *indice prématuré* : indice donné trop tôt, pouvant donner lieu à une mauvaise interprétation. Souvent, c’est 
un *indice défensif prématuré* pour une carte qui n’est pas encore en ***position défausse***.
- *indice urgent* : indice ne pouvant être donné que maintenant, avant qu’une carte ne soit défaussée ou change de position dans la main 
de son ou sa propriétaire.
- *indice de temporisation* : indice peu utile, voire *transparent*, donné en attente que des cartes soient posées, reportant éventuellement
la défausse de cartes *utiles*.
- *jouabilité implicite* : toute carte marquée doit être *utile* et devient donc ***jouable*** automatiquement à un moment donné (à moins
qu’un indice ne vienne indiquer le contraire).
- personne *éloignée* : personne qui ne joue pas juste après soi.
- *longue finesse* : *finesse* faisant poser plus de deux cartes, c’est-à-dire comportant plusieurs cartes *manquantes*.
- marquer une carte : donner un indice *valeur* ou *couleur* sur cette carte, marquant éventuellement d’autres cartes.
- *mono-bluff* : cas particulier, où la même personne reçoit l’indice, y *répond* et *révèle*  le bluff.
- *mono-finesse* : cas particulier, où une personne a en main plusieurs cartes manquantes nécessaires pour *répondre* à une *finesse*.
- NDR : Nombre de Défausses Restantes, permettant de poser toutes les cartes. Si l’on dépasse ce nombre, autant de cartes que la
différence ne pourront pas être posées.
- poser une carte : la placer dans la zone de feux d’artifice, dans une nouvelle pile ou sur la carte précédente de la même série.
- ***position défausse*** : position de la prochaine carte qu’une personne défaussera. On peut parler de plusieurs cartes dans cette
position, la seconde étant celle à défausser une fois que la première est partie.
- ***position finesse*** : position de la carte à poser en première. C’est la plus récente parmi celles que l’on peut poser. 
On peut parler de plusieurs cartes dans cette position, la seconde étant celle à poser derrière la précédente.
- *poussée ordurière* : indice marquant une carte *inutile*  en ***position défausse***, indiquant que la suivante est ***jouable***.
- *promesse* : information implicite indiquant la valeur et/ou la couleur d’une carte. Cette information n’a pas besoin d’être confirmée par un
indice explicite. On parle en général de *promesse* pour un bluff, où la carte marquée est la deuxième prochaine carte de sa couleur.
- *renversée* ou *finesse renversée* : *finesse* dont la personne à qui l’on marque une carte, **précède** celle avec la carte *manquante*.
- *renversée auto-annulative* : Mono-bluff, dont la personne recevant l’indice sait pertinemment qu’elle n’a pas la carte manquante, mais qui
*répond* quand même pour éviter une bombe.
- *renversée ordurière* ou *finesse ordurière renversée* : *finesse ordurière* dont la personne à qui l’on marque une carte, **précède** celle
avec la carte *manquante*.
- *réponse* : une *réponse* est une carte posée suite à un indice sur une carte non ***jouable***. On *répond* à une *finesse* 
ou à une *finesse ordurière* (ou à un *bluff*, à notre insu).
- *résoudre* : un indice est *résolu*, quand une ou plusieurs cartes ont été posées, et que toute l’équipe comprend pourquoi au vu de l‘indice 
donné.
- *révélé* : un bluff est dit « *révélé* », une fois que tout le monde sait (et en particulier la personne ayant reçu l’indice) que la carte marquée ne peut correspondre à la carte (ou à la suite de cartes) posée(s) en *réponse* à l’indice. Un bluff simple ne nécessite qu’une seule *réponse*
pour être *révélé*. Contrairement à du *jeu en couche*, la carte marquée ne sera pas posée.
- *semi-bluff* : indice faisant poser une ou plusieurs cartes **marquées**, ne correspondant pas à l’indice, puis compléter une *finesse*.


### Modes, options et variantes de jeu

<a href="#modes-options-et-variantes-de-jeu"></a>

- Mode **Normal** : 25 cartes à poser, de 5 séries de couleurs différentes.
- Mode **6 couleurs** : 30 cartes à poser, de 6 séries de couleurs différentes (dont les `multicolores`).
- Mode **6 couleurs difficile** : 30 cartes à poser, de 6 séries de couleurs différentes, dont une composée de 5 cartes `multicolores` 
*uniques*.
- Mode **Avalanche** : 30 cartes à poser, de 6 séries de couleurs différentes, dont une « réellement » `multicolore`.

Ces 4 modes sont incompatibles entre eux, mais peuvent être combinés avec une ou deux extensions suivantes :

- Extension **Poudre noire** : cette extension introduit des cartes noires, non marquables avec les indices *couleur*, et devant 
être posées du `5` au `1`. Les `Bk5` sont présents en trois exemplaires alors que le `Bk1`, lui, est *unique*. En jouant avec cette extension, 
l’équipe commence à **-5 points**.
- Extension **Flamboyants** : cette extension introduit cinq récompenses aléatoires lorsque l’équipe finit une série (en plaçant un `5` ou le
`Bk1`) à la place de la récupération d’un jeton indice habituelle. Parmi elles :
  - Gagnez un jeton indice + retirez une bombe
  - Donnez un indice *couleur*
  - Donnez un indice *valeur*
  - Remélangez une carte de la défausse dans la pioche
  - Posez une carte de la défausse en jeu

Au moins deux variantes sont jouées :

- Variante **nombre de cartes** :  
Quand on joue à 2, tout le monde a **une carte de plus** en main, ce qui ajoute de la souplesse en cas de mauvais tirage (ex : tous 
les 5 sortent très tôt dans la partie) et simplifie donc le jeu.   
Quand on joue à 5, tout le monde n’a que **3 cartes** en main (au lieu de 4). Cela ajoute beaucoup de complexité, et l’équipe doit être 
à l’aise avec le bluff pour pouvoir marquer uniquement les cartes *utiles* tout de suite et sans polluer les mains.

- Variante **Bouquet Final** :  
Pour gagner dans cette variante, l’équipe doit obtenir le score maximal obligatoirement. En contrepartie, le jeu continue jusqu’à ce que
tout le monde ait épuisé toute sa main ou gagné (plutôt qu’un tour seulement après la fin de la pioche).


## Traduction

<a href="#traduction"></a>

En anglais, et notamment sur [BGA](https://boardgamearena.com/), les noms suivants sont utilisés :

- bluff finesse : `finesse bluff`
- bluff ordurier : `trash bluff`
- bluff renversé : `reverse bluff`
- bluff retardé : `delayed bluff`
- cause une bombe / bombe : `bomb`
- corriger / correction : `fix`
- la défausse : the `discard`
- défausse gentleman : `gentleman discard` (ou `GD`), ou `sarcastic discard` si l’autre exemplaire de la carte était marqué.
- défausse gentleman en couche : `layered gentleman discard` (ou `layered GD`).
- défausse panique : `screaming discard`
- défausses consécutives : `double discard`
- défausse gentleman corrective : `patched gentleman discard`
- défausse positionnelle : `positional discard`
- défausser : `discard`
- donner un indice / indice : `clue` (éventuellement `hint`)
- en couches (ex : finesse en couches) : `layered` (ex : `layered finesse`)
- extension Poudre Noire : `black powder`
- extension Flamboyants :  comme en français
- indice couleur / valeur : `color / number clue`
- indice défensif : `save clue`
- indice défensif prématuré : `early save`
- indice offensif direct : `direct clue`
- indice prématuré : `early clue`
- indice transparent : `blank clue` (ou `empty clue`)
- jouabilité implicite : `good touch convention`
- finesse : `finesse`
- finesse généreuse ou renversée généreuse : `layered finesse`
- finesse méfiante : `clandestine finesse` (ou `layered finesse`)
- finesse ordurière  : `trash finesse` (ou `trash bluff`, à tort)
- finesse ordurière généreuse  : `layered trash finesse`
- finesse renversée ou renversée : `reverse finesse` ou `reverse`
- finesse ordurière renversée ou renversée ordurière : `reverse trash finesse`
- finesse superposée : `stack finesse`
- un jeton indice : a `token`
- marquage double : `reclue`
- mode Avalanche :  comme en français
- mono-bluff : `self-bluff`
- mono-finesse : `self-finesse`
- pas de protection précoce : `no early save`
- la pioche : the `deck`
- piocher : `draw`
- en position défausse : `on chop`
- en position finesse : `on finesse position`,  ou `on prompt` si la carte est déjà marquée ou `on finesse/bluff position` pour désigner la dernière
carte piochée par quelqu’un
- poussée ordurière : `trash push`
- renversée auto-annulative : `self color bluff`
- répondre / réponse : `answer`
- semi-bluff : `hidden finesse`
- vol d’indice : `steal clue` ou `skip`

Les autres termes ne possèdent pas de nom particulier.


## Table des matières

<a href="#table-des-matières"></a>

[TOC]
